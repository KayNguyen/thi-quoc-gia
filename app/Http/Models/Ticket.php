<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class Ticket extends Model
{
    protected $table = 'event_tickets';
    public $timestamps = false;

    public function events(){
        return $this->hasOne('App\Http\Models\Event', 'id', 'event_id');
    }

    public static function getDetailTicketById($id) {
        return self::where('id', $id)->first();
    }
}

<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class Event extends Model
{
    protected $table = 'events';
    public $timestamps = false;

    public function tickets(){
        return $this->hasMany('App\Http\Models\Ticket', 'event_id', 'id');
    }

    public function organizers(){
        return $this->hasOne('App\Http\Models\User', 'id', 'organizer_id');
    }

    public static function getAllEventByIdOrganizer($id) {
        return self::where('organizer_id', $id)->get()->toArray();
    }

    public static function getDetailEventById($id) {
        return self::with('tickets')->where('id', $id)->first();
    }
    
    public static function checkEventByIdOrganizer($id_admin, $id) {
        $model = self::find($id);
        if(!empty($model) && (int)$id_admin == $model['organizer_id']) {
            return true;
        }
        return false;

    }
}

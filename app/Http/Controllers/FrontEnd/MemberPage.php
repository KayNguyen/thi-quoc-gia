<?php

namespace App\Http\Controllers\FrontEnd;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class MemberPage extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:customer');
    }

    public function index(Request $request){

        $tpl = [];
        $tpl['site_title'] = 'Trang cá nhân';
        return \Lib::setView(__DIR__, 'profile.index', $tpl);
    }
}

<?php

namespace App\Http\Controllers\FrontEnd\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AuthController extends Controller
{

    public function __construct(){
        if(\Auth::guard('customer')->check()){
            return redirect()->route('home');
        }
    }

    public function showLoginForm()
    {
        return \Lib::setView(__DIR__, 'login.index');;
    }
    
    public function showRegisterForm()
    {
        return \Lib::setView(__DIR__, 'register.index');;
    }
    
    public function register()
    {
        dd(request()->all);
        return \Lib::setView(__DIR__, 'register.index');;
    }
}

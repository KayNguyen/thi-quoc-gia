<?php

namespace App\Http\Controllers\BackEnd\Events;

use Illuminate\Http\Request;
use App\Http\Controllers\AdminController;

use App\Http\Models\Event as THIS;

class EventsController extends AdminController
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct(new THIS());
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index($action = '')
    {
        $action = str_replace('-', '_', $action);

        if (method_exists($this, $action)) {
            return $this->$action();
        } else {
            return $this->home();
        }
    }
    
    public function home()
    {
        $tpl = [];
        $tpl['site_title'] = 'Trang quản trị';
        return \Lib::getInstance()->setView(__DIR__, 'index', $tpl);
    }
    
    public function input()
    {
        $tpl = [];
        $tpl['site_title'] = 'Cập nhật sự kiện';

        $id = Request::capture()->input('id', 0);
        if($id) {
            $tpl['data'] = THIS::getDetailEventById($id);

            $tpl['site_title'] = 'Cập nhật sự kiện - '.$tpl['data']['name'];
        }
        return \Lib::getInstance()->setView(__DIR__, 'input', $tpl);
    }
    
    public function detail()
    {
        $tpl = [];
        $id = Request::capture()->input('id', 0);
        
        if($id) {
            $tpl['site_title'] = 'Chi tiết sự kiện';
            $tpl['data'] = THIS::getDetailEventById($id);
            return \Lib::getInstance()->setView(__DIR__, 'index', $tpl);
        }
        return abort(404);
        
    }

    public function _save()
    {
        $id = request('id', 0);
        $obj['name'] = request('name');
        $obj['slug'] = (request('slug')) ? request('slug') : \str_slug(request('name'));
        $obj['date'] = request('date');
        $obj['organizer_id'] = \Auth::user()->id;
        $model = THIS::find($id);
        if(!$model) {
            // Thêm mới
            $id = THIS::insertGetId($obj);
            if($id) {
                $data['link'] = admin_link('/');
                return \Lib::getInstance()->getJsonSuccess('Thêm mới thành công', $data);
            }

        }else {
            $model->name = $obj['name'];
            $model->slug = $obj['slug'];
            $model->date = $obj['date'];
            $model->save();
            return \Lib::getInstance()->getJsonSuccess('Cập nhật thành công', $obj);
        }
        $tpl = [];
        $tpl['site_title'] = 'Cập nhật sự kiện';
        return \Lib::setView(__DIR__, 'input', $tpl);
    }
}

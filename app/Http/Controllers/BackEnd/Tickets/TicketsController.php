<?php

namespace App\Http\Controllers\BackEnd\Tickets;

use Illuminate\Http\Request;
use App\Http\Controllers\AdminController;

use App\Http\Models\Event;
use App\Http\Models\Ticket as THIS;

class TicketsController extends AdminController
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct(new THIS());
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index($action = '')
    {
        $action = str_replace('-', '_', $action);

        if (method_exists($this, $action)) {
            return $this->$action();
        } else {
            return $this->home();
        }
    }
    
    public function home()
    {
        $tpl = [];
        $tpl['site_title'] = 'Trang quản trị';
        return \Lib::getInstance()->setView(__DIR__, 'index', $tpl);
    }
    
    public function input()
    {
        $tpl = [];
        $tpl['site_title'] = 'Cập nhật vé';

        $id = Request::capture()->input('id', 0);
        $tpl['event_id'] = Request::capture()->input('event_id', 0);
        $model = THIS::find($id);
        if(Event::checkEventByIdOrganizer(\Auth::user()->id, $tpl['event_id'])) {
            
            if(!$model) {
                $tpl['site_title'] = 'Thêm mới vé';
                
            }else {
                $tpl['data'] = THIS::getDetailTicketById($id);
                $tpl['data']['special_validity'] = json_decode($tpl['data']['special_validity']);
                $tpl['site_title'] = 'Cập nhật vé - '.$tpl['data']['name'];
                
            }
        }
        return \Lib::getInstance()->setView(__DIR__, 'input', $tpl);
    }
    
    public function detail()
    {
        $tpl = [];
        $id = Request::capture()->input('id', 0);
        
        if($id) {
            $tpl['site_title'] = 'Chi tiết sự kiện';
            $tpl['data'] = THIS::getDetailEventById($id);
            dd($tpl['data']);
            return \Lib::getInstance()->setView(__DIR__, 'index', $tpl);
        }
        return abort(404);
        
    }

    public function _save()
    {
        $id = Request::capture()->input('id', 0);
        $event_id = Request::capture()->input('event_id', 0);
        $obj['name'] = request('name');
        $obj['cost'] = (request('cost')) ? request('cost') : 0.00;
        $obj['event_id'] = $event_id;
        
        switch (request('special_validity')) {
            case 'amount':
                $obj['special_validity'] = [
                    'type' => 'amount',
                    'amount' => request('amount')
                ];
                break;
            
            case 'date':
                $obj['special_validity'] = [
                    'type' => 'date',
                    'amount' => request('date')
                ];
                break;
            
            default:
            $obj['special_validity'] = null;
                break;
        }
        $obj['special_validity'] = \json_encode($obj['special_validity']);
        $model = THIS::find($id);
        if(!$model) {
            // Thêm mới
            $id = THIS::insertGetId($obj);
            if($id) {
                $data['link'] = admin_link('/event/detail?id='.$obj['event_id']);
                return \Lib::getInstance()->getJsonSuccess('Thêm mới thành công', $data);
            }

        }else {
            $model->name = $obj['name'];
            $model->slug = $obj['slug'];
            $model->date = $obj['date'];
            $model->save();
            return \Lib::getInstance()->getJsonSuccess('Cập nhật thành công', $obj);
        }
        $tpl = [];
        $tpl['site_title'] = 'Cập nhật sự kiện';
        return \Lib::setView(__DIR__, 'input', $tpl);
    }
}

<?php

namespace App\Http\Controllers\BackEnd\Home;

use Illuminate\Http\Request;
use App\Http\Controllers\AdminController;

use App\Http\Models\Event;

class MngSystem extends AdminController
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $tpl = [];
        $organizer_id = \Auth::user()->id;
        $tpl['events'] = Event::getAllEventByIdOrganizer($organizer_id);
        $tpl['site_title'] = 'Trang quản trị';
        return \Lib::getInstance()->setView(__DIR__, 'index', $tpl);
    }
}

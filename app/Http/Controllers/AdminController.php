<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;

class AdminController extends Controller
{
    public $model;
    public $error = [];
    public $valid = [];
    protected $title = '';
    protected $editId = 0;
    protected $editMode = false;
    protected $form = ''; // list|edit|add
    protected $timeStamp = ''; // neu co sẽ tu dong luu duoi dang time()

    function __construct($model = false, $valid = true) {
        
        if(!empty($valid)){
            $this->valid = $valid;
        }
        if(!empty($model)){
            $this->model = $model;
        }
    }

    public function save(Request $request, $id = 0) {
        $this->editId = $id;
        $this->editMode = $id > 0;

        $this->beforeSave($request);


        try{
            $this->model->save();
            $this->editId = $this->model->id;

            //hook after save
            $this->afterSave($request);

        }catch(\Exception $e){
            return redirect()->back()->withInput()->withErrors(['error:' => 'Có lỗi trong quá trình lưu dữ liệu! Vui lòng thử lại! '.$e->getMessage()]);
        }
    }

    public function beforeSave(Request $request, $ignore_ext = []){
        $titleField = $this->titleField;
        $data = $request->all();
        $ignore = array_merge(['_token', 'id'], $ignore_ext);
        foreach ($ignore as $i){
            unset($data[$i]);
        }
        foreach($data as $k => $v){
            switch ($k){
                case 'email':
                    $this->model->$k = strtolower($v);
                    break;
                case 'password':
                    if(!empty($v)) {
                        $this->model->$k = bcrypt($v);
                    }
                    break;
                case 'title_seo':
                    $this->model->$k = !empty($request->title_seo) ? $request->title_seo : $request->$titleField;
                    break;
                default:
                    $this->model->$k = $v;
            }
        }
        $timeStamp = $this->timeStamp;
        if(!empty($timeStamp) && empty($this->model->$timeStamp)){
            $this->model->$timeStamp = time();
        }
        $this->uploadImage($request, $request->$titleField, $this->imageField);
    }

    public function afterSave(Request $request){
        //todo here
    }
}

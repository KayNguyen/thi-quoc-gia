<?php

namespace App\Libs;

class Lib 
{

    static private $instance = false;

    public function __construct()
    {

        self::$instance = &$this;
    }

    public static function &getInstance()
    {

        if (!self::$instance) {
            new self();
        }

        return self::$instance;
    }

    public function siteTitle($title = '', $is_admin = false) {
        $host = \Request::getHost();
        $title = trim($title != '' ? $title : '');
        if($is_admin) {
            $title = ($title == '' ? $host : ($title . '-'. $host));
        }
        return $title;
    }

    public function setView($dir, $blade, $var = [], $render = false)
    {
        # code...
        if($dir) {
            \View::addLocation($dir);
            $location = '/views/';

        }else {
            $location == '';
        }
        $view = view($location . $blade, $var);

        if($render) {
            return $view->render();
        }
        return $view;
    }

    public function showJson($json)
    {
        if (config('app.debug')) {
            $json['debug']['sql'] = \DB::getQueryLog();
            $json['debug']['post'] = $_POST;
            $json['debug']['get'] = $_GET;
            $json['debug']['cookie'] = $_COOKIE;
            $json['debug']['raw'] = file_get_contents('php://input');
        }
        app('debugbar')->disable();
        //tracking end over here
        header('Content-Type: application/json; charset=utf-8');

        die(json_encode($json));
        // return response()->json($json);
    }

    public function getJsonSuccess($msg, $data = [])
    {
        $json['status'] = 1;
        $json['msg'] = $msg;
        $json['data'] = $data;


        return $this->showJson($json);
    }


}

<?php

namespace App\Libs;

class HtmlHelper
{
    private static $instance = FALSE;
    static $clientVersion = '1.0.0';
    public function __construct() {
        self::$instance = &$this;
    }

    public static function getInstance() {
        if(!self::$instance) {
            new self();
        }

        return self::$instance;
    }

    function setCssLink($link)
    {
        return '<link href="' . url($link) . '?v=' . static::$clientVersion . '" rel="stylesheet">';
    }
    
    function setJsLink($link)
    {
        return '<script src="' . url($link) . '?v=' . static::$clientVersion.' "type="text/javascript"></script>';
    }

    
}
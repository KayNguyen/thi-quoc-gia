<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});





Route::group(['middleware' => ['admin'], 'prefix' => 'admin', 'before' => ''], function () {
    Auth::routes();
    Route::get('/logout', ['as' => 'logout', 'uses' => 'Auth\LoginController@logout']);
    /* page admin */
    Route::group(['middleware' => ['auth']], function () {
        Route::get('/', ['as' => 'adminHome', 'uses' => 'BackEnd\Home\MngSystem@index']);
        Route::any('/event/{action_name?}', ['as' => 'adminEvents', 'uses' => 'BackEnd\Events\EventsController@index']);
        Route::any('/ticket/{action_name?}', ['as' => 'adminTickets', 'uses' => 'BackEnd\Tickets\TicketsController@index']);
    });
    
});


Route::group(['middleware' => ['web'], 'as' => 'fend'], function () {
    
    Route::get('/login', ['as' => 'login', 'uses' => 'FrontEnd\Auth\AuthController@showLoginForm']);
    Route::post('/login', ['as' => 'post.login', 'uses' => 'FrontEnd\Auth\AuthController@login']);
    Route::get('/register', ['as' => 'register', 'uses' => 'FrontEnd\Auth\AuthController@showRegisterForm']);
    Route::post('/register', ['as' => 'register', 'uses' => 'FrontEnd\Auth\AuthController@register']);

    Route::get('/home', ['as' => 'home', 'uses' => 'HomeController@index']);
    /* page profile */
    Route::group(['as' => 'profile'], function () {
        Route::get('/profile', ['uses' => 'FrontEnd\MemberPage@index']);
    });
});
var haveChangeData = true;
function _POST_FORM(formId, url, options = {}) {

    let {callback, type = 'json',globalCallback =true} = options;
    if (typeof callback !== 'function') {

    }
    let data = [];
    if(typeof formId!=="undefined") {
        data = $(formId).serializeArray();
    }else{

    }
    let _token = jQuery('meta[name=_token]').attr("content");
    if (_token) {
        let _data = {'name': '_token', 'value': _token};
        data.push(_data);
    }
    jQuery.ajax({
        url: url,
        type: "POST",
        data: data,
        dataType: type,
        success: function (res) {
            window.location.href = res.data.link;
        },
        error: function (xhr, ajaxOptions, thrownError) {
            alert(thrownError);
        }
    });
    return false;

}

function _SUBMIT_FORM(formId, link) {
    if (haveChangeData) {
        //haveChangeData = false;
        return _POST_FORM(formId, link)
    } else {
        $.toast({
            heading: 'Thông báo',
            position: 'top-right',
            text: 'Không có dữ liệu nào được thay đổi. Bạn cần thay đổi thông tin trước khi cập nhật',
            icon: 'info',
            loader: true,        // Change it to false to disable loader
            loaderBg: '#9EC600'  // To change the background
        });
    }
}
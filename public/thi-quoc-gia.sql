/*
 Navicat Premium Data Transfer

 Source Server         : sluios.com
 Source Server Type    : MySQL
 Source Server Version : 100411
 Source Host           : localhost:3306
 Source Schema         : thi-quoc-gia

 Target Server Type    : MySQL
 Target Server Version : 100411
 File Encoding         : 65001

 Date: 08/01/2020 12:38:31
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for attendees
-- ----------------------------
DROP TABLE IF EXISTS `attendees`;
CREATE TABLE `attendees`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `firstname` varchar(45) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `lastname` varchar(45) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `username` varchar(45) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `email` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `registration_code` varchar(6) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `login_token` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 201 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of attendees
-- ----------------------------
INSERT INTO `attendees` VALUES (1, 'Horacio', 'Yakovich', 'attendee1', 'hyakovich0@va.gov', '35DGZX', '');
INSERT INTO `attendees` VALUES (2, 'Nanon', 'Darthe', 'attendee2', 'ndarthe1@list-manage.com', 'UP243M', '');
INSERT INTO `attendees` VALUES (3, 'Rutter', 'Wauchope', 'rwauchope2', 'rwauchope2@yolasite.com', 'B35B6T', '');
INSERT INTO `attendees` VALUES (4, 'Lizette', 'Caroll', 'lcaroll3', 'lcaroll3@icq.com', '6F7TRJ', '');
INSERT INTO `attendees` VALUES (5, 'Margit', 'Alvar', 'malvar4', 'malvar4@woothemes.com', 'YZ4U26', '');
INSERT INTO `attendees` VALUES (6, 'Cosme', 'Minogue', 'cminogue5', 'cminogue5@techcrunch.com', 'WA322T', '');
INSERT INTO `attendees` VALUES (7, 'Cal', 'Penton', 'cpenton6', 'cpenton6@weibo.com', '9CY9AR', '');
INSERT INTO `attendees` VALUES (8, 'Corbet', 'Penton', 'cleamon7', 'cleamon7@pen.io', '7BDK38', '');
INSERT INTO `attendees` VALUES (9, 'Cacilie', 'O\'Calleran', 'cocalleran8', 'cocalleran8@wunderground.com', '38NTEF', '');
INSERT INTO `attendees` VALUES (10, 'Ephrem', 'Healey', 'ehealey9', 'ehealey9@dailymail.co.uk', 'CNXW26', '');
INSERT INTO `attendees` VALUES (11, 'Averil', 'Arnson', 'aarnsona', 'aarnsona@princeton.edu', '36PQWG', '');
INSERT INTO `attendees` VALUES (12, 'Albertina', 'Dunk', 'adunkb', 'adunkb@ifeng.com', '36PQWG', '');
INSERT INTO `attendees` VALUES (13, 'Dwain', 'Wharlton', 'dwharltonc', 'dwharltonc@telegraph.co.uk', '52425V', '');
INSERT INTO `attendees` VALUES (14, 'Cassondra', 'Doxey', 'cdoxeyd', 'cdoxeyd@statcounter.com', '14FCDF', '');
INSERT INTO `attendees` VALUES (15, 'Cybil', 'Lethibridge', 'clethibridgee', 'clethibridgee@marriott.com', 'YS48D2', '');
INSERT INTO `attendees` VALUES (16, 'Ardelis', 'Keinrat', 'akeinratf', 'akeinratf@live.com', 'E22ARB', '');
INSERT INTO `attendees` VALUES (17, 'Celestyna', 'Andresen', 'candreseng', 'candreseng@example.com', 'QZHKHS', '');
INSERT INTO `attendees` VALUES (18, 'Rosamond', 'Walkinshaw', 'rwalkinshawh', 'rwalkinshawh@canalblog.com', '8ND9MB', '');
INSERT INTO `attendees` VALUES (19, 'Jody', 'Balbeck', 'jbalbecki', 'jbalbecki@dion.ne.jp', 'XA5ENK', '');
INSERT INTO `attendees` VALUES (20, 'Dieter', 'Curneen', 'dcurneenj', 'dcurneenj@netscape.com', 'KJ7FW3', '');
INSERT INTO `attendees` VALUES (21, 'Mendy', 'Oland', 'molandk', 'molandk@joomla.org', '7YTD4C', '');
INSERT INTO `attendees` VALUES (22, 'Otho', 'Muddicliffe', 'omuddicliffel', 'omuddicliffel@360.cn', 'NMJ2C7', '');
INSERT INTO `attendees` VALUES (23, 'Lela', 'Beardsall', 'lbeardsallm', 'lbeardsallm@arstechnica.com', 'H6PZAS', '');
INSERT INTO `attendees` VALUES (24, 'Elvis', 'Grocutt', 'egrocuttn', 'egrocuttn@etsy.com', 'ABYZT7', '');
INSERT INTO `attendees` VALUES (25, 'Robbert', 'Dering', 'rderingo', 'rderingo@github.com', '116E2H', '');
INSERT INTO `attendees` VALUES (26, 'Joseph', 'Welham', 'jwelhamp', 'jwelhamp@unesco.org', '16FCNU', '');
INSERT INTO `attendees` VALUES (27, 'Gianna', 'Lantuffe', 'glantuffeq', 'glantuffeq@forbes.com', '13JY5X', '');
INSERT INTO `attendees` VALUES (28, 'Keenan', 'Agney', 'kagneyr', 'kagneyr@cnbc.com', '8J3DUE', '');
INSERT INTO `attendees` VALUES (29, 'Hamilton', 'Cortnay', 'hcortnays', 'hcortnays@java.com', 'JF3J6Q', '');
INSERT INTO `attendees` VALUES (30, 'Maury', 'Gaymar', 'mgaymart', 'mgaymart@so-net.ne.jp', 'XDRNBH', '');
INSERT INTO `attendees` VALUES (31, 'Timothea', 'Ryle', 'tryleu', 'tryleu@e-recht24.de', 'HEZC55', '');
INSERT INTO `attendees` VALUES (32, 'Gianna', 'Eallis', 'geallisv', 'geallisv@istockphoto.com', 'PGWA3A', '');
INSERT INTO `attendees` VALUES (33, 'Lew', 'Skeats', 'lskeatsw', 'lskeatsw@blogger.com', 'HXUQ6N', '');
INSERT INTO `attendees` VALUES (34, 'Ulrich', 'Mackrill', 'umackrillx', 'umackrillx@noaa.gov', '569BAJ', '');
INSERT INTO `attendees` VALUES (35, 'Emelda', 'Nash', 'enashy', 'enashy@themeforest.net', 'XY88A5', '');
INSERT INTO `attendees` VALUES (36, 'Perl', 'Markos', 'pmarkosz', 'pmarkosz@plala.or.jp', 'RTWGXZ', '');
INSERT INTO `attendees` VALUES (37, 'Jessica', 'Newnham', 'jnewnham10', 'jnewnham10@qq.com', '2T699N', '');
INSERT INTO `attendees` VALUES (38, 'Mathe', 'Colman', 'mcolman11', 'mcolman11@jiathis.com', 'QK9UUV', '');
INSERT INTO `attendees` VALUES (39, 'Calhoun', 'Flamank', 'cflamank12', 'cflamank12@desdev.cn', '575JTN', '');
INSERT INTO `attendees` VALUES (40, 'Carly', 'Sawday', 'csawday13', 'csawday13@amazon.co.jp', 'TDV4V3', '');
INSERT INTO `attendees` VALUES (41, 'Westley', 'Pontin', 'wpontin14', 'wpontin14@chron.com', '4MP83M', '');
INSERT INTO `attendees` VALUES (42, 'Evered', 'Donativo', 'edonativo15', 'edonativo15@youtube.com', 'CKN7VY', '');
INSERT INTO `attendees` VALUES (43, 'Bonny', 'Oldridge', 'boldridge16', 'boldridge16@de.vu', '9AHQ3U', '');
INSERT INTO `attendees` VALUES (44, 'Beverlee', 'Fernando', 'bfernando17', 'bfernando17@youtu.be', 'PW67GY', '');
INSERT INTO `attendees` VALUES (45, 'Darbee', 'Bovaird', 'dbovaird18', 'dbovaird18@mysql.com', 'SR5GXU', '');
INSERT INTO `attendees` VALUES (46, 'Codi', 'Dumelow', 'cdumelow19', 'cdumelow19@hexun.com', 'VR11S9', '');
INSERT INTO `attendees` VALUES (47, 'Joycelin', 'Boston', 'jboston1a', 'jboston1a@ed.gov', 'KNN25J', '');
INSERT INTO `attendees` VALUES (48, 'Terrance', 'Tyrrell', 'ttyrrell1b', 'ttyrrell1b@noaa.gov', 'U7XT92', '');
INSERT INTO `attendees` VALUES (49, 'Rodolphe', 'Caird', 'rcaird1c', 'rcaird1c@ustream.tv', '3GFX1S', '');
INSERT INTO `attendees` VALUES (50, 'Karry', 'Coysh', 'kcoysh1d', 'kcoysh1d@tuttocitta.it', 'MN7TNY', '');
INSERT INTO `attendees` VALUES (51, 'Elizabeth', 'Tamas', 'etamas1e', 'etamas1e@npr.org', '5KYK1U', '');
INSERT INTO `attendees` VALUES (52, 'Claudius', 'Pennetti', 'cpennetti1f', 'cpennetti1f@mac.com', 'ZDQEF4', '');
INSERT INTO `attendees` VALUES (53, 'Teddie', 'MacRury', 'tmacrury1g', 'tmacrury1g@marketwatch.com', 'ZX64B6', '');
INSERT INTO `attendees` VALUES (54, 'Denys', 'Harteley', 'dharteley1h', 'dharteley1h@elpais.com', '8SDA34', '');
INSERT INTO `attendees` VALUES (55, 'Moises', 'Satch', 'msatch1i', 'msatch1i@google.es', 'X99PFB', '');
INSERT INTO `attendees` VALUES (56, 'Cassandra', 'Fairfull', 'cfairfull1j', 'cfairfull1j@bandcamp.com', 'ST7J7Y', '');
INSERT INTO `attendees` VALUES (57, 'Cody', 'Tureville', 'ctureville1k', 'ctureville1k@tuttocitta.it', 'CRF7MS', '');
INSERT INTO `attendees` VALUES (58, 'Kate', 'Petranek', 'kpetranek1l', 'kpetranek1l@youtube.com', 'NWYTXB', '');
INSERT INTO `attendees` VALUES (59, 'Austen', 'Pobjoy', 'apobjoy1m', 'apobjoy1m@google.pl', 'HW88CJ', '');
INSERT INTO `attendees` VALUES (60, 'Orville', 'Oliver', 'ooliver1n', 'ooliver1n@sbwire.com', 'F8J8G6', '');
INSERT INTO `attendees` VALUES (61, 'Ab', 'McNalley', 'amcnalley1o', 'amcnalley1o@blogtalkradio.com', '9UHJDW', '');
INSERT INTO `attendees` VALUES (62, 'Toddy', 'Blight', 'tblight1p', 'tblight1p@cyberchimps.com', 'NUHA6B', '');
INSERT INTO `attendees` VALUES (63, 'Laureen', 'Zarfai', 'lzarfai1q', 'lzarfai1q@wired.com', 'NS4UF1', '');
INSERT INTO `attendees` VALUES (64, 'Dulciana', 'Sylvaine', 'dsylvaine1r', 'dsylvaine1r@engadget.com', 'C9VYMS', '');
INSERT INTO `attendees` VALUES (65, 'Elisabeth', 'Swanwick', 'eswanwick1s', 'eswanwick1s@hibu.com', 'U4BSTQ', '');
INSERT INTO `attendees` VALUES (66, 'Elliott', 'Cullerne', 'ecullerne1t', 'ecullerne1t@hc360.com', '9CP98W', '');
INSERT INTO `attendees` VALUES (67, 'Lynnett', 'Hassell', 'lhassell1u', 'lhassell1u@loc.gov', 'JE5BDY', '');
INSERT INTO `attendees` VALUES (68, 'Lenka', 'McOmish', 'lmcomish1v', 'lmcomish1v@ocn.ne.jp', 'RN7M68', '');
INSERT INTO `attendees` VALUES (69, 'Gisela', 'Jorger', 'gjorger1w', 'gjorger1w@google.co.jp', 'QCWXHS', '');
INSERT INTO `attendees` VALUES (70, 'Shoshana', 'Usherwood', 'susherwood1x', 'susherwood1x@google.es', 'JT198X', '');
INSERT INTO `attendees` VALUES (71, 'Florrie', 'Metcalf', 'fmetcalf1y', 'fmetcalf1y@bbc.co.uk', 'T4UVFX', '');
INSERT INTO `attendees` VALUES (72, 'Kellina', 'Reims', 'kreims1z', 'kreims1z@apache.org', 'DTC86N', '');
INSERT INTO `attendees` VALUES (73, 'Sybille', 'Kehoe', 'skehoe20', 'skehoe20@mlb.com', 'W5GAJB', '');
INSERT INTO `attendees` VALUES (74, 'Mabel', 'Carwithan', 'mcarwithan21', 'mcarwithan21@yolasite.com', 'AG5Z69', '');
INSERT INTO `attendees` VALUES (75, 'Deane', 'Haydn', 'dhaydn22', 'dhaydn22@netvibes.com', 'TV3KHN', '');
INSERT INTO `attendees` VALUES (76, 'Archy', 'Earngy', 'aearngy23', 'aearngy23@typepad.com', '86KPG5', '');
INSERT INTO `attendees` VALUES (77, 'Clareta', 'Jesteco', 'cjesteco24', 'cjesteco24@ucoz.ru', 'U5AWGG', '');
INSERT INTO `attendees` VALUES (78, 'Delinda', 'Barrell', 'dbarrell25', 'dbarrell25@globo.com', 'SJGNWV', '');
INSERT INTO `attendees` VALUES (79, 'Theresina', 'Rummins', 'trummins26', 'trummins26@marketwatch.com', 'TM3AQB', '');
INSERT INTO `attendees` VALUES (80, 'Carine', 'Garwill', 'cgarwill27', 'cgarwill27@icq.com', 'THYSQE', '');
INSERT INTO `attendees` VALUES (81, 'Mercedes', 'Boncore', 'mboncore28', 'mboncore28@phoca.cz', 'W5ZYMV', '');
INSERT INTO `attendees` VALUES (82, 'Sauveur', 'Schutt', 'sschutt29', 'sschutt29@craigslist.org', 'CCGXZN', '');
INSERT INTO `attendees` VALUES (83, 'Kalila', 'Balnave', 'kbalnave2a', 'kbalnave2a@webeden.co.uk', 'UZX4QB', '');
INSERT INTO `attendees` VALUES (84, 'Elsie', 'Pasque', 'epasque2b', 'epasque2b@ehow.com', 'NXVM5A', '');
INSERT INTO `attendees` VALUES (85, 'Omar', 'Kimmons', 'okimmons2c', 'okimmons2c@huffingtonpost.com', '38WZ3U', '');
INSERT INTO `attendees` VALUES (86, 'Theresita', 'Mara', 'tmara2d', 'tmara2d@youtu.be', '2DN8B2', '');
INSERT INTO `attendees` VALUES (87, 'Dina', 'McGoldrick', 'dmcgoldrick2e', 'dmcgoldrick2e@go.com', 'NAW3V7', '');
INSERT INTO `attendees` VALUES (88, 'Ertha', 'Moynihan', 'emoynihan2f', 'emoynihan2f@issuu.com', '4M5QVU', '');
INSERT INTO `attendees` VALUES (89, 'Averell', 'Perocci', 'aperocci2g', 'aperocci2g@rakuten.co.jp', 'CNTMS8', '');
INSERT INTO `attendees` VALUES (90, 'Rosalind', 'Osgarby', 'rosgarby2h', 'rosgarby2h@mlb.com', 'UPU53U', '');
INSERT INTO `attendees` VALUES (91, 'Nichole', 'Ingre', 'ningre2i', 'ningre2i@geocities.com', 'YERUD9', '');
INSERT INTO `attendees` VALUES (92, 'Jonah', 'Servant', 'jservant2j', 'jservant2j@people.com.cn', 'X1GFCD', '');
INSERT INTO `attendees` VALUES (93, 'Carmella', 'Allmann', 'callmann2k', 'callmann2k@infoseek.co.jp', 'H2USV6', '');
INSERT INTO `attendees` VALUES (94, 'Dru', 'Hathorn', 'dhathorn2l', 'dhathorn2l@smh.com.au', 'GB9MTW', '');
INSERT INTO `attendees` VALUES (95, 'Kory', 'D\'Alesco', 'kdalesco2m', 'kdalesco2m@netlog.com', '1EHX7V', '');
INSERT INTO `attendees` VALUES (96, 'Orella', 'Pollastrone', 'opollastrone2n', 'opollastrone2n@biblegateway.com', '445VMJ', '');
INSERT INTO `attendees` VALUES (97, 'Noella', 'Gopsall', 'ngopsall2o', 'ngopsall2o@geocities.jp', '2TUFJH', '');
INSERT INTO `attendees` VALUES (98, 'Elyssa', 'Le Cornu', 'elecornu2p', 'elecornu2p@ebay.com', 'MN5S89', '');
INSERT INTO `attendees` VALUES (99, 'Bruno', 'Linkin', 'blinkin2q', 'blinkin2q@ocn.ne.jp', 'NR9PN5', '');
INSERT INTO `attendees` VALUES (100, 'Staffard', 'Kamienski', 'skamienski2r', 'skamienski2r@wp.com', 'W6QKP3', '');
INSERT INTO `attendees` VALUES (101, 'Elliott', 'Seppey', 'eseppey0', 'eseppey0@icio.us', 'AWLEV2', '');
INSERT INTO `attendees` VALUES (102, 'Veronike', 'Heyworth', 'vheyworth1', 'vheyworth1@cargocollective.com', '2T67NH', '');
INSERT INTO `attendees` VALUES (103, 'Barbara', 'Gauntlett', 'bgauntlett2', 'bgauntlett2@webmd.com', 'TTKUU0', '');
INSERT INTO `attendees` VALUES (104, 'Alphard', 'Abramovic', 'aabramovic3', 'aabramovic3@apache.org', 'PC9XMT', '');
INSERT INTO `attendees` VALUES (105, 'Arlen', 'Lymbourne', 'alymbourne4', 'alymbourne4@scientificamerican.com', '4KG2OQ', '');
INSERT INTO `attendees` VALUES (106, 'Thadeus', 'Lemm', 'tlemm5', 'tlemm5@squidoo.com', 'WJBO8C', '');
INSERT INTO `attendees` VALUES (107, 'Cooper', 'Cordero', 'ccordero6', 'ccordero6@loc.gov', 'S77FXU', '');
INSERT INTO `attendees` VALUES (108, 'Janel', 'Gelling', 'jgelling7', 'jgelling7@reuters.com', '2TIIN8', '');
INSERT INTO `attendees` VALUES (109, 'Angy', 'Yoell', 'ayoell8', 'ayoell8@theglobeandmail.com', '017OVA', '');
INSERT INTO `attendees` VALUES (110, 'Montague', 'Dahill', 'mdahill9', 'mdahill9@zimbio.com', '1AYTEX', '');
INSERT INTO `attendees` VALUES (111, 'Briny', 'McAlester', 'bmcalestera', 'bmcalestera@goodreads.com', '07RP55', '');
INSERT INTO `attendees` VALUES (112, 'Travers', 'Dowyer', 'tdowyerb', 'tdowyerb@ow.ly', '4U2HZ4', '');
INSERT INTO `attendees` VALUES (113, 'Grantley', 'Chalice', 'gchalicec', 'gchalicec@go.com', 'DVI7LY', '');
INSERT INTO `attendees` VALUES (114, 'Cleon', 'Klinck', 'cklinckd', 'cklinckd@whitehouse.gov', '39FVFX', '');
INSERT INTO `attendees` VALUES (115, 'Freddy', 'Gravestone', 'fgravestonee', 'fgravestonee@state.tx.us', 'MKXX2I', '');
INSERT INTO `attendees` VALUES (116, 'Guglielmo', 'Bruggeman', 'gbruggemanf', 'gbruggemanf@bigcartel.com', 'D0FT2P', '');
INSERT INTO `attendees` VALUES (117, 'Legra', 'Briat', 'lbriatg', 'lbriatg@bloglines.com', '82VTBG', '');
INSERT INTO `attendees` VALUES (118, 'Christoph', 'Vardey', 'cvardeyh', 'cvardeyh@soup.io', 'TWF9S2', '');
INSERT INTO `attendees` VALUES (119, 'Nicolina', 'Greated', 'ngreatedi', 'ngreatedi@walmart.com', 'HOMPX2', '');
INSERT INTO `attendees` VALUES (120, 'Jerrie', 'Goacher', 'jgoacherj', 'jgoacherj@purevolume.com', '2J0E6O', '');
INSERT INTO `attendees` VALUES (121, 'Alvie', 'Dello', 'adellok', 'adellok@vk.com', '8JB7O5', '');
INSERT INTO `attendees` VALUES (122, 'Genia', 'Maylor', 'gmaylorl', 'gmaylorl@cloudflare.com', '06AUEO', '');
INSERT INTO `attendees` VALUES (123, 'Annalise', 'Marty', 'amartym', 'amartym@answers.com', 'JD53JK', '');
INSERT INTO `attendees` VALUES (124, 'Maia', 'Gridley', 'mgridleyn', 'mgridleyn@4shared.com', '612A8N', '');
INSERT INTO `attendees` VALUES (125, 'Zolly', 'O\'Deoran', 'zodeorano', 'zodeorano@hibu.com', 'RD4P3B', '');
INSERT INTO `attendees` VALUES (126, 'Adelind', 'Feander', 'afeanderp', 'afeanderp@g.co', 'I5V3CG', '');
INSERT INTO `attendees` VALUES (127, 'Melisa', 'Spillman', 'mspillmanq', 'mspillmanq@seattletimes.com', 'VZ65P4', '');
INSERT INTO `attendees` VALUES (128, 'Aurora', 'Ourry', 'aourryr', 'aourryr@cnbc.com', 'UHJZ6L', '');
INSERT INTO `attendees` VALUES (129, 'Ricoriki', 'Crosio', 'rcrosios', 'rcrosios@amazon.com', 'BNNV5D', '');
INSERT INTO `attendees` VALUES (130, 'Millard', 'Timmens', 'mtimmenst', 'mtimmenst@google.nl', 'WB13UW', '');
INSERT INTO `attendees` VALUES (131, 'Colet', 'Medwell', 'cmedwellu', 'cmedwellu@gravatar.com', 'KYN0OZ', '');
INSERT INTO `attendees` VALUES (132, 'Karly', 'Mosconi', 'kmosconiv', 'kmosconiv@icio.us', '6ESIHL', '');
INSERT INTO `attendees` VALUES (133, 'Lotti', 'Walicki', 'lwalickiw', 'lwalickiw@chicagotribune.com', 'CBXERI', '');
INSERT INTO `attendees` VALUES (134, 'Alejandrina', 'Ege', 'aegex', 'aegex@usnews.com', '6GSD4Z', '');
INSERT INTO `attendees` VALUES (135, 'Hamish', 'Reffe', 'hreffey', 'hreffey@t.co', '2AHTPV', '');
INSERT INTO `attendees` VALUES (136, 'Ilysa', 'Kirtlan', 'ikirtlanz', 'ikirtlanz@t-online.de', 'JR6ZHP', '');
INSERT INTO `attendees` VALUES (137, 'Franchot', 'Gounot', 'fgounot10', 'fgounot10@mapquest.com', '2UT25Y', '');
INSERT INTO `attendees` VALUES (138, 'Eduino', 'Owers', 'eowers11', 'eowers11@wisc.edu', 'UK0U08', '');
INSERT INTO `attendees` VALUES (139, 'Latrina', 'Smylie', 'lsmylie12', 'lsmylie12@intel.com', 'QHBUSR', '');
INSERT INTO `attendees` VALUES (140, 'Grannie', 'Scane', 'gscane13', 'gscane13@amazon.com', 'OFLD2M', '');
INSERT INTO `attendees` VALUES (141, 'Leyla', 'McKinn', 'lmckinn14', 'lmckinn14@acquirethisname.com', 'RYNFQV', '');
INSERT INTO `attendees` VALUES (142, 'Tiphany', 'Thomsen', 'tthomsen15', 'tthomsen15@apple.com', 'FVAWDX', '');
INSERT INTO `attendees` VALUES (143, 'Olga', 'Spera', 'ospera16', 'ospera16@ox.ac.uk', 'OOLAR7', '');
INSERT INTO `attendees` VALUES (144, 'Lesly', 'Haighton', 'lhaighton17', 'lhaighton17@purevolume.com', '6PWUVR', '');
INSERT INTO `attendees` VALUES (145, 'Maressa', 'Budd', 'mbudd18', 'mbudd18@ezinearticles.com', 'E201WI', '');
INSERT INTO `attendees` VALUES (146, 'Kennedy', 'Burnyate', 'kburnyate19', 'kburnyate19@ycombinator.com', 'OO1QW3', '');
INSERT INTO `attendees` VALUES (147, 'Timotheus', 'Skupinski', 'tskupinski1a', 'tskupinski1a@people.com.cn', 'BQ699Y', '');
INSERT INTO `attendees` VALUES (148, 'Birdie', 'Rase', 'brase1b', 'brase1b@ft.com', 'XRSBNI', '');
INSERT INTO `attendees` VALUES (149, 'Orelie', 'Lamberton', 'olamberton1c', 'olamberton1c@aboutads.info', 'NKFTLI', '');
INSERT INTO `attendees` VALUES (150, 'Aleece', 'Champney', 'achampney1d', 'achampney1d@sciencedirect.com', '3OF96G', '');
INSERT INTO `attendees` VALUES (151, 'Duke', 'Wethey', 'dwethey1e', 'dwethey1e@nationalgeographic.com', 'WS5GJ4', '');
INSERT INTO `attendees` VALUES (152, 'Colline', 'Jako', 'cjako1f', 'cjako1f@netscape.com', 'RH39JQ', '');
INSERT INTO `attendees` VALUES (153, 'Benjamen', 'Walak', 'bwalak1g', 'bwalak1g@unicef.org', 'U56D8I', '');
INSERT INTO `attendees` VALUES (154, 'Gannie', 'Chippendale', 'gchippendale1h', 'gchippendale1h@wikimedia.org', 'PY3C9L', '');
INSERT INTO `attendees` VALUES (155, 'Prue', 'Lowther', 'plowther1i', 'plowther1i@buzzfeed.com', '2GD2YD', '');
INSERT INTO `attendees` VALUES (156, 'Kenneth', 'Ellsworthe', 'kellsworthe1j', 'kellsworthe1j@privacy.gov.au', 'YVSTHM', '');
INSERT INTO `attendees` VALUES (157, 'Myca', 'Wigfall', 'mwigfall1k', 'mwigfall1k@wufoo.com', 'OC8GWR', '');
INSERT INTO `attendees` VALUES (158, 'Alphard', 'Mosten', 'amosten1l', 'amosten1l@sitemeter.com', 'ILC3YR', '');
INSERT INTO `attendees` VALUES (159, 'Merna', 'Biggs', 'mbiggs1m', 'mbiggs1m@webs.com', 'VPQ0SI', '');
INSERT INTO `attendees` VALUES (160, 'Norri', 'Francklyn', 'nfrancklyn1n', 'nfrancklyn1n@lycos.com', 'B8QHOW', '');
INSERT INTO `attendees` VALUES (161, 'Stewart', 'Yesenin', 'syesenin1o', 'syesenin1o@dion.ne.jp', 'QW21I9', '');
INSERT INTO `attendees` VALUES (162, 'Maggee', 'Penni', 'mpenni1p', 'mpenni1p@ocn.ne.jp', 'Q6XBTW', '');
INSERT INTO `attendees` VALUES (163, 'Netti', 'Capnerhurst', 'ncapnerhurst1q', 'ncapnerhurst1q@weibo.com', 'A51GMF', '');
INSERT INTO `attendees` VALUES (164, 'Ross', 'Agate', 'ragate1r', 'ragate1r@dion.ne.jp', 'R8WZ29', '');
INSERT INTO `attendees` VALUES (165, 'Colas', 'Amthor', 'camthor1s', 'camthor1s@usatoday.com', 'A3LCGE', '');
INSERT INTO `attendees` VALUES (166, 'Kylila', 'Artis', 'kartis1t', 'kartis1t@ezinearticles.com', 'ASNTOH', '');
INSERT INTO `attendees` VALUES (167, 'Arie', 'Symcox', 'asymcox1u', 'asymcox1u@squarespace.com', 'MJY3SS', '');
INSERT INTO `attendees` VALUES (168, 'Desiree', 'Strand', 'dstrand1v', 'dstrand1v@angelfire.com', '36USDT', '');
INSERT INTO `attendees` VALUES (169, 'Verine', 'Alabastar', 'valabastar1w', 'valabastar1w@harvard.edu', 'TK5D2N', '');
INSERT INTO `attendees` VALUES (170, 'Ranna', 'Abbott', 'rabbott1x', 'rabbott1x@usgs.gov', 'NE6GPK', '');
INSERT INTO `attendees` VALUES (171, 'Cobbie', 'Minger', 'cminger1y', 'cminger1y@guardian.co.uk', 'F7IVR1', '');
INSERT INTO `attendees` VALUES (172, 'Lorelle', 'Cuvley', 'lcuvley1z', 'lcuvley1z@pbs.org', 'OD3LHV', '');
INSERT INTO `attendees` VALUES (173, 'Sorcha', 'MacCracken', 'smaccracken20', 'smaccracken20@tinyurl.com', 'B2N8C8', '');
INSERT INTO `attendees` VALUES (174, 'Prentice', 'Whitear', 'pwhitear21', 'pwhitear21@fastcompany.com', '173XB6', '');
INSERT INTO `attendees` VALUES (175, 'Engracia', 'Burkin', 'eburkin22', 'eburkin22@blinklist.com', '50FZKO', '');
INSERT INTO `attendees` VALUES (176, 'Anitra', 'Skirvin', 'askirvin23', 'askirvin23@yolasite.com', '2USOJG', '');
INSERT INTO `attendees` VALUES (177, 'Giorgi', 'Guiraud', 'gguiraud24', 'gguiraud24@xinhuanet.com', 'M1SH4M', '');
INSERT INTO `attendees` VALUES (178, 'Christiana', 'Skene', 'cskene25', 'cskene25@dion.ne.jp', '5IDFIM', '');
INSERT INTO `attendees` VALUES (179, 'Kandy', 'Sacks', 'ksacks26', 'ksacks26@last.fm', 'T627UE', '');
INSERT INTO `attendees` VALUES (180, 'Cleavland', 'Jowitt', 'cjowitt27', 'cjowitt27@technorati.com', 'VN8HWD', '');
INSERT INTO `attendees` VALUES (181, 'Florina', 'Bruckenthal', 'fbruckenthal28', 'fbruckenthal28@friendfeed.com', '2DGK91', '');
INSERT INTO `attendees` VALUES (182, 'Phil', 'Isacoff', 'pisacoff29', 'pisacoff29@pinterest.com', '574XNL', '');
INSERT INTO `attendees` VALUES (183, 'Darill', 'Spencer', 'dspencer2a', 'dspencer2a@hugedomains.com', 'BH5XL3', '');
INSERT INTO `attendees` VALUES (184, 'Emiline', 'Minico', 'eminico2b', 'eminico2b@dell.com', '0QMMAZ', '');
INSERT INTO `attendees` VALUES (185, 'Chaddie', 'Sackur', 'csackur2c', 'csackur2c@sciencedirect.com', 'T6C0A1', '');
INSERT INTO `attendees` VALUES (186, 'Sindee', 'Woolfall', 'swoolfall2d', 'swoolfall2d@dagondesign.com', '6680YX', '');
INSERT INTO `attendees` VALUES (187, 'Dulci', 'Kingdom', 'dkingdom2e', 'dkingdom2e@umn.edu', 'HV4WDS', '');
INSERT INTO `attendees` VALUES (188, 'Ermanno', 'Searson', 'esearson2f', 'esearson2f@freewebs.com', 'FPLVYR', '');
INSERT INTO `attendees` VALUES (189, 'Max', 'Rainbird', 'mrainbird2g', 'mrainbird2g@dailymotion.com', 'VRPD03', '');
INSERT INTO `attendees` VALUES (190, 'Alexander', 'Marusyak', 'amarusyak2h', 'amarusyak2h@chron.com', 'JWY6WJ', '');
INSERT INTO `attendees` VALUES (191, 'Gino', 'Matura', 'gmatura2i', 'gmatura2i@globo.com', 'EFFU83', '');
INSERT INTO `attendees` VALUES (192, 'Quent', 'Ragborne', 'qragborne2j', 'qragborne2j@google.co.jp', 'E21O8Y', '');
INSERT INTO `attendees` VALUES (193, 'Lauraine', 'Smithyman', 'lsmithyman2k', 'lsmithyman2k@samsung.com', 'WRGF38', '');
INSERT INTO `attendees` VALUES (194, 'Shirl', 'Tabbernor', 'stabbernor2l', 'stabbernor2l@posterous.com', 'J4EI3H', '');
INSERT INTO `attendees` VALUES (195, 'Aridatha', 'Hateley', 'ahateley2m', 'ahateley2m@desdev.cn', '6RG0VT', '');
INSERT INTO `attendees` VALUES (196, 'Archibaldo', 'Boanas', 'aboanas2n', 'aboanas2n@google.com.au', 'DPBYBP', '');
INSERT INTO `attendees` VALUES (197, 'Dorisa', 'Heminsley', 'dheminsley2o', 'dheminsley2o@photobucket.com', 'PRGKEV', '');
INSERT INTO `attendees` VALUES (198, 'Benton', 'Jesson', 'bjesson2p', 'bjesson2p@comcast.net', 'ZEQCXQ', '');
INSERT INTO `attendees` VALUES (199, 'Billie', 'Windrus', 'bwindrus2q', 'bwindrus2q@istockphoto.com', 'HTPE2X', '');
INSERT INTO `attendees` VALUES (200, 'Silvie', 'Kaye', 'skaye2r', 'skaye2r@adobe.com', '35UWIU', '');

-- ----------------------------
-- Table structure for channels
-- ----------------------------
DROP TABLE IF EXISTS `channels`;
CREATE TABLE `channels`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `event_id` int(11) NOT NULL,
  `name` varchar(45) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `fk_channels_events1_idx`(`event_id`) USING BTREE,
  CONSTRAINT `fk_channels_events1` FOREIGN KEY (`event_id`) REFERENCES `events` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE = InnoDB AUTO_INCREMENT = 13 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of channels
-- ----------------------------
INSERT INTO `channels` VALUES (1, 1, 'Roadmaps to thriving societies');
INSERT INTO `channels` VALUES (2, 1, 'Skills for evolving economies');
INSERT INTO `channels` VALUES (3, 1, 'Towards self-sustaining ecosystems');
INSERT INTO `channels` VALUES (4, 2, 'What\'s new');
INSERT INTO `channels` VALUES (5, 2, 'General React');
INSERT INTO `channels` VALUES (6, 3, 'What\'s new in react');
INSERT INTO `channels` VALUES (7, 3, 'General React');
INSERT INTO `channels` VALUES (8, 4, 'Basics');
INSERT INTO `channels` VALUES (9, 4, 'Deep dive');
INSERT INTO `channels` VALUES (10, 5, 'General Angular');
INSERT INTO `channels` VALUES (11, 5, 'Testing');
INSERT INTO `channels` VALUES (12, 5, 'User Experience');

-- ----------------------------
-- Table structure for customers
-- ----------------------------
DROP TABLE IF EXISTS `customers`;
CREATE TABLE `customers`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp(0) NULL DEFAULT NULL,
  `password` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `customers_email_unique`(`email`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for event_tickets
-- ----------------------------
DROP TABLE IF EXISTS `event_tickets`;
CREATE TABLE `event_tickets`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `event_id` int(11) NOT NULL,
  `name` varchar(45) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `special_validity` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `cost` decimal(9, 2) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `fk_tickets_events_idx`(`event_id`) USING BTREE,
  CONSTRAINT `fk_tickets_events` FOREIGN KEY (`event_id`) REFERENCES `events` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE = InnoDB AUTO_INCREMENT = 18 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of event_tickets
-- ----------------------------
INSERT INTO `event_tickets` VALUES (1, 1, 'Normal', NULL, NULL);
INSERT INTO `event_tickets` VALUES (2, 1, 'Early Bird', '{\"type\":\"date\",\"date\":\"2019-06-01\"}', NULL);
INSERT INTO `event_tickets` VALUES (3, 1, 'Hotel Package', '{\"type\":\"amount\",\"amount\":50}', NULL);
INSERT INTO `event_tickets` VALUES (4, 2, 'Normal', NULL, NULL);
INSERT INTO `event_tickets` VALUES (5, 2, 'Scholarship', '{\"type\":\"amount\",\"amount\":30}', NULL);
INSERT INTO `event_tickets` VALUES (6, 3, 'Normal', NULL, NULL);
INSERT INTO `event_tickets` VALUES (7, 3, 'Scholarship', '{\"type\":\"amount\",\"amount\":35}', NULL);
INSERT INTO `event_tickets` VALUES (8, 4, 'Normal', NULL, NULL);
INSERT INTO `event_tickets` VALUES (9, 4, 'Early Bird', '{\"type\":\"date\",\"date\":\"2019-10-01\"}', NULL);
INSERT INTO `event_tickets` VALUES (10, 4, 'Sponsor', NULL, NULL);
INSERT INTO `event_tickets` VALUES (11, 5, 'Normal', NULL, NULL);
INSERT INTO `event_tickets` VALUES (12, 21, 'Front Row Seat', '{\"type\":\"amount\",\"amount\":60}', NULL);
INSERT INTO `event_tickets` VALUES (14, 21, 'Khoa Nguyen', 'null', NULL);
INSERT INTO `event_tickets` VALUES (15, 20, 'Máy giặt', '{\"type\":\"amount\",\"amount\":\"21\"}', NULL);
INSERT INTO `event_tickets` VALUES (16, 4, NULL, 'null', 0.00);
INSERT INTO `event_tickets` VALUES (17, 4, 'Count Down', '{\"type\":\"amount\",\"amount\":\"213\"}', 231.00);

-- ----------------------------
-- Table structure for events
-- ----------------------------
DROP TABLE IF EXISTS `events`;
CREATE TABLE `events`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `organizer_id` int(11) NOT NULL,
  `name` varchar(45) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `slug` varchar(45) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `date` date NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `fk_events_organizers1_idx`(`organizer_id`) USING BTREE,
  CONSTRAINT `fk_events_organizers1` FOREIGN KEY (`organizer_id`) REFERENCES `organizers` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE = InnoDB AUTO_INCREMENT = 28 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of events
-- ----------------------------
INSERT INTO `events` VALUES (1, 1, 'WorldSkills Conference', 'wsc-2019', '2019-09-23');
INSERT INTO `events` VALUES (2, 1, 'React Conf 2018', 'react-conf-2018', '2018-06-12');
INSERT INTO `events` VALUES (3, 1, 'React Conf 2019', 'react-conf-2019', '2019-10-24');
INSERT INTO `events` VALUES (4, 2, 'Vuejs Amsterdam', 'vuejs-2019', '2020-02-14');
INSERT INTO `events` VALUES (5, 2, 'ng conf', 'ng-2019', '2019-09-30');
INSERT INTO `events` VALUES (12, 1, 'Sự kiện tháng 11', 'su-kien-thang-11', '2019-12-06');
INSERT INTO `events` VALUES (13, 1, 'WorldSkills Conference 2019', 'wsc-2019', '2019-09-23');
INSERT INTO `events` VALUES (19, 4, 'Count Down', 'count-down', '2019-12-06');
INSERT INTO `events` VALUES (20, 4, 'Sự kiện tháng 12', 'su-kien-thang-12', '2019-12-14');
INSERT INTO `events` VALUES (21, 4, 'Sự kiện tháng 1-2020', 'https://thi-quoc-gia.com', '2019-12-06');
INSERT INTO `events` VALUES (25, 5, 'Sự kiện tháng 2', 'su-kien-thang-1', '2019-12-06');

-- ----------------------------
-- Table structure for migrations
-- ----------------------------
DROP TABLE IF EXISTS `migrations`;
CREATE TABLE `migrations`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for organizers
-- ----------------------------
DROP TABLE IF EXISTS `organizers`;
CREATE TABLE `organizers`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `slug` varchar(45) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `email` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `email_verified_at` timestamp(0) NULL DEFAULT NULL,
  `password` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 6 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of organizers
-- ----------------------------
INSERT INTO `organizers` VALUES (1, 'Organizerdemo1', 'demo1', 'demo1@hanoiskills.orga', NULL, '', NULL, NULL, NULL);
INSERT INTO `organizers` VALUES (2, 'Organizerdemo2', 'demo2', 'demo2@hanoiskills.org', NULL, '', NULL, NULL, NULL);
INSERT INTO `organizers` VALUES (4, 'Khoa Nguyen', 'khoa-nguyen', 'khoait109@gmail.com', NULL, '$2y$10$h9ia1esWf.X3v6RUnzROmOgQjpCKKVbYDG3ARglW3PwhT/RprxDmq', NULL, NULL, NULL);
INSERT INTO `organizers` VALUES (5, 'Khoa Nguyen', 'khoa-nguyen', 'demo1@gmail.com', NULL, '$2y$10$FpZbZ/4RljbXrXj5Q/l3tOZyMCxAvCV4OCKK1yA16RFx.YBP9UXO.', NULL, NULL, NULL);

-- ----------------------------
-- Table structure for password_resets
-- ----------------------------
DROP TABLE IF EXISTS `password_resets`;
CREATE TABLE `password_resets`  (
  `email` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  INDEX `password_resets_email_index`(`email`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for rating
-- ----------------------------
DROP TABLE IF EXISTS `rating`;
CREATE TABLE `rating`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `event_id` int(11) NOT NULL,
  `name` varchar(45) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `comment` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `created_date` datetime(0) NULL DEFAULT NULL,
  `rating` tinyint(1) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `event_id`(`event_id`) USING BTREE,
  CONSTRAINT `rating_ibfk_1` FOREIGN KEY (`event_id`) REFERENCES `events` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for registrations
-- ----------------------------
DROP TABLE IF EXISTS `registrations`;
CREATE TABLE `registrations`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `attendee_id` int(11) NOT NULL,
  `ticket_id` int(11) NOT NULL,
  `registration_time` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `fk_registrations_attendees1_idx`(`attendee_id`) USING BTREE,
  INDEX `fk_registrations_tickets1_idx`(`ticket_id`) USING BTREE,
  CONSTRAINT `fk_registrations_attendees1` FOREIGN KEY (`attendee_id`) REFERENCES `attendees` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_registrations_tickets1` FOREIGN KEY (`ticket_id`) REFERENCES `event_tickets` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE = InnoDB AUTO_INCREMENT = 703 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of registrations
-- ----------------------------
INSERT INTO `registrations` VALUES (1, 51, 5, '2019-03-11 11:37:12');
INSERT INTO `registrations` VALUES (2, 31, 5, '2019-06-28 23:33:25');
INSERT INTO `registrations` VALUES (3, 62, 5, '2019-05-24 21:46:26');
INSERT INTO `registrations` VALUES (4, 13, 5, '2019-05-21 11:37:52');
INSERT INTO `registrations` VALUES (5, 77, 5, '2019-02-09 04:30:16');
INSERT INTO `registrations` VALUES (6, 82, 5, '2019-05-14 07:50:21');
INSERT INTO `registrations` VALUES (7, 68, 5, '2019-04-02 21:12:11');
INSERT INTO `registrations` VALUES (8, 27, 5, '2019-01-16 07:54:35');
INSERT INTO `registrations` VALUES (9, 49, 5, '2019-01-03 11:07:43');
INSERT INTO `registrations` VALUES (10, 16, 5, '2019-04-08 16:27:38');
INSERT INTO `registrations` VALUES (11, 45, 5, '2019-03-08 05:47:44');
INSERT INTO `registrations` VALUES (12, 63, 5, '2019-05-11 11:15:30');
INSERT INTO `registrations` VALUES (13, 30, 5, '2019-02-01 20:30:43');
INSERT INTO `registrations` VALUES (14, 78, 5, '2019-04-05 18:22:24');
INSERT INTO `registrations` VALUES (15, 67, 5, '2019-04-14 20:25:45');
INSERT INTO `registrations` VALUES (16, 40, 5, '2019-05-12 00:59:58');
INSERT INTO `registrations` VALUES (17, 83, 5, '2019-03-12 05:54:22');
INSERT INTO `registrations` VALUES (18, 79, 5, '2019-03-21 01:36:47');
INSERT INTO `registrations` VALUES (19, 17, 5, '2019-06-21 05:01:50');
INSERT INTO `registrations` VALUES (20, 81, 5, '2019-02-04 05:33:33');
INSERT INTO `registrations` VALUES (21, 75, 5, '2019-05-10 18:20:30');
INSERT INTO `registrations` VALUES (22, 8, 5, '2019-05-18 20:59:44');
INSERT INTO `registrations` VALUES (23, 46, 5, '2019-03-13 14:09:22');
INSERT INTO `registrations` VALUES (24, 64, 5, '2019-03-07 16:28:14');
INSERT INTO `registrations` VALUES (25, 80, 5, '2019-06-19 15:24:30');
INSERT INTO `registrations` VALUES (26, 25, 5, '2019-03-06 21:06:00');
INSERT INTO `registrations` VALUES (27, 53, 5, '2019-03-08 18:17:24');
INSERT INTO `registrations` VALUES (28, 48, 5, '2019-04-30 18:03:42');
INSERT INTO `registrations` VALUES (29, 84, 5, '2019-03-19 12:03:45');
INSERT INTO `registrations` VALUES (30, 69, 5, '2019-05-06 02:31:14');
INSERT INTO `registrations` VALUES (31, 100, 7, '2019-02-22 15:24:17');
INSERT INTO `registrations` VALUES (32, 28, 7, '2019-05-25 04:29:21');
INSERT INTO `registrations` VALUES (33, 5, 7, '2019-07-22 16:22:22');
INSERT INTO `registrations` VALUES (34, 9, 7, '2019-02-15 15:17:17');
INSERT INTO `registrations` VALUES (35, 23, 7, '2019-03-16 05:11:31');
INSERT INTO `registrations` VALUES (36, 72, 7, '2019-02-26 00:29:48');
INSERT INTO `registrations` VALUES (37, 84, 7, '2019-07-26 07:48:31');
INSERT INTO `registrations` VALUES (38, 80, 7, '2019-05-02 23:22:58');
INSERT INTO `registrations` VALUES (39, 3, 7, '2019-06-10 10:34:35');
INSERT INTO `registrations` VALUES (40, 51, 7, '2019-06-14 08:36:37');
INSERT INTO `registrations` VALUES (41, 22, 7, '2019-01-10 00:38:04');
INSERT INTO `registrations` VALUES (42, 8, 7, '2019-03-01 15:22:13');
INSERT INTO `registrations` VALUES (43, 98, 7, '2019-01-09 13:03:25');
INSERT INTO `registrations` VALUES (44, 41, 7, '2019-04-15 03:12:24');
INSERT INTO `registrations` VALUES (45, 77, 7, '2019-06-12 16:34:34');
INSERT INTO `registrations` VALUES (46, 70, 7, '2019-03-02 17:24:41');
INSERT INTO `registrations` VALUES (47, 38, 7, '2019-02-13 16:55:27');
INSERT INTO `registrations` VALUES (48, 78, 7, '2019-02-11 04:11:51');
INSERT INTO `registrations` VALUES (49, 83, 7, '2019-06-24 11:50:52');
INSERT INTO `registrations` VALUES (50, 65, 7, '2019-05-07 10:37:13');
INSERT INTO `registrations` VALUES (51, 92, 7, '2019-05-23 18:24:45');
INSERT INTO `registrations` VALUES (52, 67, 7, '2019-04-18 03:20:42');
INSERT INTO `registrations` VALUES (53, 53, 7, '2019-02-23 16:22:41');
INSERT INTO `registrations` VALUES (54, 74, 7, '2019-02-14 00:04:15');
INSERT INTO `registrations` VALUES (55, 50, 7, '2019-03-17 10:10:28');
INSERT INTO `registrations` VALUES (56, 33, 7, '2019-04-07 05:32:26');
INSERT INTO `registrations` VALUES (57, 55, 7, '2019-07-26 03:49:28');
INSERT INTO `registrations` VALUES (58, 39, 7, '2019-04-15 11:39:16');
INSERT INTO `registrations` VALUES (59, 82, 7, '2019-07-18 16:47:10');
INSERT INTO `registrations` VALUES (60, 52, 7, '2019-05-15 17:53:53');
INSERT INTO `registrations` VALUES (61, 95, 7, '2019-01-04 07:19:41');
INSERT INTO `registrations` VALUES (62, 37, 7, '2019-06-20 23:39:05');
INSERT INTO `registrations` VALUES (63, 76, 7, '2019-05-02 18:27:19');
INSERT INTO `registrations` VALUES (64, 15, 7, '2019-04-23 22:36:26');
INSERT INTO `registrations` VALUES (65, 2, 1, '2019-04-11 11:33:19');
INSERT INTO `registrations` VALUES (66, 2, 11, '2019-02-23 13:45:17');
INSERT INTO `registrations` VALUES (67, 15, 12, '2019-04-01 13:49:53');
INSERT INTO `registrations` VALUES (68, 42, 12, '2019-04-11 15:30:02');
INSERT INTO `registrations` VALUES (69, 44, 12, '2019-03-19 16:30:34');
INSERT INTO `registrations` VALUES (70, 50, 12, '2019-07-26 18:33:12');
INSERT INTO `registrations` VALUES (71, 45, 12, '2019-06-20 01:50:34');
INSERT INTO `registrations` VALUES (72, 93, 12, '2019-01-16 06:37:14');
INSERT INTO `registrations` VALUES (73, 58, 12, '2019-06-04 20:42:56');
INSERT INTO `registrations` VALUES (74, 41, 12, '2019-06-09 10:02:44');
INSERT INTO `registrations` VALUES (75, 29, 12, '2019-02-24 15:17:26');
INSERT INTO `registrations` VALUES (76, 54, 12, '2019-05-26 10:50:12');
INSERT INTO `registrations` VALUES (77, 56, 12, '2019-06-26 17:47:52');
INSERT INTO `registrations` VALUES (78, 31, 12, '2019-02-02 01:23:53');
INSERT INTO `registrations` VALUES (79, 20, 12, '2019-07-18 22:38:54');
INSERT INTO `registrations` VALUES (80, 9, 12, '2019-06-20 00:57:53');
INSERT INTO `registrations` VALUES (81, 83, 12, '2019-02-04 01:44:21');
INSERT INTO `registrations` VALUES (82, 8, 12, '2019-01-02 22:05:50');
INSERT INTO `registrations` VALUES (83, 60, 12, '2019-06-25 16:34:38');
INSERT INTO `registrations` VALUES (84, 11, 12, '2019-02-16 16:05:47');
INSERT INTO `registrations` VALUES (85, 94, 12, '2019-03-16 00:07:03');
INSERT INTO `registrations` VALUES (86, 43, 12, '2019-03-14 09:57:38');
INSERT INTO `registrations` VALUES (87, 85, 12, '2019-04-28 08:50:45');
INSERT INTO `registrations` VALUES (88, 48, 12, '2019-01-18 14:05:21');
INSERT INTO `registrations` VALUES (89, 66, 12, '2019-03-16 19:38:12');
INSERT INTO `registrations` VALUES (90, 49, 12, '2019-03-13 08:00:25');
INSERT INTO `registrations` VALUES (91, 28, 12, '2019-05-07 10:35:55');
INSERT INTO `registrations` VALUES (92, 14, 12, '2019-03-17 08:44:43');
INSERT INTO `registrations` VALUES (93, 10, 12, '2019-02-18 08:56:51');
INSERT INTO `registrations` VALUES (94, 12, 12, '2019-06-01 15:55:28');
INSERT INTO `registrations` VALUES (95, 70, 12, '2019-05-25 10:09:31');
INSERT INTO `registrations` VALUES (96, 46, 12, '2019-05-01 11:10:45');
INSERT INTO `registrations` VALUES (97, 63, 12, '2019-02-10 18:22:26');
INSERT INTO `registrations` VALUES (98, 62, 12, '2019-05-11 14:58:29');
INSERT INTO `registrations` VALUES (99, 67, 12, '2019-07-16 03:46:20');
INSERT INTO `registrations` VALUES (100, 13, 12, '2019-05-06 09:36:50');
INSERT INTO `registrations` VALUES (101, 17, 12, '2019-04-06 01:24:10');
INSERT INTO `registrations` VALUES (102, 33, 2, '2019-05-15 20:04:15');
INSERT INTO `registrations` VALUES (103, 18, 2, '2019-05-14 06:03:06');
INSERT INTO `registrations` VALUES (104, 76, 1, '2019-06-14 06:39:24');
INSERT INTO `registrations` VALUES (105, 79, 2, '2019-01-18 18:48:06');
INSERT INTO `registrations` VALUES (106, 59, 2, '2019-02-06 04:21:32');
INSERT INTO `registrations` VALUES (107, 52, 2, '2019-02-18 22:26:49');
INSERT INTO `registrations` VALUES (108, 69, 2, '2019-01-14 04:19:11');
INSERT INTO `registrations` VALUES (109, 99, 2, '2019-04-16 11:47:38');
INSERT INTO `registrations` VALUES (110, 84, 2, '2019-04-09 19:41:45');
INSERT INTO `registrations` VALUES (111, 88, 2, '2019-01-18 03:18:20');
INSERT INTO `registrations` VALUES (112, 49, 2, '2019-04-02 09:00:30');
INSERT INTO `registrations` VALUES (113, 98, 3, '2019-04-20 20:36:25');
INSERT INTO `registrations` VALUES (114, 39, 2, '2019-03-26 08:13:15');
INSERT INTO `registrations` VALUES (115, 60, 2, '2019-05-28 04:58:18');
INSERT INTO `registrations` VALUES (116, 6, 1, '2019-06-14 08:04:34');
INSERT INTO `registrations` VALUES (117, 48, 2, '2019-02-22 13:07:13');
INSERT INTO `registrations` VALUES (118, 46, 2, '2019-03-27 08:26:57');
INSERT INTO `registrations` VALUES (119, 30, 1, '2019-04-28 15:41:15');
INSERT INTO `registrations` VALUES (120, 85, 2, '2019-02-21 21:15:30');
INSERT INTO `registrations` VALUES (121, 9, 1, '2019-06-23 12:14:45');
INSERT INTO `registrations` VALUES (122, 80, 1, '2019-04-07 00:01:55');
INSERT INTO `registrations` VALUES (123, 50, 2, '2019-01-26 08:15:58');
INSERT INTO `registrations` VALUES (124, 62, 2, '2019-03-21 13:47:39');
INSERT INTO `registrations` VALUES (125, 54, 2, '2019-04-15 17:43:26');
INSERT INTO `registrations` VALUES (126, 199, 1, '2019-05-26 08:27:51');
INSERT INTO `registrations` VALUES (127, 7, 2, '2019-01-05 14:35:39');
INSERT INTO `registrations` VALUES (128, 89, 2, '2019-03-16 04:19:37');
INSERT INTO `registrations` VALUES (129, 53, 1, '2019-06-28 00:01:34');
INSERT INTO `registrations` VALUES (130, 51, 2, '2019-05-04 14:33:07');
INSERT INTO `registrations` VALUES (131, 4, 1, '2019-06-27 00:43:56');
INSERT INTO `registrations` VALUES (132, 94, 2, '2019-04-25 08:43:48');
INSERT INTO `registrations` VALUES (133, 95, 3, '2019-05-23 15:21:41');
INSERT INTO `registrations` VALUES (134, 31, 2, '2019-01-23 07:20:26');
INSERT INTO `registrations` VALUES (135, 65, 2, '2019-04-21 01:27:42');
INSERT INTO `registrations` VALUES (136, 96, 2, '2019-04-10 01:41:21');
INSERT INTO `registrations` VALUES (137, 42, 2, '2019-04-13 02:18:00');
INSERT INTO `registrations` VALUES (138, 78, 1, '2019-05-18 09:48:03');
INSERT INTO `registrations` VALUES (139, 26, 2, '2019-04-18 17:39:53');
INSERT INTO `registrations` VALUES (140, 34, 1, '2019-05-27 21:51:13');
INSERT INTO `registrations` VALUES (141, 91, 1, '2019-06-12 17:55:32');
INSERT INTO `registrations` VALUES (142, 24, 1, '2019-05-03 16:55:32');
INSERT INTO `registrations` VALUES (143, 93, 2, '2019-03-05 12:39:08');
INSERT INTO `registrations` VALUES (144, 67, 1, '2019-06-21 02:05:11');
INSERT INTO `registrations` VALUES (145, 57, 2, '2019-04-23 16:39:32');
INSERT INTO `registrations` VALUES (146, 11, 1, '2019-06-18 04:51:31');
INSERT INTO `registrations` VALUES (147, 19, 1, '2019-06-14 10:23:52');
INSERT INTO `registrations` VALUES (148, 56, 3, '2019-05-24 07:26:45');
INSERT INTO `registrations` VALUES (149, 86, 1, '2019-06-05 17:07:57');
INSERT INTO `registrations` VALUES (150, 71, 1, '2019-05-04 21:32:34');
INSERT INTO `registrations` VALUES (151, 58, 2, '2019-04-01 11:50:55');
INSERT INTO `registrations` VALUES (152, 70, 2, '2019-04-10 14:15:58');
INSERT INTO `registrations` VALUES (153, 68, 2, '2019-04-26 07:29:03');
INSERT INTO `registrations` VALUES (154, 8, 1, '2019-05-27 01:52:31');
INSERT INTO `registrations` VALUES (155, 87, 2, '2019-02-03 03:41:34');
INSERT INTO `registrations` VALUES (156, 16, 3, '2019-04-01 07:46:03');
INSERT INTO `registrations` VALUES (157, 29, 2, '2019-02-20 07:55:16');
INSERT INTO `registrations` VALUES (158, 40, 2, '2019-03-07 11:21:35');
INSERT INTO `registrations` VALUES (159, 74, 2, '2019-04-12 11:38:14');
INSERT INTO `registrations` VALUES (160, 17, 2, '2019-03-27 12:27:24');
INSERT INTO `registrations` VALUES (161, 13, 2, '2019-05-10 09:50:46');
INSERT INTO `registrations` VALUES (162, 20, 2, '2019-01-28 05:20:50');
INSERT INTO `registrations` VALUES (163, 47, 2, '2019-02-14 01:26:38');
INSERT INTO `registrations` VALUES (164, 77, 2, '2019-03-16 19:36:01');
INSERT INTO `registrations` VALUES (165, 10, 3, '2019-05-07 03:23:16');
INSERT INTO `registrations` VALUES (166, 97, 2, '2019-03-28 00:34:04');
INSERT INTO `registrations` VALUES (167, 28, 2, '2019-04-26 03:28:14');
INSERT INTO `registrations` VALUES (168, 22, 1, '2019-06-17 21:45:55');
INSERT INTO `registrations` VALUES (169, 36, 2, '2019-04-16 15:48:51');
INSERT INTO `registrations` VALUES (170, 43, 2, '2019-04-25 16:38:16');
INSERT INTO `registrations` VALUES (171, 21, 1, '2019-06-02 21:02:45');
INSERT INTO `registrations` VALUES (172, 14, 3, '2019-03-28 11:11:46');
INSERT INTO `registrations` VALUES (173, 27, 1, '2019-06-19 06:10:37');
INSERT INTO `registrations` VALUES (174, 32, 2, '2019-01-10 12:39:16');
INSERT INTO `registrations` VALUES (175, 81, 2, '2019-03-24 21:16:53');
INSERT INTO `registrations` VALUES (176, 90, 1, '2019-04-14 06:25:17');
INSERT INTO `registrations` VALUES (177, 66, 1, '2019-06-25 03:44:10');
INSERT INTO `registrations` VALUES (178, 75, 3, '2019-01-01 03:33:49');
INSERT INTO `registrations` VALUES (179, 35, 2, '2019-04-22 05:03:20');
INSERT INTO `registrations` VALUES (180, 45, 2, '2019-03-12 17:33:40');
INSERT INTO `registrations` VALUES (181, 37, 2, '2019-01-22 12:20:30');
INSERT INTO `registrations` VALUES (182, 44, 3, '2019-04-19 21:37:31');
INSERT INTO `registrations` VALUES (183, 41, 2, '2019-01-26 19:02:11');
INSERT INTO `registrations` VALUES (184, 82, 2, '2019-01-04 14:42:30');
INSERT INTO `registrations` VALUES (185, 72, 2, '2019-03-11 17:33:15');
INSERT INTO `registrations` VALUES (186, 25, 2, '2019-04-07 01:00:00');
INSERT INTO `registrations` VALUES (187, 15, 2, '2019-02-15 12:53:06');
INSERT INTO `registrations` VALUES (188, 63, 2, '2019-01-14 17:58:42');
INSERT INTO `registrations` VALUES (189, 92, 1, '2019-04-03 13:33:52');
INSERT INTO `registrations` VALUES (190, 83, 2, '2019-01-03 14:43:33');
INSERT INTO `registrations` VALUES (191, 73, 2, '2019-02-04 18:14:01');
INSERT INTO `registrations` VALUES (192, 64, 2, '2019-03-23 16:31:45');
INSERT INTO `registrations` VALUES (193, 55, 2, '2019-01-15 12:40:41');
INSERT INTO `registrations` VALUES (194, 5, 2, '2019-04-27 09:17:51');
INSERT INTO `registrations` VALUES (195, 61, 1, '2019-05-07 15:21:54');
INSERT INTO `registrations` VALUES (196, 23, 1, '2019-05-26 04:38:15');
INSERT INTO `registrations` VALUES (197, 172, 2, '2019-02-20 11:38:01');
INSERT INTO `registrations` VALUES (198, 157, 1, '2019-05-21 12:50:58');
INSERT INTO `registrations` VALUES (199, 113, 2, '2019-01-04 01:13:32');
INSERT INTO `registrations` VALUES (200, 196, 2, '2019-02-09 19:39:15');
INSERT INTO `registrations` VALUES (201, 186, 2, '2019-03-21 22:45:01');
INSERT INTO `registrations` VALUES (202, 189, 2, '2019-02-02 21:46:48');
INSERT INTO `registrations` VALUES (203, 148, 2, '2019-02-11 14:49:20');
INSERT INTO `registrations` VALUES (204, 135, 2, '2019-02-09 20:27:04');
INSERT INTO `registrations` VALUES (205, 174, 2, '2019-01-18 07:49:42');
INSERT INTO `registrations` VALUES (206, 122, 3, '2019-05-04 19:17:21');
INSERT INTO `registrations` VALUES (207, 162, 2, '2019-01-20 16:50:03');
INSERT INTO `registrations` VALUES (208, 120, 2, '2019-03-11 02:43:40');
INSERT INTO `registrations` VALUES (209, 175, 2, '2019-02-12 09:49:27');
INSERT INTO `registrations` VALUES (210, 159, 2, '2019-04-23 16:00:57');
INSERT INTO `registrations` VALUES (211, 198, 2, '2019-01-13 20:00:29');
INSERT INTO `registrations` VALUES (212, 178, 2, '2019-03-13 05:52:31');
INSERT INTO `registrations` VALUES (213, 103, 1, '2019-05-07 06:03:23');
INSERT INTO `registrations` VALUES (214, 105, 2, '2019-03-03 03:02:34');
INSERT INTO `registrations` VALUES (215, 184, 2, '2019-01-27 04:07:49');
INSERT INTO `registrations` VALUES (216, 187, 1, '2019-01-27 08:04:47');
INSERT INTO `registrations` VALUES (217, 115, 3, '2019-05-21 18:29:13');
INSERT INTO `registrations` VALUES (218, 121, 2, '2019-02-18 05:51:35');
INSERT INTO `registrations` VALUES (219, 133, 1, '2019-04-06 02:04:28');
INSERT INTO `registrations` VALUES (220, 108, 1, '2019-05-23 10:18:40');
INSERT INTO `registrations` VALUES (221, 109, 1, '2019-04-02 11:58:13');
INSERT INTO `registrations` VALUES (222, 107, 2, '2019-01-17 03:57:16');
INSERT INTO `registrations` VALUES (223, 110, 2, '2019-01-16 03:04:21');
INSERT INTO `registrations` VALUES (224, 188, 2, '2019-01-28 02:24:12');
INSERT INTO `registrations` VALUES (225, 177, 3, '2019-06-11 11:19:13');
INSERT INTO `registrations` VALUES (226, 182, 1, '2019-05-14 12:22:25');
INSERT INTO `registrations` VALUES (227, 192, 3, '2019-05-18 05:09:50');
INSERT INTO `registrations` VALUES (228, 197, 1, '2019-06-13 22:35:33');
INSERT INTO `registrations` VALUES (229, 123, 2, '2019-04-25 18:43:05');
INSERT INTO `registrations` VALUES (230, 171, 1, '2019-04-22 10:54:18');
INSERT INTO `registrations` VALUES (231, 153, 1, '2019-06-22 02:03:40');
INSERT INTO `registrations` VALUES (232, 165, 2, '2019-01-22 19:16:41');
INSERT INTO `registrations` VALUES (233, 164, 1, '2019-04-09 14:19:46');
INSERT INTO `registrations` VALUES (234, 147, 2, '2019-02-03 15:26:20');
INSERT INTO `registrations` VALUES (235, 143, 2, '2019-02-21 13:07:01');
INSERT INTO `registrations` VALUES (236, 195, 2, '2019-02-16 01:53:51');
INSERT INTO `registrations` VALUES (237, 145, 1, '2019-06-08 10:09:53');
INSERT INTO `registrations` VALUES (238, 117, 2, '2019-01-23 17:29:05');
INSERT INTO `registrations` VALUES (239, 170, 3, '2019-03-10 05:15:12');
INSERT INTO `registrations` VALUES (240, 144, 1, '2019-03-18 19:31:44');
INSERT INTO `registrations` VALUES (241, 136, 2, '2019-01-23 16:05:02');
INSERT INTO `registrations` VALUES (242, 141, 2, '2019-03-23 13:17:35');
INSERT INTO `registrations` VALUES (243, 176, 1, '2019-06-19 11:25:07');
INSERT INTO `registrations` VALUES (244, 155, 3, '2019-05-07 12:18:27');
INSERT INTO `registrations` VALUES (245, 154, 1, '2019-05-04 04:26:00');
INSERT INTO `registrations` VALUES (246, 151, 2, '2019-04-26 03:09:35');
INSERT INTO `registrations` VALUES (247, 146, 1, '2019-06-26 14:47:05');
INSERT INTO `registrations` VALUES (248, 129, 2, '2019-03-28 14:03:04');
INSERT INTO `registrations` VALUES (249, 130, 2, '2019-01-05 12:37:36');
INSERT INTO `registrations` VALUES (250, 183, 2, '2019-01-21 10:43:40');
INSERT INTO `registrations` VALUES (251, 131, 1, '2019-06-07 16:32:46');
INSERT INTO `registrations` VALUES (252, 140, 3, '2019-03-09 16:32:37');
INSERT INTO `registrations` VALUES (253, 119, 2, '2019-01-08 01:02:46');
INSERT INTO `registrations` VALUES (254, 158, 1, '2019-06-12 11:52:19');
INSERT INTO `registrations` VALUES (255, 102, 3, '2019-06-13 03:16:56');
INSERT INTO `registrations` VALUES (256, 179, 1, '2019-04-25 11:53:10');
INSERT INTO `registrations` VALUES (257, 116, 2, '2019-03-20 17:34:16');
INSERT INTO `registrations` VALUES (258, 161, 2, '2019-01-15 11:37:07');
INSERT INTO `registrations` VALUES (259, 156, 3, '2019-06-28 17:50:23');
INSERT INTO `registrations` VALUES (260, 180, 2, '2019-02-25 08:25:57');
INSERT INTO `registrations` VALUES (261, 152, 2, '2019-02-05 07:41:04');
INSERT INTO `registrations` VALUES (262, 169, 2, '2019-02-25 14:44:33');
INSERT INTO `registrations` VALUES (263, 160, 2, '2019-02-22 12:38:37');
INSERT INTO `registrations` VALUES (264, 190, 1, '2019-06-06 12:57:23');
INSERT INTO `registrations` VALUES (265, 106, 1, '2019-05-11 12:26:40');
INSERT INTO `registrations` VALUES (266, 118, 2, '2019-02-05 17:13:14');
INSERT INTO `registrations` VALUES (267, 114, 3, '2019-05-06 11:07:50');
INSERT INTO `registrations` VALUES (268, 139, 1, '2019-05-23 06:37:23');
INSERT INTO `registrations` VALUES (269, 173, 1, '2019-05-07 02:00:35');
INSERT INTO `registrations` VALUES (270, 185, 3, '2019-04-17 08:49:17');
INSERT INTO `registrations` VALUES (271, 191, 1, '2019-06-01 16:13:52');
INSERT INTO `registrations` VALUES (272, 124, 2, '2019-04-21 20:07:44');
INSERT INTO `registrations` VALUES (273, 138, 2, '2019-03-14 01:53:24');
INSERT INTO `registrations` VALUES (274, 167, 1, '2019-04-11 09:10:50');
INSERT INTO `registrations` VALUES (275, 101, 2, '2019-01-20 22:40:28');
INSERT INTO `registrations` VALUES (276, 168, 2, '2019-03-26 00:12:55');
INSERT INTO `registrations` VALUES (277, 134, 2, '2019-02-25 13:23:06');
INSERT INTO `registrations` VALUES (278, 132, 3, '2019-05-14 07:44:17');
INSERT INTO `registrations` VALUES (279, 128, 1, '2019-06-17 02:09:44');
INSERT INTO `registrations` VALUES (280, 193, 1, '2019-05-14 09:08:55');
INSERT INTO `registrations` VALUES (281, 190, 4, '2019-05-17 18:08:53');
INSERT INTO `registrations` VALUES (282, 176, 4, '2019-04-26 15:28:10');
INSERT INTO `registrations` VALUES (283, 100, 4, '2019-03-19 19:52:49');
INSERT INTO `registrations` VALUES (284, 104, 4, '2019-06-16 04:27:34');
INSERT INTO `registrations` VALUES (285, 133, 4, '2019-05-10 09:44:54');
INSERT INTO `registrations` VALUES (286, 146, 4, '2019-03-19 22:15:05');
INSERT INTO `registrations` VALUES (287, 119, 4, '2019-05-27 09:11:24');
INSERT INTO `registrations` VALUES (288, 173, 4, '2019-06-28 09:19:09');
INSERT INTO `registrations` VALUES (289, 169, 4, '2019-04-11 07:32:48');
INSERT INTO `registrations` VALUES (290, 111, 4, '2019-06-24 08:45:03');
INSERT INTO `registrations` VALUES (291, 99, 4, '2019-03-10 10:38:31');
INSERT INTO `registrations` VALUES (292, 152, 4, '2019-03-22 08:11:26');
INSERT INTO `registrations` VALUES (293, 85, 4, '2019-03-23 11:41:47');
INSERT INTO `registrations` VALUES (294, 170, 4, '2019-05-18 21:03:17');
INSERT INTO `registrations` VALUES (295, 182, 4, '2019-05-02 12:14:27');
INSERT INTO `registrations` VALUES (296, 93, 4, '2019-06-06 06:08:09');
INSERT INTO `registrations` VALUES (297, 138, 4, '2019-05-27 16:38:52');
INSERT INTO `registrations` VALUES (298, 135, 4, '2019-04-26 20:36:03');
INSERT INTO `registrations` VALUES (299, 153, 4, '2019-04-25 11:26:34');
INSERT INTO `registrations` VALUES (300, 174, 4, '2019-04-11 03:43:12');
INSERT INTO `registrations` VALUES (301, 181, 4, '2019-03-18 08:08:12');
INSERT INTO `registrations` VALUES (302, 175, 4, '2019-05-24 12:16:26');
INSERT INTO `registrations` VALUES (303, 101, 4, '2019-02-09 18:48:20');
INSERT INTO `registrations` VALUES (304, 134, 4, '2019-02-23 06:44:16');
INSERT INTO `registrations` VALUES (305, 163, 4, '2019-04-12 06:11:57');
INSERT INTO `registrations` VALUES (306, 167, 4, '2019-06-09 14:36:01');
INSERT INTO `registrations` VALUES (307, 156, 4, '2019-02-27 17:23:52');
INSERT INTO `registrations` VALUES (308, 183, 4, '2019-04-09 09:03:38');
INSERT INTO `registrations` VALUES (309, 185, 4, '2019-03-28 21:48:16');
INSERT INTO `registrations` VALUES (310, 154, 4, '2019-02-14 16:51:18');
INSERT INTO `registrations` VALUES (311, 103, 4, '2019-01-23 11:56:13');
INSERT INTO `registrations` VALUES (312, 124, 4, '2019-03-18 02:57:22');
INSERT INTO `registrations` VALUES (313, 90, 4, '2019-03-28 07:26:31');
INSERT INTO `registrations` VALUES (314, 193, 4, '2019-01-19 09:20:14');
INSERT INTO `registrations` VALUES (315, 180, 4, '2019-05-20 21:01:15');
INSERT INTO `registrations` VALUES (316, 164, 4, '2019-06-14 14:03:55');
INSERT INTO `registrations` VALUES (317, 127, 4, '2019-03-23 19:48:53');
INSERT INTO `registrations` VALUES (318, 128, 4, '2019-03-24 22:12:31');
INSERT INTO `registrations` VALUES (319, 108, 4, '2019-04-27 16:09:11');
INSERT INTO `registrations` VALUES (320, 143, 4, '2019-02-11 03:54:10');
INSERT INTO `registrations` VALUES (321, 141, 4, '2019-01-27 17:17:33');
INSERT INTO `registrations` VALUES (322, 118, 4, '2019-06-21 20:06:50');
INSERT INTO `registrations` VALUES (323, 91, 4, '2019-01-26 00:48:57');
INSERT INTO `registrations` VALUES (324, 144, 4, '2019-01-06 07:37:19');
INSERT INTO `registrations` VALUES (325, 199, 4, '2019-03-10 04:35:00');
INSERT INTO `registrations` VALUES (326, 98, 4, '2019-01-28 12:19:21');
INSERT INTO `registrations` VALUES (327, 114, 4, '2019-04-04 13:48:07');
INSERT INTO `registrations` VALUES (328, 192, 4, '2019-05-27 03:50:14');
INSERT INTO `registrations` VALUES (329, 120, 4, '2019-01-10 20:35:57');
INSERT INTO `registrations` VALUES (330, 142, 4, '2019-01-09 01:44:40');
INSERT INTO `registrations` VALUES (331, 196, 4, '2019-05-13 00:54:53');
INSERT INTO `registrations` VALUES (332, 155, 4, '2019-01-07 06:11:24');
INSERT INTO `registrations` VALUES (333, 96, 4, '2019-03-23 06:09:12');
INSERT INTO `registrations` VALUES (334, 137, 4, '2019-02-25 12:07:49');
INSERT INTO `registrations` VALUES (335, 116, 4, '2019-01-09 08:10:25');
INSERT INTO `registrations` VALUES (336, 110, 4, '2019-01-21 03:26:34');
INSERT INTO `registrations` VALUES (337, 186, 4, '2019-05-22 17:25:05');
INSERT INTO `registrations` VALUES (338, 88, 4, '2019-03-19 04:29:07');
INSERT INTO `registrations` VALUES (339, 113, 4, '2019-03-07 22:41:17');
INSERT INTO `registrations` VALUES (340, 117, 4, '2019-01-21 05:00:04');
INSERT INTO `registrations` VALUES (341, 139, 4, '2019-03-16 10:09:21');
INSERT INTO `registrations` VALUES (342, 105, 4, '2019-06-21 06:04:32');
INSERT INTO `registrations` VALUES (343, 168, 4, '2019-04-07 22:07:48');
INSERT INTO `registrations` VALUES (344, 122, 4, '2019-05-09 20:21:52');
INSERT INTO `registrations` VALUES (345, 115, 4, '2019-06-03 15:46:04');
INSERT INTO `registrations` VALUES (346, 151, 4, '2019-01-13 17:19:31');
INSERT INTO `registrations` VALUES (347, 198, 4, '2019-04-01 16:13:21');
INSERT INTO `registrations` VALUES (348, 187, 4, '2019-02-06 16:36:05');
INSERT INTO `registrations` VALUES (349, 136, 4, '2019-04-02 01:26:08');
INSERT INTO `registrations` VALUES (350, 161, 4, '2019-06-05 20:58:55');
INSERT INTO `registrations` VALUES (351, 89, 4, '2019-04-03 00:27:25');
INSERT INTO `registrations` VALUES (352, 166, 4, '2019-01-19 09:04:50');
INSERT INTO `registrations` VALUES (353, 121, 4, '2019-01-04 11:56:40');
INSERT INTO `registrations` VALUES (354, 191, 4, '2019-02-10 01:01:09');
INSERT INTO `registrations` VALUES (355, 147, 4, '2019-03-01 19:19:28');
INSERT INTO `registrations` VALUES (356, 125, 4, '2019-06-24 02:39:09');
INSERT INTO `registrations` VALUES (357, 132, 4, '2019-04-09 15:26:06');
INSERT INTO `registrations` VALUES (358, 95, 4, '2019-06-24 02:48:07');
INSERT INTO `registrations` VALUES (359, 177, 4, '2019-02-15 00:19:28');
INSERT INTO `registrations` VALUES (360, 106, 4, '2019-02-19 10:31:54');
INSERT INTO `registrations` VALUES (361, 160, 4, '2019-04-25 13:58:55');
INSERT INTO `registrations` VALUES (362, 130, 4, '2019-02-09 12:15:39');
INSERT INTO `registrations` VALUES (363, 178, 4, '2019-06-22 07:40:49');
INSERT INTO `registrations` VALUES (364, 197, 4, '2019-02-02 12:26:09');
INSERT INTO `registrations` VALUES (365, 94, 4, '2019-04-14 15:03:57');
INSERT INTO `registrations` VALUES (366, 188, 4, '2019-04-24 06:25:57');
INSERT INTO `registrations` VALUES (367, 123, 4, '2019-06-20 16:19:25');
INSERT INTO `registrations` VALUES (368, 184, 4, '2019-04-04 07:04:05');
INSERT INTO `registrations` VALUES (369, 131, 4, '2019-01-16 03:13:06');
INSERT INTO `registrations` VALUES (370, 165, 4, '2019-02-14 11:36:57');
INSERT INTO `registrations` VALUES (371, 145, 4, '2019-05-16 18:06:36');
INSERT INTO `registrations` VALUES (372, 126, 4, '2019-01-23 20:53:28');
INSERT INTO `registrations` VALUES (373, 171, 4, '2019-03-18 13:56:45');
INSERT INTO `registrations` VALUES (374, 140, 4, '2019-05-19 13:16:34');
INSERT INTO `registrations` VALUES (375, 109, 4, '2019-03-20 01:00:16');
INSERT INTO `registrations` VALUES (376, 112, 4, '2019-02-19 06:15:19');
INSERT INTO `registrations` VALUES (377, 179, 4, '2019-01-04 13:21:19');
INSERT INTO `registrations` VALUES (378, 149, 4, '2019-04-02 15:42:16');
INSERT INTO `registrations` VALUES (379, 172, 4, '2019-03-06 16:19:52');
INSERT INTO `registrations` VALUES (380, 159, 4, '2019-02-01 07:15:57');
INSERT INTO `registrations` VALUES (381, 195, 4, '2019-06-27 15:43:27');
INSERT INTO `registrations` VALUES (382, 194, 4, '2019-03-01 14:33:06');
INSERT INTO `registrations` VALUES (383, 150, 4, '2019-02-09 00:57:20');
INSERT INTO `registrations` VALUES (384, 158, 6, '2019-04-26 15:51:42');
INSERT INTO `registrations` VALUES (385, 156, 6, '2019-03-01 11:33:44');
INSERT INTO `registrations` VALUES (386, 199, 6, '2019-02-21 13:26:16');
INSERT INTO `registrations` VALUES (387, 189, 6, '2019-01-22 19:13:47');
INSERT INTO `registrations` VALUES (388, 193, 6, '2019-03-14 01:34:09');
INSERT INTO `registrations` VALUES (389, 159, 6, '2019-04-07 22:12:43');
INSERT INTO `registrations` VALUES (390, 106, 6, '2019-01-07 21:04:50');
INSERT INTO `registrations` VALUES (391, 137, 6, '2019-05-03 19:09:21');
INSERT INTO `registrations` VALUES (392, 121, 6, '2019-05-07 02:26:19');
INSERT INTO `registrations` VALUES (393, 198, 6, '2019-03-16 17:11:19');
INSERT INTO `registrations` VALUES (394, 143, 6, '2019-06-08 13:50:55');
INSERT INTO `registrations` VALUES (395, 146, 6, '2019-03-18 18:39:02');
INSERT INTO `registrations` VALUES (396, 101, 6, '2019-04-14 05:07:33');
INSERT INTO `registrations` VALUES (397, 139, 6, '2019-02-02 02:53:44');
INSERT INTO `registrations` VALUES (398, 136, 6, '2019-01-11 07:28:23');
INSERT INTO `registrations` VALUES (399, 190, 6, '2019-06-16 04:49:02');
INSERT INTO `registrations` VALUES (400, 127, 6, '2019-05-22 08:19:46');
INSERT INTO `registrations` VALUES (401, 119, 6, '2019-03-09 08:23:07');
INSERT INTO `registrations` VALUES (402, 126, 6, '2019-05-04 22:54:53');
INSERT INTO `registrations` VALUES (403, 181, 6, '2019-03-14 20:41:19');
INSERT INTO `registrations` VALUES (404, 194, 6, '2019-01-12 09:18:55');
INSERT INTO `registrations` VALUES (405, 134, 6, '2019-01-24 09:06:16');
INSERT INTO `registrations` VALUES (406, 142, 6, '2019-04-12 21:48:40');
INSERT INTO `registrations` VALUES (407, 180, 6, '2019-03-12 04:33:54');
INSERT INTO `registrations` VALUES (408, 108, 6, '2019-06-08 03:42:51');
INSERT INTO `registrations` VALUES (409, 157, 6, '2019-04-12 20:47:24');
INSERT INTO `registrations` VALUES (410, 167, 6, '2019-05-12 17:42:05');
INSERT INTO `registrations` VALUES (411, 122, 6, '2019-04-19 19:35:56');
INSERT INTO `registrations` VALUES (412, 197, 6, '2019-03-17 12:44:44');
INSERT INTO `registrations` VALUES (413, 144, 6, '2019-01-05 15:10:41');
INSERT INTO `registrations` VALUES (414, 178, 6, '2019-05-16 04:39:12');
INSERT INTO `registrations` VALUES (415, 196, 6, '2019-03-13 20:27:11');
INSERT INTO `registrations` VALUES (416, 152, 6, '2019-05-03 15:03:34');
INSERT INTO `registrations` VALUES (417, 123, 6, '2019-06-20 10:22:21');
INSERT INTO `registrations` VALUES (418, 150, 6, '2019-04-01 10:02:43');
INSERT INTO `registrations` VALUES (419, 161, 6, '2019-02-02 19:36:33');
INSERT INTO `registrations` VALUES (420, 172, 6, '2019-03-01 14:33:19');
INSERT INTO `registrations` VALUES (421, 176, 6, '2019-02-02 20:22:28');
INSERT INTO `registrations` VALUES (422, 107, 6, '2019-04-18 18:49:49');
INSERT INTO `registrations` VALUES (423, 112, 6, '2019-05-13 00:53:56');
INSERT INTO `registrations` VALUES (424, 147, 6, '2019-03-03 22:58:35');
INSERT INTO `registrations` VALUES (425, 182, 6, '2019-04-22 09:22:18');
INSERT INTO `registrations` VALUES (426, 124, 6, '2019-05-13 21:28:13');
INSERT INTO `registrations` VALUES (427, 148, 6, '2019-02-19 12:05:39');
INSERT INTO `registrations` VALUES (428, 154, 6, '2019-02-10 11:30:15');
INSERT INTO `registrations` VALUES (429, 183, 6, '2019-01-16 07:28:02');
INSERT INTO `registrations` VALUES (430, 135, 6, '2019-02-09 09:11:50');
INSERT INTO `registrations` VALUES (431, 133, 6, '2019-05-24 14:31:08');
INSERT INTO `registrations` VALUES (432, 140, 6, '2019-03-14 15:44:46');
INSERT INTO `registrations` VALUES (433, 171, 6, '2019-06-02 18:41:45');
INSERT INTO `registrations` VALUES (434, 125, 6, '2019-02-09 12:29:48');
INSERT INTO `registrations` VALUES (435, 177, 6, '2019-04-15 06:33:24');
INSERT INTO `registrations` VALUES (436, 110, 6, '2019-06-22 16:43:40');
INSERT INTO `registrations` VALUES (437, 118, 6, '2019-05-26 10:18:20');
INSERT INTO `registrations` VALUES (438, 184, 6, '2019-03-01 20:01:07');
INSERT INTO `registrations` VALUES (439, 115, 6, '2019-01-09 09:49:34');
INSERT INTO `registrations` VALUES (440, 129, 6, '2019-02-22 06:23:04');
INSERT INTO `registrations` VALUES (441, 116, 6, '2019-06-21 11:20:00');
INSERT INTO `registrations` VALUES (442, 169, 6, '2019-04-25 21:26:15');
INSERT INTO `registrations` VALUES (443, 170, 6, '2019-02-18 12:15:47');
INSERT INTO `registrations` VALUES (444, 131, 6, '2019-01-06 13:52:13');
INSERT INTO `registrations` VALUES (445, 164, 6, '2019-05-17 12:49:38');
INSERT INTO `registrations` VALUES (446, 130, 6, '2019-03-20 17:01:35');
INSERT INTO `registrations` VALUES (447, 186, 6, '2019-01-12 02:42:46');
INSERT INTO `registrations` VALUES (448, 138, 6, '2019-02-20 07:41:30');
INSERT INTO `registrations` VALUES (449, 162, 6, '2019-05-19 19:53:22');
INSERT INTO `registrations` VALUES (450, 145, 6, '2019-02-10 08:46:17');
INSERT INTO `registrations` VALUES (451, 114, 6, '2019-04-11 04:40:28');
INSERT INTO `registrations` VALUES (452, 132, 6, '2019-02-02 03:13:07');
INSERT INTO `registrations` VALUES (453, 185, 6, '2019-05-18 04:06:07');
INSERT INTO `registrations` VALUES (454, 102, 6, '2019-01-02 12:26:12');
INSERT INTO `registrations` VALUES (455, 111, 6, '2019-06-14 17:33:58');
INSERT INTO `registrations` VALUES (456, 149, 6, '2019-02-15 03:56:46');
INSERT INTO `registrations` VALUES (457, 160, 6, '2019-04-04 20:33:13');
INSERT INTO `registrations` VALUES (458, 151, 6, '2019-06-17 02:10:24');
INSERT INTO `registrations` VALUES (459, 155, 6, '2019-06-10 17:22:04');
INSERT INTO `registrations` VALUES (460, 120, 6, '2019-06-27 09:43:42');
INSERT INTO `registrations` VALUES (461, 163, 6, '2019-03-10 15:11:29');
INSERT INTO `registrations` VALUES (462, 168, 6, '2019-02-16 17:34:20');
INSERT INTO `registrations` VALUES (463, 166, 6, '2019-01-20 03:58:36');
INSERT INTO `registrations` VALUES (464, 153, 6, '2019-04-09 04:45:48');
INSERT INTO `registrations` VALUES (465, 117, 6, '2019-03-03 09:11:20');
INSERT INTO `registrations` VALUES (466, 179, 6, '2019-03-03 07:47:05');
INSERT INTO `registrations` VALUES (467, 188, 6, '2019-03-26 10:24:17');
INSERT INTO `registrations` VALUES (468, 103, 6, '2019-05-23 09:39:46');
INSERT INTO `registrations` VALUES (469, 187, 6, '2019-03-12 01:04:26');
INSERT INTO `registrations` VALUES (470, 175, 6, '2019-01-03 08:06:05');
INSERT INTO `registrations` VALUES (471, 173, 6, '2019-01-04 02:22:44');
INSERT INTO `registrations` VALUES (472, 191, 6, '2019-02-17 17:11:18');
INSERT INTO `registrations` VALUES (473, 113, 6, '2019-03-07 17:36:15');
INSERT INTO `registrations` VALUES (474, 195, 6, '2019-05-19 06:25:49');
INSERT INTO `registrations` VALUES (475, 141, 6, '2019-05-04 13:46:53');
INSERT INTO `registrations` VALUES (476, 105, 6, '2019-05-21 13:34:26');
INSERT INTO `registrations` VALUES (477, 165, 6, '2019-06-10 01:56:58');
INSERT INTO `registrations` VALUES (478, 174, 6, '2019-02-02 01:45:54');
INSERT INTO `registrations` VALUES (479, 109, 11, '2019-06-07 18:39:09');
INSERT INTO `registrations` VALUES (480, 129, 11, '2019-06-04 00:01:24');
INSERT INTO `registrations` VALUES (481, 134, 11, '2019-04-25 09:00:47');
INSERT INTO `registrations` VALUES (482, 107, 11, '2019-04-15 17:20:32');
INSERT INTO `registrations` VALUES (483, 184, 11, '2019-01-12 15:50:49');
INSERT INTO `registrations` VALUES (484, 161, 11, '2019-06-07 15:15:08');
INSERT INTO `registrations` VALUES (485, 157, 11, '2019-05-02 07:34:27');
INSERT INTO `registrations` VALUES (486, 95, 11, '2019-04-24 09:32:12');
INSERT INTO `registrations` VALUES (487, 114, 11, '2019-01-16 02:12:06');
INSERT INTO `registrations` VALUES (488, 145, 11, '2019-01-24 00:25:45');
INSERT INTO `registrations` VALUES (489, 123, 11, '2019-02-22 14:22:42');
INSERT INTO `registrations` VALUES (490, 199, 11, '2019-02-12 17:23:04');
INSERT INTO `registrations` VALUES (491, 102, 11, '2019-02-24 01:44:05');
INSERT INTO `registrations` VALUES (492, 172, 11, '2019-06-05 19:51:06');
INSERT INTO `registrations` VALUES (493, 136, 11, '2019-03-16 02:50:06');
INSERT INTO `registrations` VALUES (494, 177, 11, '2019-05-10 13:42:07');
INSERT INTO `registrations` VALUES (495, 151, 11, '2019-04-02 13:01:06');
INSERT INTO `registrations` VALUES (496, 158, 11, '2019-01-19 18:58:22');
INSERT INTO `registrations` VALUES (497, 126, 11, '2019-03-25 02:21:14');
INSERT INTO `registrations` VALUES (498, 190, 11, '2019-03-05 08:06:25');
INSERT INTO `registrations` VALUES (499, 146, 11, '2019-05-24 12:04:49');
INSERT INTO `registrations` VALUES (500, 131, 11, '2019-02-17 12:18:49');
INSERT INTO `registrations` VALUES (501, 133, 11, '2019-03-20 06:45:41');
INSERT INTO `registrations` VALUES (502, 147, 11, '2019-05-02 12:34:06');
INSERT INTO `registrations` VALUES (503, 135, 11, '2019-02-01 02:57:00');
INSERT INTO `registrations` VALUES (504, 197, 11, '2019-02-23 05:47:56');
INSERT INTO `registrations` VALUES (505, 171, 11, '2019-03-14 20:09:44');
INSERT INTO `registrations` VALUES (506, 176, 11, '2019-01-01 10:48:10');
INSERT INTO `registrations` VALUES (507, 196, 11, '2019-03-26 07:19:03');
INSERT INTO `registrations` VALUES (508, 153, 11, '2019-04-14 04:20:38');
INSERT INTO `registrations` VALUES (509, 150, 11, '2019-06-26 17:37:46');
INSERT INTO `registrations` VALUES (510, 189, 11, '2019-05-17 07:02:45');
INSERT INTO `registrations` VALUES (511, 112, 11, '2019-02-06 07:56:18');
INSERT INTO `registrations` VALUES (512, 187, 11, '2019-03-08 02:49:45');
INSERT INTO `registrations` VALUES (513, 164, 11, '2019-05-07 15:36:01');
INSERT INTO `registrations` VALUES (514, 148, 11, '2019-05-09 07:39:31');
INSERT INTO `registrations` VALUES (515, 103, 11, '2019-06-15 14:20:50');
INSERT INTO `registrations` VALUES (516, 121, 11, '2019-04-27 06:29:27');
INSERT INTO `registrations` VALUES (517, 191, 11, '2019-04-19 02:10:44');
INSERT INTO `registrations` VALUES (518, 101, 11, '2019-04-04 16:18:01');
INSERT INTO `registrations` VALUES (519, 122, 11, '2019-06-06 00:55:38');
INSERT INTO `registrations` VALUES (520, 173, 11, '2019-03-05 02:33:03');
INSERT INTO `registrations` VALUES (521, 198, 11, '2019-03-10 16:12:57');
INSERT INTO `registrations` VALUES (522, 186, 11, '2019-01-18 17:49:29');
INSERT INTO `registrations` VALUES (523, 192, 11, '2019-04-09 07:08:10');
INSERT INTO `registrations` VALUES (524, 111, 11, '2019-06-01 15:26:02');
INSERT INTO `registrations` VALUES (525, 137, 11, '2019-04-27 06:45:41');
INSERT INTO `registrations` VALUES (526, 139, 11, '2019-02-10 12:23:26');
INSERT INTO `registrations` VALUES (527, 115, 11, '2019-01-28 20:42:38');
INSERT INTO `registrations` VALUES (528, 117, 11, '2019-04-04 12:43:31');
INSERT INTO `registrations` VALUES (529, 116, 11, '2019-04-13 22:12:44');
INSERT INTO `registrations` VALUES (530, 144, 11, '2019-06-24 09:29:58');
INSERT INTO `registrations` VALUES (531, 127, 11, '2019-05-01 13:11:56');
INSERT INTO `registrations` VALUES (532, 193, 11, '2019-04-17 09:19:28');
INSERT INTO `registrations` VALUES (533, 195, 11, '2019-03-20 01:36:09');
INSERT INTO `registrations` VALUES (534, 113, 11, '2019-01-03 07:47:35');
INSERT INTO `registrations` VALUES (535, 168, 11, '2019-05-09 18:06:46');
INSERT INTO `registrations` VALUES (536, 154, 11, '2019-02-11 11:45:03');
INSERT INTO `registrations` VALUES (537, 96, 11, '2019-03-01 16:53:38');
INSERT INTO `registrations` VALUES (538, 120, 11, '2019-06-08 06:04:48');
INSERT INTO `registrations` VALUES (539, 166, 11, '2019-06-22 22:44:10');
INSERT INTO `registrations` VALUES (540, 182, 11, '2019-02-25 20:58:35');
INSERT INTO `registrations` VALUES (541, 143, 11, '2019-01-11 13:11:43');
INSERT INTO `registrations` VALUES (542, 163, 11, '2019-01-27 06:10:17');
INSERT INTO `registrations` VALUES (543, 167, 11, '2019-01-13 04:21:28');
INSERT INTO `registrations` VALUES (544, 140, 11, '2019-03-24 18:39:46');
INSERT INTO `registrations` VALUES (545, 119, 11, '2019-04-24 13:10:45');
INSERT INTO `registrations` VALUES (546, 98, 11, '2019-01-13 01:51:01');
INSERT INTO `registrations` VALUES (547, 183, 11, '2019-05-15 13:35:08');
INSERT INTO `registrations` VALUES (548, 178, 11, '2019-04-25 08:15:30');
INSERT INTO `registrations` VALUES (549, 106, 11, '2019-06-25 07:06:29');
INSERT INTO `registrations` VALUES (550, 125, 11, '2019-06-11 18:25:35');
INSERT INTO `registrations` VALUES (551, 155, 11, '2019-03-16 15:02:11');
INSERT INTO `registrations` VALUES (552, 100, 11, '2019-01-20 16:53:38');
INSERT INTO `registrations` VALUES (553, 165, 11, '2019-04-18 17:42:41');
INSERT INTO `registrations` VALUES (554, 179, 11, '2019-01-19 04:51:42');
INSERT INTO `registrations` VALUES (555, 130, 11, '2019-05-11 22:57:50');
INSERT INTO `registrations` VALUES (556, 128, 11, '2019-02-28 06:54:09');
INSERT INTO `registrations` VALUES (557, 97, 11, '2019-05-28 03:56:22');
INSERT INTO `registrations` VALUES (558, 105, 11, '2019-01-13 15:03:14');
INSERT INTO `registrations` VALUES (559, 142, 11, '2019-02-19 15:05:05');
INSERT INTO `registrations` VALUES (560, 181, 11, '2019-01-03 16:37:31');
INSERT INTO `registrations` VALUES (561, 160, 11, '2019-01-28 04:55:33');
INSERT INTO `registrations` VALUES (562, 180, 11, '2019-02-28 14:18:54');
INSERT INTO `registrations` VALUES (563, 118, 11, '2019-05-06 17:39:36');
INSERT INTO `registrations` VALUES (564, 162, 11, '2019-03-09 09:54:25');
INSERT INTO `registrations` VALUES (565, 149, 11, '2019-04-18 06:14:36');
INSERT INTO `registrations` VALUES (566, 170, 11, '2019-04-22 12:33:04');
INSERT INTO `registrations` VALUES (567, 174, 11, '2019-01-22 06:47:36');
INSERT INTO `registrations` VALUES (568, 110, 11, '2019-04-23 20:07:13');
INSERT INTO `registrations` VALUES (569, 99, 11, '2019-02-06 14:25:45');
INSERT INTO `registrations` VALUES (570, 188, 11, '2019-02-17 06:10:56');
INSERT INTO `registrations` VALUES (571, 194, 11, '2019-04-25 04:14:09');
INSERT INTO `registrations` VALUES (572, 185, 11, '2019-05-14 19:42:34');
INSERT INTO `registrations` VALUES (573, 169, 11, '2019-03-06 04:36:58');
INSERT INTO `registrations` VALUES (574, 108, 11, '2019-04-04 04:42:09');
INSERT INTO `registrations` VALUES (575, 152, 11, '2019-01-01 13:51:54');
INSERT INTO `registrations` VALUES (576, 124, 11, '2019-06-10 02:00:14');
INSERT INTO `registrations` VALUES (577, 104, 11, '2019-02-19 16:35:39');
INSERT INTO `registrations` VALUES (578, 175, 11, '2019-05-09 15:30:00');
INSERT INTO `registrations` VALUES (579, 169, 9, '2019-04-27 10:57:50');
INSERT INTO `registrations` VALUES (580, 126, 9, '2019-04-06 18:31:23');
INSERT INTO `registrations` VALUES (581, 115, 9, '2019-05-27 08:46:36');
INSERT INTO `registrations` VALUES (582, 176, 9, '2019-01-08 16:05:56');
INSERT INTO `registrations` VALUES (583, 36, 9, '2019-01-16 17:02:53');
INSERT INTO `registrations` VALUES (584, 175, 8, '2019-03-05 19:21:07');
INSERT INTO `registrations` VALUES (585, 161, 9, '2019-03-25 01:09:25');
INSERT INTO `registrations` VALUES (586, 42, 9, '2019-02-16 11:24:08');
INSERT INTO `registrations` VALUES (587, 83, 10, '2019-05-15 18:22:37');
INSERT INTO `registrations` VALUES (588, 80, 9, '2019-06-22 05:45:42');
INSERT INTO `registrations` VALUES (589, 77, 9, '2019-04-04 21:49:46');
INSERT INTO `registrations` VALUES (590, 121, 9, '2019-05-03 00:46:45');
INSERT INTO `registrations` VALUES (591, 181, 9, '2019-04-12 00:54:57');
INSERT INTO `registrations` VALUES (592, 32, 8, '2019-04-08 06:00:51');
INSERT INTO `registrations` VALUES (593, 134, 9, '2019-05-13 19:46:48');
INSERT INTO `registrations` VALUES (594, 129, 9, '2019-06-07 06:05:57');
INSERT INTO `registrations` VALUES (595, 136, 9, '2019-01-02 13:27:15');
INSERT INTO `registrations` VALUES (596, 40, 9, '2019-05-09 10:08:11');
INSERT INTO `registrations` VALUES (597, 124, 9, '2019-05-04 15:57:31');
INSERT INTO `registrations` VALUES (598, 44, 9, '2019-04-20 12:00:01');
INSERT INTO `registrations` VALUES (599, 180, 9, '2019-03-27 05:42:00');
INSERT INTO `registrations` VALUES (600, 86, 9, '2019-05-22 16:29:26');
INSERT INTO `registrations` VALUES (601, 150, 9, '2019-03-26 16:26:03');
INSERT INTO `registrations` VALUES (602, 15, 8, '2019-02-11 07:45:33');
INSERT INTO `registrations` VALUES (603, 156, 9, '2019-06-18 13:07:14');
INSERT INTO `registrations` VALUES (604, 13, 9, '2019-02-12 01:11:05');
INSERT INTO `registrations` VALUES (605, 22, 9, '2019-01-20 06:44:48');
INSERT INTO `registrations` VALUES (606, 116, 9, '2019-04-13 17:03:21');
INSERT INTO `registrations` VALUES (607, 74, 9, '2019-02-23 00:44:43');
INSERT INTO `registrations` VALUES (608, 64, 9, '2019-01-26 14:38:44');
INSERT INTO `registrations` VALUES (609, 109, 9, '2019-04-26 05:24:39');
INSERT INTO `registrations` VALUES (610, 104, 10, '2019-01-22 19:16:09');
INSERT INTO `registrations` VALUES (611, 66, 9, '2019-01-28 22:11:28');
INSERT INTO `registrations` VALUES (612, 113, 9, '2019-04-24 08:31:09');
INSERT INTO `registrations` VALUES (613, 20, 9, '2019-01-21 12:42:47');
INSERT INTO `registrations` VALUES (614, 137, 9, '2019-05-15 00:23:31');
INSERT INTO `registrations` VALUES (615, 110, 9, '2019-02-23 18:00:52');
INSERT INTO `registrations` VALUES (616, 177, 9, '2019-01-26 22:52:32');
INSERT INTO `registrations` VALUES (617, 119, 8, '2019-04-25 05:03:46');
INSERT INTO `registrations` VALUES (618, 118, 9, '2019-03-02 12:18:21');
INSERT INTO `registrations` VALUES (619, 151, 9, '2019-03-01 11:15:46');
INSERT INTO `registrations` VALUES (620, 154, 9, '2019-02-15 14:58:06');
INSERT INTO `registrations` VALUES (621, 153, 8, '2019-06-23 00:12:02');
INSERT INTO `registrations` VALUES (622, 53, 9, '2019-05-21 10:09:24');
INSERT INTO `registrations` VALUES (623, 112, 9, '2019-05-23 00:27:55');
INSERT INTO `registrations` VALUES (624, 84, 9, '2019-03-21 03:33:31');
INSERT INTO `registrations` VALUES (625, 54, 9, '2019-01-11 08:09:02');
INSERT INTO `registrations` VALUES (626, 51, 9, '2019-04-17 17:23:33');
INSERT INTO `registrations` VALUES (627, 170, 9, '2019-06-24 00:15:48');
INSERT INTO `registrations` VALUES (628, 71, 9, '2019-02-24 20:40:41');
INSERT INTO `registrations` VALUES (629, 61, 9, '2019-04-12 03:03:56');
INSERT INTO `registrations` VALUES (630, 67, 9, '2019-01-18 07:47:53');
INSERT INTO `registrations` VALUES (631, 89, 9, '2019-01-02 08:17:07');
INSERT INTO `registrations` VALUES (632, 65, 9, '2019-05-15 05:10:06');
INSERT INTO `registrations` VALUES (633, 167, 9, '2019-05-06 11:52:53');
INSERT INTO `registrations` VALUES (634, 76, 9, '2019-04-11 14:26:05');
INSERT INTO `registrations` VALUES (635, 120, 8, '2019-02-28 04:24:33');
INSERT INTO `registrations` VALUES (636, 155, 9, '2019-01-17 17:02:23');
INSERT INTO `registrations` VALUES (637, 182, 9, '2019-01-22 13:10:06');
INSERT INTO `registrations` VALUES (638, 190, 10, '2019-03-11 04:27:55');
INSERT INTO `registrations` VALUES (639, 178, 9, '2019-03-25 14:18:35');
INSERT INTO `registrations` VALUES (640, 108, 9, '2019-05-02 19:39:31');
INSERT INTO `registrations` VALUES (641, 106, 9, '2019-02-09 21:50:36');
INSERT INTO `registrations` VALUES (642, 159, 9, '2019-02-02 21:27:49');
INSERT INTO `registrations` VALUES (643, 90, 9, '2019-03-09 08:20:08');
INSERT INTO `registrations` VALUES (644, 60, 9, '2019-02-14 10:16:47');
INSERT INTO `registrations` VALUES (645, 56, 9, '2019-03-07 20:30:22');
INSERT INTO `registrations` VALUES (646, 148, 9, '2019-02-01 22:07:26');
INSERT INTO `registrations` VALUES (647, 25, 9, '2019-06-22 19:30:51');
INSERT INTO `registrations` VALUES (648, 62, 9, '2019-01-28 13:14:18');
INSERT INTO `registrations` VALUES (649, 168, 8, '2019-06-25 14:55:43');
INSERT INTO `registrations` VALUES (650, 85, 9, '2019-05-23 18:10:04');
INSERT INTO `registrations` VALUES (651, 138, 9, '2019-01-27 15:43:49');
INSERT INTO `registrations` VALUES (652, 184, 9, '2019-04-20 08:52:32');
INSERT INTO `registrations` VALUES (653, 31, 9, '2019-01-02 13:17:37');
INSERT INTO `registrations` VALUES (654, 57, 9, '2019-06-11 20:06:30');
INSERT INTO `registrations` VALUES (655, 72, 9, '2019-01-24 21:41:33');
INSERT INTO `registrations` VALUES (656, 46, 9, '2019-05-10 20:55:20');
INSERT INTO `registrations` VALUES (657, 45, 9, '2019-06-25 12:43:48');
INSERT INTO `registrations` VALUES (658, 133, 9, '2019-06-21 14:09:36');
INSERT INTO `registrations` VALUES (659, 101, 9, '2019-04-15 13:12:33');
INSERT INTO `registrations` VALUES (660, 59, 10, '2019-04-10 00:21:34');
INSERT INTO `registrations` VALUES (661, 114, 9, '2019-03-06 21:47:06');
INSERT INTO `registrations` VALUES (662, 123, 8, '2019-03-14 10:09:14');
INSERT INTO `registrations` VALUES (663, 163, 8, '2019-05-27 18:48:33');
INSERT INTO `registrations` VALUES (664, 52, 9, '2019-01-12 20:30:14');
INSERT INTO `registrations` VALUES (665, 105, 8, '2019-06-10 09:49:08');
INSERT INTO `registrations` VALUES (666, 37, 9, '2019-03-27 22:14:52');
INSERT INTO `registrations` VALUES (667, 103, 9, '2019-06-11 03:26:45');
INSERT INTO `registrations` VALUES (668, 98, 9, '2019-02-26 02:30:43');
INSERT INTO `registrations` VALUES (669, 28, 9, '2019-06-26 12:42:45');
INSERT INTO `registrations` VALUES (670, 117, 9, '2019-02-14 13:58:52');
INSERT INTO `registrations` VALUES (671, 47, 9, '2019-03-22 14:17:02');
INSERT INTO `registrations` VALUES (672, 10, 9, '2019-03-08 02:54:39');
INSERT INTO `registrations` VALUES (673, 55, 9, '2019-02-18 08:06:34');
INSERT INTO `registrations` VALUES (674, 147, 9, '2019-05-01 20:52:23');
INSERT INTO `registrations` VALUES (675, 21, 9, '2019-05-14 18:31:10');
INSERT INTO `registrations` VALUES (676, 19, 9, '2019-06-27 06:31:47');
INSERT INTO `registrations` VALUES (677, 174, 9, '2019-02-09 04:51:22');
INSERT INTO `registrations` VALUES (678, 198, 9, '2019-06-02 16:16:53');
INSERT INTO `registrations` VALUES (679, 131, 9, '2019-03-17 08:44:31');
INSERT INTO `registrations` VALUES (680, 160, 10, '2019-03-21 05:16:27');
INSERT INTO `registrations` VALUES (681, 158, 9, '2019-02-28 09:48:44');
INSERT INTO `registrations` VALUES (682, 95, 9, '2019-06-03 04:41:50');
INSERT INTO `registrations` VALUES (683, 127, 9, '2019-05-09 06:54:54');
INSERT INTO `registrations` VALUES (684, 63, 8, '2019-03-01 01:12:18');
INSERT INTO `registrations` VALUES (685, 97, 9, '2019-01-27 14:08:35');
INSERT INTO `registrations` VALUES (686, 11, 9, '2019-05-09 09:39:38');
INSERT INTO `registrations` VALUES (687, 186, 9, '2019-01-08 10:26:00');
INSERT INTO `registrations` VALUES (688, 195, 9, '2019-01-08 07:13:07');
INSERT INTO `registrations` VALUES (689, 189, 9, '2019-06-13 01:04:16');
INSERT INTO `registrations` VALUES (690, 142, 9, '2019-03-13 15:47:42');
INSERT INTO `registrations` VALUES (691, 149, 9, '2019-02-17 06:25:36');
INSERT INTO `registrations` VALUES (692, 162, 9, '2019-04-24 08:35:22');
INSERT INTO `registrations` VALUES (693, 81, 10, '2019-04-18 03:33:43');
INSERT INTO `registrations` VALUES (694, 35, 9, '2019-06-06 15:35:43');
INSERT INTO `registrations` VALUES (695, 141, 9, '2019-02-27 02:45:02');
INSERT INTO `registrations` VALUES (696, 192, 8, '2019-04-01 10:49:32');
INSERT INTO `registrations` VALUES (697, 122, 9, '2019-01-05 17:29:40');
INSERT INTO `registrations` VALUES (698, 58, 9, '2019-05-24 21:24:45');
INSERT INTO `registrations` VALUES (699, 18, 9, '2019-06-23 11:30:00');
INSERT INTO `registrations` VALUES (700, 41, 9, '2019-06-04 17:09:37');
INSERT INTO `registrations` VALUES (701, 38, 9, '2019-06-26 17:42:32');
INSERT INTO `registrations` VALUES (702, 87, 10, '2019-01-16 06:51:07');

-- ----------------------------
-- Table structure for rooms
-- ----------------------------
DROP TABLE IF EXISTS `rooms`;
CREATE TABLE `rooms`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `channel_id` int(11) NOT NULL,
  `name` varchar(45) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `capacity` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `fk_rooms_channels1_idx`(`channel_id`) USING BTREE,
  CONSTRAINT `fk_rooms_channels1` FOREIGN KEY (`channel_id`) REFERENCES `channels` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE = InnoDB AUTO_INCREMENT = 17 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of rooms
-- ----------------------------
INSERT INTO `rooms` VALUES (1, 1, 'Main hall', 380);
INSERT INTO `rooms` VALUES (2, 1, 'Room 1.1', 220);
INSERT INTO `rooms` VALUES (3, 2, 'Room 1.3', 210);
INSERT INTO `rooms` VALUES (4, 2, 'Room 2.1', 60);
INSERT INTO `rooms` VALUES (5, 3, 'Room 3.1', 190);
INSERT INTO `rooms` VALUES (6, 4, 'Main room', 250);
INSERT INTO `rooms` VALUES (7, 5, 'Side room', 150);
INSERT INTO `rooms` VALUES (8, 6, 'Theater', 350);
INSERT INTO `rooms` VALUES (9, 7, 'Room 3', 250);
INSERT INTO `rooms` VALUES (10, 8, 'Room 1', 214);
INSERT INTO `rooms` VALUES (11, 8, 'Room 2', 185);
INSERT INTO `rooms` VALUES (12, 9, 'Room 3', 73);
INSERT INTO `rooms` VALUES (13, 10, 'Main room', 268);
INSERT INTO `rooms` VALUES (14, 10, 'Room 1.2', 106);
INSERT INTO `rooms` VALUES (15, 11, 'Room 1.3', 115);
INSERT INTO `rooms` VALUES (16, 12, 'Room 1.4', 167);

-- ----------------------------
-- Table structure for session_registrations
-- ----------------------------
DROP TABLE IF EXISTS `session_registrations`;
CREATE TABLE `session_registrations`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `registration_id` int(11) NOT NULL,
  `session_id` int(11) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `fk_registrations_has_sessions_sessions1_idx`(`session_id`) USING BTREE,
  INDEX `fk_registrations_has_sessions_registrations1_idx`(`registration_id`) USING BTREE,
  CONSTRAINT `fk_registrations_has_sessions_registrations1` FOREIGN KEY (`registration_id`) REFERENCES `registrations` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_registrations_has_sessions_sessions1` FOREIGN KEY (`session_id`) REFERENCES `sessions` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE = InnoDB AUTO_INCREMENT = 875 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of session_registrations
-- ----------------------------
INSERT INTO `session_registrations` VALUES (1, 65, 6);
INSERT INTO `session_registrations` VALUES (2, 65, 11);
INSERT INTO `session_registrations` VALUES (3, 249, 4);
INSERT INTO `session_registrations` VALUES (4, 120, 4);
INSERT INTO `session_registrations` VALUES (5, 155, 4);
INSERT INTO `session_registrations` VALUES (6, 251, 4);
INSERT INTO `session_registrations` VALUES (7, 257, 4);
INSERT INTO `session_registrations` VALUES (8, 179, 4);
INSERT INTO `session_registrations` VALUES (9, 110, 4);
INSERT INTO `session_registrations` VALUES (10, 163, 4);
INSERT INTO `session_registrations` VALUES (11, 240, 4);
INSERT INTO `session_registrations` VALUES (12, 216, 4);
INSERT INTO `session_registrations` VALUES (13, 169, 4);
INSERT INTO `session_registrations` VALUES (14, 111, 4);
INSERT INTO `session_registrations` VALUES (15, 274, 4);
INSERT INTO `session_registrations` VALUES (16, 200, 4);
INSERT INTO `session_registrations` VALUES (17, 132, 4);
INSERT INTO `session_registrations` VALUES (18, 136, 4);
INSERT INTO `session_registrations` VALUES (19, 237, 4);
INSERT INTO `session_registrations` VALUES (20, 253, 4);
INSERT INTO `session_registrations` VALUES (21, 160, 4);
INSERT INTO `session_registrations` VALUES (22, 121, 4);
INSERT INTO `session_registrations` VALUES (23, 157, 4);
INSERT INTO `session_registrations` VALUES (24, 180, 4);
INSERT INTO `session_registrations` VALUES (25, 275, 4);
INSERT INTO `session_registrations` VALUES (26, 116, 4);
INSERT INTO `session_registrations` VALUES (27, 148, 4);
INSERT INTO `session_registrations` VALUES (28, 214, 4);
INSERT INTO `session_registrations` VALUES (29, 190, 4);
INSERT INTO `session_registrations` VALUES (30, 146, 4);
INSERT INTO `session_registrations` VALUES (31, 247, 4);
INSERT INTO `session_registrations` VALUES (32, 229, 4);
INSERT INTO `session_registrations` VALUES (33, 267, 4);
INSERT INTO `session_registrations` VALUES (34, 195, 4);
INSERT INTO `session_registrations` VALUES (35, 208, 4);
INSERT INTO `session_registrations` VALUES (36, 258, 4);
INSERT INTO `session_registrations` VALUES (37, 199, 4);
INSERT INTO `session_registrations` VALUES (38, 194, 4);
INSERT INTO `session_registrations` VALUES (39, 113, 4);
INSERT INTO `session_registrations` VALUES (40, 234, 4);
INSERT INTO `session_registrations` VALUES (41, 182, 4);
INSERT INTO `session_registrations` VALUES (42, 210, 4);
INSERT INTO `session_registrations` VALUES (43, 191, 4);
INSERT INTO `session_registrations` VALUES (44, 226, 4);
INSERT INTO `session_registrations` VALUES (45, 222, 4);
INSERT INTO `session_registrations` VALUES (46, 172, 4);
INSERT INTO `session_registrations` VALUES (47, 142, 4);
INSERT INTO `session_registrations` VALUES (48, 198, 4);
INSERT INTO `session_registrations` VALUES (49, 129, 4);
INSERT INTO `session_registrations` VALUES (50, 235, 4);
INSERT INTO `session_registrations` VALUES (51, 276, 4);
INSERT INTO `session_registrations` VALUES (52, 162, 4);
INSERT INTO `session_registrations` VALUES (53, 126, 4);
INSERT INTO `session_registrations` VALUES (54, 189, 4);
INSERT INTO `session_registrations` VALUES (55, 269, 4);
INSERT INTO `session_registrations` VALUES (56, 114, 4);
INSERT INTO `session_registrations` VALUES (57, 109, 4);
INSERT INTO `session_registrations` VALUES (58, 206, 4);
INSERT INTO `session_registrations` VALUES (59, 227, 4);
INSERT INTO `session_registrations` VALUES (60, 114, 11);
INSERT INTO `session_registrations` VALUES (61, 112, 11);
INSERT INTO `session_registrations` VALUES (62, 174, 11);
INSERT INTO `session_registrations` VALUES (63, 184, 11);
INSERT INTO `session_registrations` VALUES (64, 252, 11);
INSERT INTO `session_registrations` VALUES (65, 195, 11);
INSERT INTO `session_registrations` VALUES (66, 262, 11);
INSERT INTO `session_registrations` VALUES (67, 164, 11);
INSERT INTO `session_registrations` VALUES (68, 263, 11);
INSERT INTO `session_registrations` VALUES (69, 137, 11);
INSERT INTO `session_registrations` VALUES (70, 139, 11);
INSERT INTO `session_registrations` VALUES (71, 209, 11);
INSERT INTO `session_registrations` VALUES (72, 132, 11);
INSERT INTO `session_registrations` VALUES (73, 257, 11);
INSERT INTO `session_registrations` VALUES (74, 236, 11);
INSERT INTO `session_registrations` VALUES (75, 144, 11);
INSERT INTO `session_registrations` VALUES (76, 115, 11);
INSERT INTO `session_registrations` VALUES (77, 126, 11);
INSERT INTO `session_registrations` VALUES (78, 186, 11);
INSERT INTO `session_registrations` VALUES (79, 222, 11);
INSERT INTO `session_registrations` VALUES (80, 280, 11);
INSERT INTO `session_registrations` VALUES (81, 183, 11);
INSERT INTO `session_registrations` VALUES (82, 151, 11);
INSERT INTO `session_registrations` VALUES (83, 276, 11);
INSERT INTO `session_registrations` VALUES (84, 246, 11);
INSERT INTO `session_registrations` VALUES (85, 239, 11);
INSERT INTO `session_registrations` VALUES (86, 113, 11);
INSERT INTO `session_registrations` VALUES (87, 210, 11);
INSERT INTO `session_registrations` VALUES (88, 267, 11);
INSERT INTO `session_registrations` VALUES (89, 175, 11);
INSERT INTO `session_registrations` VALUES (90, 214, 11);
INSERT INTO `session_registrations` VALUES (91, 232, 11);
INSERT INTO `session_registrations` VALUES (92, 237, 11);
INSERT INTO `session_registrations` VALUES (93, 134, 11);
INSERT INTO `session_registrations` VALUES (94, 272, 11);
INSERT INTO `session_registrations` VALUES (95, 207, 11);
INSERT INTO `session_registrations` VALUES (96, 261, 11);
INSERT INTO `session_registrations` VALUES (97, 254, 11);
INSERT INTO `session_registrations` VALUES (98, 208, 11);
INSERT INTO `session_registrations` VALUES (99, 105, 11);
INSERT INTO `session_registrations` VALUES (100, 219, 11);
INSERT INTO `session_registrations` VALUES (101, 162, 11);
INSERT INTO `session_registrations` VALUES (102, 247, 11);
INSERT INTO `session_registrations` VALUES (103, 241, 11);
INSERT INTO `session_registrations` VALUES (104, 127, 11);
INSERT INTO `session_registrations` VALUES (105, 127, 9);
INSERT INTO `session_registrations` VALUES (106, 111, 11);
INSERT INTO `session_registrations` VALUES (107, 216, 11);
INSERT INTO `session_registrations` VALUES (108, 269, 11);
INSERT INTO `session_registrations` VALUES (109, 123, 11);
INSERT INTO `session_registrations` VALUES (110, 256, 11);
INSERT INTO `session_registrations` VALUES (111, 157, 11);
INSERT INTO `session_registrations` VALUES (112, 271, 11);
INSERT INTO `session_registrations` VALUES (113, 163, 11);
INSERT INTO `session_registrations` VALUES (114, 218, 11);
INSERT INTO `session_registrations` VALUES (115, 253, 11);
INSERT INTO `session_registrations` VALUES (116, 152, 11);
INSERT INTO `session_registrations` VALUES (117, 198, 11);
INSERT INTO `session_registrations` VALUES (118, 245, 11);
INSERT INTO `session_registrations` VALUES (119, 204, 11);
INSERT INTO `session_registrations` VALUES (120, 225, 11);
INSERT INTO `session_registrations` VALUES (121, 242, 11);
INSERT INTO `session_registrations` VALUES (122, 270, 11);
INSERT INTO `session_registrations` VALUES (123, 147, 11);
INSERT INTO `session_registrations` VALUES (124, 104, 11);
INSERT INTO `session_registrations` VALUES (125, 180, 11);
INSERT INTO `session_registrations` VALUES (126, 122, 11);
INSERT INTO `session_registrations` VALUES (127, 160, 11);
INSERT INTO `session_registrations` VALUES (128, 226, 11);
INSERT INTO `session_registrations` VALUES (129, 185, 11);
INSERT INTO `session_registrations` VALUES (130, 125, 11);
INSERT INTO `session_registrations` VALUES (131, 212, 11);
INSERT INTO `session_registrations` VALUES (132, 156, 11);
INSERT INTO `session_registrations` VALUES (133, 168, 11);
INSERT INTO `session_registrations` VALUES (134, 158, 11);
INSERT INTO `session_registrations` VALUES (135, 167, 11);
INSERT INTO `session_registrations` VALUES (136, 140, 11);
INSERT INTO `session_registrations` VALUES (137, 259, 11);
INSERT INTO `session_registrations` VALUES (138, 161, 11);
INSERT INTO `session_registrations` VALUES (139, 103, 11);
INSERT INTO `session_registrations` VALUES (140, 238, 11);
INSERT INTO `session_registrations` VALUES (141, 278, 11);
INSERT INTO `session_registrations` VALUES (142, 149, 11);
INSERT INTO `session_registrations` VALUES (143, 224, 11);
INSERT INTO `session_registrations` VALUES (144, 251, 11);
INSERT INTO `session_registrations` VALUES (145, 153, 11);
INSERT INTO `session_registrations` VALUES (146, 131, 11);
INSERT INTO `session_registrations` VALUES (147, 200, 11);
INSERT INTO `session_registrations` VALUES (148, 203, 11);
INSERT INTO `session_registrations` VALUES (149, 221, 11);
INSERT INTO `session_registrations` VALUES (150, 169, 11);
INSERT INTO `session_registrations` VALUES (151, 171, 11);
INSERT INTO `session_registrations` VALUES (152, 193, 11);
INSERT INTO `session_registrations` VALUES (153, 173, 11);
INSERT INTO `session_registrations` VALUES (154, 136, 11);
INSERT INTO `session_registrations` VALUES (155, 179, 11);
INSERT INTO `session_registrations` VALUES (156, 124, 11);
INSERT INTO `session_registrations` VALUES (157, 155, 11);
INSERT INTO `session_registrations` VALUES (158, 150, 11);
INSERT INTO `session_registrations` VALUES (159, 187, 11);
INSERT INTO `session_registrations` VALUES (160, 206, 11);
INSERT INTO `session_registrations` VALUES (161, 274, 9);
INSERT INTO `session_registrations` VALUES (162, 176, 9);
INSERT INTO `session_registrations` VALUES (163, 144, 9);
INSERT INTO `session_registrations` VALUES (164, 230, 9);
INSERT INTO `session_registrations` VALUES (165, 142, 9);
INSERT INTO `session_registrations` VALUES (166, 124, 9);
INSERT INTO `session_registrations` VALUES (167, 178, 9);
INSERT INTO `session_registrations` VALUES (168, 269, 9);
INSERT INTO `session_registrations` VALUES (169, 225, 9);
INSERT INTO `session_registrations` VALUES (170, 175, 9);
INSERT INTO `session_registrations` VALUES (171, 201, 9);
INSERT INTO `session_registrations` VALUES (172, 108, 9);
INSERT INTO `session_registrations` VALUES (173, 203, 9);
INSERT INTO `session_registrations` VALUES (174, 235, 9);
INSERT INTO `session_registrations` VALUES (175, 210, 9);
INSERT INTO `session_registrations` VALUES (176, 258, 9);
INSERT INTO `session_registrations` VALUES (177, 139, 9);
INSERT INTO `session_registrations` VALUES (178, 223, 9);
INSERT INTO `session_registrations` VALUES (179, 217, 9);
INSERT INTO `session_registrations` VALUES (180, 200, 9);
INSERT INTO `session_registrations` VALUES (181, 164, 9);
INSERT INTO `session_registrations` VALUES (182, 165, 9);
INSERT INTO `session_registrations` VALUES (183, 265, 9);
INSERT INTO `session_registrations` VALUES (184, 122, 9);
INSERT INTO `session_registrations` VALUES (185, 167, 9);
INSERT INTO `session_registrations` VALUES (186, 182, 9);
INSERT INTO `session_registrations` VALUES (187, 126, 9);
INSERT INTO `session_registrations` VALUES (188, 199, 9);
INSERT INTO `session_registrations` VALUES (189, 123, 9);
INSERT INTO `session_registrations` VALUES (190, 118, 9);
INSERT INTO `session_registrations` VALUES (191, 133, 9);
INSERT INTO `session_registrations` VALUES (192, 104, 9);
INSERT INTO `session_registrations` VALUES (193, 280, 9);
INSERT INTO `session_registrations` VALUES (194, 113, 9);
INSERT INTO `session_registrations` VALUES (195, 248, 9);
INSERT INTO `session_registrations` VALUES (196, 177, 9);
INSERT INTO `session_registrations` VALUES (197, 205, 9);
INSERT INTO `session_registrations` VALUES (198, 197, 9);
INSERT INTO `session_registrations` VALUES (199, 216, 9);
INSERT INTO `session_registrations` VALUES (200, 238, 9);
INSERT INTO `session_registrations` VALUES (201, 189, 9);
INSERT INTO `session_registrations` VALUES (202, 180, 9);
INSERT INTO `session_registrations` VALUES (203, 149, 9);
INSERT INTO `session_registrations` VALUES (204, 279, 9);
INSERT INTO `session_registrations` VALUES (205, 181, 9);
INSERT INTO `session_registrations` VALUES (206, 174, 9);
INSERT INTO `session_registrations` VALUES (207, 251, 9);
INSERT INTO `session_registrations` VALUES (208, 193, 9);
INSERT INTO `session_registrations` VALUES (209, 111, 9);
INSERT INTO `session_registrations` VALUES (210, 232, 9);
INSERT INTO `session_registrations` VALUES (211, 110, 9);
INSERT INTO `session_registrations` VALUES (212, 166, 9);
INSERT INTO `session_registrations` VALUES (213, 219, 9);
INSERT INTO `session_registrations` VALUES (214, 236, 9);
INSERT INTO `session_registrations` VALUES (215, 209, 9);
INSERT INTO `session_registrations` VALUES (216, 179, 9);
INSERT INTO `session_registrations` VALUES (217, 246, 9);
INSERT INTO `session_registrations` VALUES (218, 267, 9);
INSERT INTO `session_registrations` VALUES (219, 252, 9);
INSERT INTO `session_registrations` VALUES (220, 160, 9);
INSERT INTO `session_registrations` VALUES (221, 204, 9);
INSERT INTO `session_registrations` VALUES (222, 120, 9);
INSERT INTO `session_registrations` VALUES (223, 250, 9);
INSERT INTO `session_registrations` VALUES (224, 250, 11);
INSERT INTO `session_registrations` VALUES (225, 194, 9);
INSERT INTO `session_registrations` VALUES (226, 198, 9);
INSERT INTO `session_registrations` VALUES (227, 192, 9);
INSERT INTO `session_registrations` VALUES (228, 255, 9);
INSERT INTO `session_registrations` VALUES (229, 153, 9);
INSERT INTO `session_registrations` VALUES (230, 136, 9);
INSERT INTO `session_registrations` VALUES (231, 134, 9);
INSERT INTO `session_registrations` VALUES (232, 221, 9);
INSERT INTO `session_registrations` VALUES (233, 114, 9);
INSERT INTO `session_registrations` VALUES (234, 196, 9);
INSERT INTO `session_registrations` VALUES (235, 196, 6);
INSERT INTO `session_registrations` VALUES (236, 234, 6);
INSERT INTO `session_registrations` VALUES (237, 109, 6);
INSERT INTO `session_registrations` VALUES (238, 211, 6);
INSERT INTO `session_registrations` VALUES (239, 221, 6);
INSERT INTO `session_registrations` VALUES (240, 142, 6);
INSERT INTO `session_registrations` VALUES (241, 203, 6);
INSERT INTO `session_registrations` VALUES (242, 199, 6);
INSERT INTO `session_registrations` VALUES (243, 161, 6);
INSERT INTO `session_registrations` VALUES (244, 212, 6);
INSERT INTO `session_registrations` VALUES (245, 113, 6);
INSERT INTO `session_registrations` VALUES (246, 218, 6);
INSERT INTO `session_registrations` VALUES (247, 260, 6);
INSERT INTO `session_registrations` VALUES (248, 186, 6);
INSERT INTO `session_registrations` VALUES (249, 127, 6);
INSERT INTO `session_registrations` VALUES (250, 103, 6);
INSERT INTO `session_registrations` VALUES (251, 278, 6);
INSERT INTO `session_registrations` VALUES (252, 268, 6);
INSERT INTO `session_registrations` VALUES (253, 153, 6);
INSERT INTO `session_registrations` VALUES (254, 198, 6);
INSERT INTO `session_registrations` VALUES (255, 230, 6);
INSERT INTO `session_registrations` VALUES (256, 200, 6);
INSERT INTO `session_registrations` VALUES (257, 257, 6);
INSERT INTO `session_registrations` VALUES (258, 235, 6);
INSERT INTO `session_registrations` VALUES (259, 182, 6);
INSERT INTO `session_registrations` VALUES (260, 184, 6);
INSERT INTO `session_registrations` VALUES (261, 146, 6);
INSERT INTO `session_registrations` VALUES (262, 156, 6);
INSERT INTO `session_registrations` VALUES (263, 233, 6);
INSERT INTO `session_registrations` VALUES (264, 104, 6);
INSERT INTO `session_registrations` VALUES (265, 267, 6);
INSERT INTO `session_registrations` VALUES (266, 116, 6);
INSERT INTO `session_registrations` VALUES (267, 266, 6);
INSERT INTO `session_registrations` VALUES (268, 253, 6);
INSERT INTO `session_registrations` VALUES (269, 204, 6);
INSERT INTO `session_registrations` VALUES (270, 192, 6);
INSERT INTO `session_registrations` VALUES (271, 262, 6);
INSERT INTO `session_registrations` VALUES (272, 105, 6);
INSERT INTO `session_registrations` VALUES (273, 185, 6);
INSERT INTO `session_registrations` VALUES (274, 188, 6);
INSERT INTO `session_registrations` VALUES (275, 117, 6);
INSERT INTO `session_registrations` VALUES (276, 132, 6);
INSERT INTO `session_registrations` VALUES (277, 214, 6);
INSERT INTO `session_registrations` VALUES (278, 319, 18);
INSERT INTO `session_registrations` VALUES (279, 15, 18);
INSERT INTO `session_registrations` VALUES (280, 22, 18);
INSERT INTO `session_registrations` VALUES (281, 339, 18);
INSERT INTO `session_registrations` VALUES (282, 321, 18);
INSERT INTO `session_registrations` VALUES (283, 360, 18);
INSERT INTO `session_registrations` VALUES (284, 325, 18);
INSERT INTO `session_registrations` VALUES (285, 293, 18);
INSERT INTO `session_registrations` VALUES (286, 330, 18);
INSERT INTO `session_registrations` VALUES (287, 290, 18);
INSERT INTO `session_registrations` VALUES (288, 318, 18);
INSERT INTO `session_registrations` VALUES (289, 23, 18);
INSERT INTO `session_registrations` VALUES (290, 374, 18);
INSERT INTO `session_registrations` VALUES (291, 294, 18);
INSERT INTO `session_registrations` VALUES (292, 28, 18);
INSERT INTO `session_registrations` VALUES (293, 347, 18);
INSERT INTO `session_registrations` VALUES (294, 381, 18);
INSERT INTO `session_registrations` VALUES (295, 3, 18);
INSERT INTO `session_registrations` VALUES (296, 331, 18);
INSERT INTO `session_registrations` VALUES (297, 336, 18);
INSERT INTO `session_registrations` VALUES (298, 288, 18);
INSERT INTO `session_registrations` VALUES (299, 306, 18);
INSERT INTO `session_registrations` VALUES (300, 328, 18);
INSERT INTO `session_registrations` VALUES (301, 286, 18);
INSERT INTO `session_registrations` VALUES (302, 302, 18);
INSERT INTO `session_registrations` VALUES (303, 372, 18);
INSERT INTO `session_registrations` VALUES (304, 26, 18);
INSERT INTO `session_registrations` VALUES (305, 355, 18);
INSERT INTO `session_registrations` VALUES (306, 365, 18);
INSERT INTO `session_registrations` VALUES (307, 369, 18);
INSERT INTO `session_registrations` VALUES (308, 296, 18);
INSERT INTO `session_registrations` VALUES (309, 289, 18);
INSERT INTO `session_registrations` VALUES (310, 345, 18);
INSERT INTO `session_registrations` VALUES (311, 9, 18);
INSERT INTO `session_registrations` VALUES (312, 353, 18);
INSERT INTO `session_registrations` VALUES (313, 299, 18);
INSERT INTO `session_registrations` VALUES (314, 380, 18);
INSERT INTO `session_registrations` VALUES (315, 295, 18);
INSERT INTO `session_registrations` VALUES (316, 313, 18);
INSERT INTO `session_registrations` VALUES (317, 350, 18);
INSERT INTO `session_registrations` VALUES (318, 282, 18);
INSERT INTO `session_registrations` VALUES (319, 351, 18);
INSERT INTO `session_registrations` VALUES (320, 27, 18);
INSERT INTO `session_registrations` VALUES (321, 21, 18);
INSERT INTO `session_registrations` VALUES (322, 315, 18);
INSERT INTO `session_registrations` VALUES (323, 332, 18);
INSERT INTO `session_registrations` VALUES (324, 370, 18);
INSERT INTO `session_registrations` VALUES (325, 19, 18);
INSERT INTO `session_registrations` VALUES (326, 285, 18);
INSERT INTO `session_registrations` VALUES (327, 4, 18);
INSERT INTO `session_registrations` VALUES (328, 8, 18);
INSERT INTO `session_registrations` VALUES (329, 305, 18);
INSERT INTO `session_registrations` VALUES (330, 13, 18);
INSERT INTO `session_registrations` VALUES (331, 301, 18);
INSERT INTO `session_registrations` VALUES (332, 308, 18);
INSERT INTO `session_registrations` VALUES (333, 375, 18);
INSERT INTO `session_registrations` VALUES (334, 361, 18);
INSERT INTO `session_registrations` VALUES (335, 376, 18);
INSERT INTO `session_registrations` VALUES (336, 11, 18);
INSERT INTO `session_registrations` VALUES (337, 338, 18);
INSERT INTO `session_registrations` VALUES (338, 309, 18);
INSERT INTO `session_registrations` VALUES (339, 10, 18);
INSERT INTO `session_registrations` VALUES (340, 356, 18);
INSERT INTO `session_registrations` VALUES (341, 352, 18);
INSERT INTO `session_registrations` VALUES (342, 307, 18);
INSERT INTO `session_registrations` VALUES (343, 383, 18);
INSERT INTO `session_registrations` VALUES (344, 329, 18);
INSERT INTO `session_registrations` VALUES (345, 316, 18);
INSERT INTO `session_registrations` VALUES (346, 359, 18);
INSERT INTO `session_registrations` VALUES (347, 366, 18);
INSERT INTO `session_registrations` VALUES (348, 25, 18);
INSERT INTO `session_registrations` VALUES (349, 348, 18);
INSERT INTO `session_registrations` VALUES (350, 323, 18);
INSERT INTO `session_registrations` VALUES (351, 303, 18);
INSERT INTO `session_registrations` VALUES (352, 20, 18);
INSERT INTO `session_registrations` VALUES (353, 358, 18);
INSERT INTO `session_registrations` VALUES (354, 45, 24);
INSERT INTO `session_registrations` VALUES (355, 450, 24);
INSERT INTO `session_registrations` VALUES (356, 405, 24);
INSERT INTO `session_registrations` VALUES (357, 458, 24);
INSERT INTO `session_registrations` VALUES (358, 397, 24);
INSERT INTO `session_registrations` VALUES (359, 417, 24);
INSERT INTO `session_registrations` VALUES (360, 400, 24);
INSERT INTO `session_registrations` VALUES (361, 456, 24);
INSERT INTO `session_registrations` VALUES (362, 402, 24);
INSERT INTO `session_registrations` VALUES (363, 467, 24);
INSERT INTO `session_registrations` VALUES (364, 389, 24);
INSERT INTO `session_registrations` VALUES (365, 428, 24);
INSERT INTO `session_registrations` VALUES (366, 387, 24);
INSERT INTO `session_registrations` VALUES (367, 414, 24);
INSERT INTO `session_registrations` VALUES (368, 462, 24);
INSERT INTO `session_registrations` VALUES (369, 392, 24);
INSERT INTO `session_registrations` VALUES (370, 457, 24);
INSERT INTO `session_registrations` VALUES (371, 42, 24);
INSERT INTO `session_registrations` VALUES (372, 432, 24);
INSERT INTO `session_registrations` VALUES (373, 61, 24);
INSERT INTO `session_registrations` VALUES (374, 446, 24);
INSERT INTO `session_registrations` VALUES (375, 419, 24);
INSERT INTO `session_registrations` VALUES (376, 449, 24);
INSERT INTO `session_registrations` VALUES (377, 464, 24);
INSERT INTO `session_registrations` VALUES (378, 38, 24);
INSERT INTO `session_registrations` VALUES (379, 56, 24);
INSERT INTO `session_registrations` VALUES (380, 443, 24);
INSERT INTO `session_registrations` VALUES (381, 62, 24);
INSERT INTO `session_registrations` VALUES (382, 64, 24);
INSERT INTO `session_registrations` VALUES (383, 50, 24);
INSERT INTO `session_registrations` VALUES (384, 429, 24);
INSERT INTO `session_registrations` VALUES (385, 465, 24);
INSERT INTO `session_registrations` VALUES (386, 448, 24);
INSERT INTO `session_registrations` VALUES (387, 403, 24);
INSERT INTO `session_registrations` VALUES (388, 477, 24);
INSERT INTO `session_registrations` VALUES (389, 468, 24);
INSERT INTO `session_registrations` VALUES (390, 452, 24);
INSERT INTO `session_registrations` VALUES (391, 422, 24);
INSERT INTO `session_registrations` VALUES (392, 406, 24);
INSERT INTO `session_registrations` VALUES (393, 399, 24);
INSERT INTO `session_registrations` VALUES (394, 447, 24);
INSERT INTO `session_registrations` VALUES (395, 461, 24);
INSERT INTO `session_registrations` VALUES (396, 476, 24);
INSERT INTO `session_registrations` VALUES (397, 438, 24);
INSERT INTO `session_registrations` VALUES (398, 423, 24);
INSERT INTO `session_registrations` VALUES (399, 39, 24);
INSERT INTO `session_registrations` VALUES (400, 451, 24);
INSERT INTO `session_registrations` VALUES (401, 404, 24);
INSERT INTO `session_registrations` VALUES (402, 49, 24);
INSERT INTO `session_registrations` VALUES (403, 444, 24);
INSERT INTO `session_registrations` VALUES (404, 411, 24);
INSERT INTO `session_registrations` VALUES (405, 426, 24);
INSERT INTO `session_registrations` VALUES (406, 424, 24);
INSERT INTO `session_registrations` VALUES (407, 410, 24);
INSERT INTO `session_registrations` VALUES (408, 401, 24);
INSERT INTO `session_registrations` VALUES (409, 472, 24);
INSERT INTO `session_registrations` VALUES (410, 420, 24);
INSERT INTO `session_registrations` VALUES (411, 445, 24);
INSERT INTO `session_registrations` VALUES (412, 393, 24);
INSERT INTO `session_registrations` VALUES (413, 478, 24);
INSERT INTO `session_registrations` VALUES (414, 442, 24);
INSERT INTO `session_registrations` VALUES (415, 454, 24);
INSERT INTO `session_registrations` VALUES (416, 44, 24);
INSERT INTO `session_registrations` VALUES (417, 60, 24);
INSERT INTO `session_registrations` VALUES (418, 47, 24);
INSERT INTO `session_registrations` VALUES (419, 473, 24);
INSERT INTO `session_registrations` VALUES (420, 59, 24);
INSERT INTO `session_registrations` VALUES (421, 53, 24);
INSERT INTO `session_registrations` VALUES (422, 408, 24);
INSERT INTO `session_registrations` VALUES (423, 421, 24);
INSERT INTO `session_registrations` VALUES (424, 385, 24);
INSERT INTO `session_registrations` VALUES (425, 63, 24);
INSERT INTO `session_registrations` VALUES (426, 386, 24);
INSERT INTO `session_registrations` VALUES (427, 46, 24);
INSERT INTO `session_registrations` VALUES (428, 469, 24);
INSERT INTO `session_registrations` VALUES (429, 37, 24);
INSERT INTO `session_registrations` VALUES (430, 571, 39);
INSERT INTO `session_registrations` VALUES (431, 559, 39);
INSERT INTO `session_registrations` VALUES (432, 570, 39);
INSERT INTO `session_registrations` VALUES (433, 531, 39);
INSERT INTO `session_registrations` VALUES (434, 100, 39);
INSERT INTO `session_registrations` VALUES (435, 76, 39);
INSERT INTO `session_registrations` VALUES (436, 564, 39);
INSERT INTO `session_registrations` VALUES (437, 558, 39);
INSERT INTO `session_registrations` VALUES (438, 502, 39);
INSERT INTO `session_registrations` VALUES (439, 566, 39);
INSERT INTO `session_registrations` VALUES (440, 551, 39);
INSERT INTO `session_registrations` VALUES (441, 88, 39);
INSERT INTO `session_registrations` VALUES (442, 513, 39);
INSERT INTO `session_registrations` VALUES (443, 70, 39);
INSERT INTO `session_registrations` VALUES (444, 493, 39);
INSERT INTO `session_registrations` VALUES (445, 556, 39);
INSERT INTO `session_registrations` VALUES (446, 567, 39);
INSERT INTO `session_registrations` VALUES (447, 488, 39);
INSERT INTO `session_registrations` VALUES (448, 526, 39);
INSERT INTO `session_registrations` VALUES (449, 510, 39);
INSERT INTO `session_registrations` VALUES (450, 497, 39);
INSERT INTO `session_registrations` VALUES (451, 565, 39);
INSERT INTO `session_registrations` VALUES (452, 569, 39);
INSERT INTO `session_registrations` VALUES (453, 514, 39);
INSERT INTO `session_registrations` VALUES (454, 575, 39);
INSERT INTO `session_registrations` VALUES (455, 547, 39);
INSERT INTO `session_registrations` VALUES (456, 512, 39);
INSERT INTO `session_registrations` VALUES (457, 93, 39);
INSERT INTO `session_registrations` VALUES (458, 484, 39);
INSERT INTO `session_registrations` VALUES (459, 560, 39);
INSERT INTO `session_registrations` VALUES (460, 486, 39);
INSERT INTO `session_registrations` VALUES (461, 562, 39);
INSERT INTO `session_registrations` VALUES (462, 520, 39);
INSERT INTO `session_registrations` VALUES (463, 480, 39);
INSERT INTO `session_registrations` VALUES (464, 95, 39);
INSERT INTO `session_registrations` VALUES (465, 557, 39);
INSERT INTO `session_registrations` VALUES (466, 69, 39);
INSERT INTO `session_registrations` VALUES (467, 535, 39);
INSERT INTO `session_registrations` VALUES (468, 509, 39);
INSERT INTO `session_registrations` VALUES (469, 487, 39);
INSERT INTO `session_registrations` VALUES (470, 99, 39);
INSERT INTO `session_registrations` VALUES (471, 479, 39);
INSERT INTO `session_registrations` VALUES (472, 516, 39);
INSERT INTO `session_registrations` VALUES (473, 490, 39);
INSERT INTO `session_registrations` VALUES (474, 538, 39);
INSERT INTO `session_registrations` VALUES (475, 86, 39);
INSERT INTO `session_registrations` VALUES (476, 577, 39);
INSERT INTO `session_registrations` VALUES (477, 552, 39);
INSERT INTO `session_registrations` VALUES (478, 576, 39);
INSERT INTO `session_registrations` VALUES (479, 92, 39);
INSERT INTO `session_registrations` VALUES (480, 483, 39);
INSERT INTO `session_registrations` VALUES (481, 96, 39);
INSERT INTO `session_registrations` VALUES (482, 563, 39);
INSERT INTO `session_registrations` VALUES (483, 546, 39);
INSERT INTO `session_registrations` VALUES (484, 505, 39);
INSERT INTO `session_registrations` VALUES (485, 555, 39);
INSERT INTO `session_registrations` VALUES (486, 548, 39);
INSERT INTO `session_registrations` VALUES (487, 550, 39);
INSERT INTO `session_registrations` VALUES (488, 525, 39);
INSERT INTO `session_registrations` VALUES (489, 485, 39);
INSERT INTO `session_registrations` VALUES (490, 87, 39);
INSERT INTO `session_registrations` VALUES (491, 568, 39);
INSERT INTO `session_registrations` VALUES (492, 573, 39);
INSERT INTO `session_registrations` VALUES (493, 67, 39);
INSERT INTO `session_registrations` VALUES (494, 94, 39);
INSERT INTO `session_registrations` VALUES (495, 97, 39);
INSERT INTO `session_registrations` VALUES (496, 521, 39);
INSERT INTO `session_registrations` VALUES (497, 495, 39);
INSERT INTO `session_registrations` VALUES (498, 71, 39);
INSERT INTO `session_registrations` VALUES (499, 79, 39);
INSERT INTO `session_registrations` VALUES (500, 75, 39);
INSERT INTO `session_registrations` VALUES (501, 85, 39);
INSERT INTO `session_registrations` VALUES (502, 74, 39);
INSERT INTO `session_registrations` VALUES (503, 528, 39);
INSERT INTO `session_registrations` VALUES (504, 101, 39);
INSERT INTO `session_registrations` VALUES (505, 482, 39);
INSERT INTO `session_registrations` VALUES (506, 495, 42);
INSERT INTO `session_registrations` VALUES (507, 516, 42);
INSERT INTO `session_registrations` VALUES (508, 548, 42);
INSERT INTO `session_registrations` VALUES (509, 578, 42);
INSERT INTO `session_registrations` VALUES (510, 68, 42);
INSERT INTO `session_registrations` VALUES (511, 485, 42);
INSERT INTO `session_registrations` VALUES (512, 515, 42);
INSERT INTO `session_registrations` VALUES (513, 498, 42);
INSERT INTO `session_registrations` VALUES (514, 540, 42);
INSERT INTO `session_registrations` VALUES (515, 85, 42);
INSERT INTO `session_registrations` VALUES (516, 512, 42);
INSERT INTO `session_registrations` VALUES (517, 518, 42);
INSERT INTO `session_registrations` VALUES (518, 91, 42);
INSERT INTO `session_registrations` VALUES (519, 530, 42);
INSERT INTO `session_registrations` VALUES (520, 493, 42);
INSERT INTO `session_registrations` VALUES (521, 552, 42);
INSERT INTO `session_registrations` VALUES (522, 539, 42);
INSERT INTO `session_registrations` VALUES (523, 537, 42);
INSERT INTO `session_registrations` VALUES (524, 517, 42);
INSERT INTO `session_registrations` VALUES (525, 96, 42);
INSERT INTO `session_registrations` VALUES (526, 576, 42);
INSERT INTO `session_registrations` VALUES (527, 79, 42);
INSERT INTO `session_registrations` VALUES (528, 532, 42);
INSERT INTO `session_registrations` VALUES (529, 71, 42);
INSERT INTO `session_registrations` VALUES (530, 89, 42);
INSERT INTO `session_registrations` VALUES (531, 550, 42);
INSERT INTO `session_registrations` VALUES (532, 525, 42);
INSERT INTO `session_registrations` VALUES (533, 566, 42);
INSERT INTO `session_registrations` VALUES (534, 70, 42);
INSERT INTO `session_registrations` VALUES (535, 558, 42);
INSERT INTO `session_registrations` VALUES (536, 81, 42);
INSERT INTO `session_registrations` VALUES (537, 553, 42);
INSERT INTO `session_registrations` VALUES (538, 77, 42);
INSERT INTO `session_registrations` VALUES (539, 528, 42);
INSERT INTO `session_registrations` VALUES (540, 555, 42);
INSERT INTO `session_registrations` VALUES (541, 574, 42);
INSERT INTO `session_registrations` VALUES (542, 546, 42);
INSERT INTO `session_registrations` VALUES (543, 573, 42);
INSERT INTO `session_registrations` VALUES (544, 499, 42);
INSERT INTO `session_registrations` VALUES (545, 571, 42);
INSERT INTO `session_registrations` VALUES (546, 87, 42);
INSERT INTO `session_registrations` VALUES (547, 545, 42);
INSERT INTO `session_registrations` VALUES (548, 73, 42);
INSERT INTO `session_registrations` VALUES (549, 551, 42);
INSERT INTO `session_registrations` VALUES (550, 560, 42);
INSERT INTO `session_registrations` VALUES (551, 481, 42);
INSERT INTO `session_registrations` VALUES (552, 479, 42);
INSERT INTO `session_registrations` VALUES (553, 94, 42);
INSERT INTO `session_registrations` VALUES (554, 67, 42);
INSERT INTO `session_registrations` VALUES (555, 562, 42);
INSERT INTO `session_registrations` VALUES (556, 519, 42);
INSERT INTO `session_registrations` VALUES (557, 86, 42);
INSERT INTO `session_registrations` VALUES (558, 95, 42);
INSERT INTO `session_registrations` VALUES (559, 567, 42);
INSERT INTO `session_registrations` VALUES (560, 502, 42);
INSERT INTO `session_registrations` VALUES (561, 575, 42);
INSERT INTO `session_registrations` VALUES (562, 98, 42);
INSERT INTO `session_registrations` VALUES (563, 484, 42);
INSERT INTO `session_registrations` VALUES (564, 506, 42);
INSERT INTO `session_registrations` VALUES (565, 93, 42);
INSERT INTO `session_registrations` VALUES (566, 542, 42);
INSERT INTO `session_registrations` VALUES (567, 88, 42);
INSERT INTO `session_registrations` VALUES (568, 100, 42);
INSERT INTO `session_registrations` VALUES (569, 80, 42);
INSERT INTO `session_registrations` VALUES (570, 534, 42);
INSERT INTO `session_registrations` VALUES (571, 101, 42);
INSERT INTO `session_registrations` VALUES (572, 72, 42);
INSERT INTO `session_registrations` VALUES (573, 483, 42);
INSERT INTO `session_registrations` VALUES (574, 533, 42);
INSERT INTO `session_registrations` VALUES (575, 480, 42);
INSERT INTO `session_registrations` VALUES (576, 76, 42);
INSERT INTO `session_registrations` VALUES (577, 538, 42);
INSERT INTO `session_registrations` VALUES (578, 66, 42);
INSERT INTO `session_registrations` VALUES (579, 570, 42);
INSERT INTO `session_registrations` VALUES (580, 501, 42);
INSERT INTO `session_registrations` VALUES (581, 536, 42);
INSERT INTO `session_registrations` VALUES (582, 559, 42);
INSERT INTO `session_registrations` VALUES (583, 82, 42);
INSERT INTO `session_registrations` VALUES (584, 563, 42);
INSERT INTO `session_registrations` VALUES (585, 543, 42);
INSERT INTO `session_registrations` VALUES (586, 524, 42);
INSERT INTO `session_registrations` VALUES (587, 564, 42);
INSERT INTO `session_registrations` VALUES (588, 496, 42);
INSERT INTO `session_registrations` VALUES (589, 75, 42);
INSERT INTO `session_registrations` VALUES (590, 547, 42);
INSERT INTO `session_registrations` VALUES (591, 527, 42);
INSERT INTO `session_registrations` VALUES (592, 488, 42);
INSERT INTO `session_registrations` VALUES (593, 482, 42);
INSERT INTO `session_registrations` VALUES (594, 504, 42);
INSERT INTO `session_registrations` VALUES (595, 503, 42);
INSERT INTO `session_registrations` VALUES (596, 544, 42);
INSERT INTO `session_registrations` VALUES (597, 521, 42);
INSERT INTO `session_registrations` VALUES (598, 565, 42);
INSERT INTO `session_registrations` VALUES (599, 510, 42);
INSERT INTO `session_registrations` VALUES (600, 541, 42);
INSERT INTO `session_registrations` VALUES (601, 535, 42);
INSERT INTO `session_registrations` VALUES (602, 513, 42);
INSERT INTO `session_registrations` VALUES (603, 531, 42);
INSERT INTO `session_registrations` VALUES (604, 549, 40);
INSERT INTO `session_registrations` VALUES (605, 493, 40);
INSERT INTO `session_registrations` VALUES (606, 547, 40);
INSERT INTO `session_registrations` VALUES (607, 95, 40);
INSERT INTO `session_registrations` VALUES (608, 568, 40);
INSERT INTO `session_registrations` VALUES (609, 563, 40);
INSERT INTO `session_registrations` VALUES (610, 572, 40);
INSERT INTO `session_registrations` VALUES (611, 522, 40);
INSERT INTO `session_registrations` VALUES (612, 80, 40);
INSERT INTO `session_registrations` VALUES (613, 546, 40);
INSERT INTO `session_registrations` VALUES (614, 96, 40);
INSERT INTO `session_registrations` VALUES (615, 483, 40);
INSERT INTO `session_registrations` VALUES (616, 81, 40);
INSERT INTO `session_registrations` VALUES (617, 539, 40);
INSERT INTO `session_registrations` VALUES (618, 516, 40);
INSERT INTO `session_registrations` VALUES (619, 531, 40);
INSERT INTO `session_registrations` VALUES (620, 512, 40);
INSERT INTO `session_registrations` VALUES (621, 99, 40);
INSERT INTO `session_registrations` VALUES (622, 562, 40);
INSERT INTO `session_registrations` VALUES (623, 542, 40);
INSERT INTO `session_registrations` VALUES (624, 83, 40);
INSERT INTO `session_registrations` VALUES (625, 78, 40);
INSERT INTO `session_registrations` VALUES (626, 513, 40);
INSERT INTO `session_registrations` VALUES (627, 98, 40);
INSERT INTO `session_registrations` VALUES (628, 100, 40);
INSERT INTO `session_registrations` VALUES (629, 76, 40);
INSERT INTO `session_registrations` VALUES (630, 490, 40);
INSERT INTO `session_registrations` VALUES (631, 101, 40);
INSERT INTO `session_registrations` VALUES (632, 518, 40);
INSERT INTO `session_registrations` VALUES (633, 492, 40);
INSERT INTO `session_registrations` VALUES (634, 558, 40);
INSERT INTO `session_registrations` VALUES (635, 538, 40);
INSERT INTO `session_registrations` VALUES (636, 88, 40);
INSERT INTO `session_registrations` VALUES (637, 541, 40);
INSERT INTO `session_registrations` VALUES (638, 534, 40);
INSERT INTO `session_registrations` VALUES (639, 528, 40);
INSERT INTO `session_registrations` VALUES (640, 557, 40);
INSERT INTO `session_registrations` VALUES (641, 530, 40);
INSERT INTO `session_registrations` VALUES (642, 482, 40);
INSERT INTO `session_registrations` VALUES (643, 523, 40);
INSERT INTO `session_registrations` VALUES (644, 71, 40);
INSERT INTO `session_registrations` VALUES (645, 564, 40);
INSERT INTO `session_registrations` VALUES (646, 486, 40);
INSERT INTO `session_registrations` VALUES (647, 555, 40);
INSERT INTO `session_registrations` VALUES (648, 517, 41);
INSERT INTO `session_registrations` VALUES (649, 492, 41);
INSERT INTO `session_registrations` VALUES (650, 86, 41);
INSERT INTO `session_registrations` VALUES (651, 575, 41);
INSERT INTO `session_registrations` VALUES (652, 70, 41);
INSERT INTO `session_registrations` VALUES (653, 504, 41);
INSERT INTO `session_registrations` VALUES (654, 551, 41);
INSERT INTO `session_registrations` VALUES (655, 576, 41);
INSERT INTO `session_registrations` VALUES (656, 75, 41);
INSERT INTO `session_registrations` VALUES (657, 514, 41);
INSERT INTO `session_registrations` VALUES (658, 486, 41);
INSERT INTO `session_registrations` VALUES (659, 566, 41);
INSERT INTO `session_registrations` VALUES (660, 548, 41);
INSERT INTO `session_registrations` VALUES (661, 570, 41);
INSERT INTO `session_registrations` VALUES (662, 82, 41);
INSERT INTO `session_registrations` VALUES (663, 88, 41);
INSERT INTO `session_registrations` VALUES (664, 565, 41);
INSERT INTO `session_registrations` VALUES (665, 493, 41);
INSERT INTO `session_registrations` VALUES (666, 67, 41);
INSERT INTO `session_registrations` VALUES (667, 479, 41);
INSERT INTO `session_registrations` VALUES (668, 530, 41);
INSERT INTO `session_registrations` VALUES (669, 513, 41);
INSERT INTO `session_registrations` VALUES (670, 87, 41);
INSERT INTO `session_registrations` VALUES (671, 540, 41);
INSERT INTO `session_registrations` VALUES (672, 100, 41);
INSERT INTO `session_registrations` VALUES (673, 499, 41);
INSERT INTO `session_registrations` VALUES (674, 537, 41);
INSERT INTO `session_registrations` VALUES (675, 544, 41);
INSERT INTO `session_registrations` VALUES (676, 491, 41);
INSERT INTO `session_registrations` VALUES (677, 84, 41);
INSERT INTO `session_registrations` VALUES (678, 527, 41);
INSERT INTO `session_registrations` VALUES (679, 560, 41);
INSERT INTO `session_registrations` VALUES (680, 96, 41);
INSERT INTO `session_registrations` VALUES (681, 507, 41);
INSERT INTO `session_registrations` VALUES (682, 526, 41);
INSERT INTO `session_registrations` VALUES (683, 502, 41);
INSERT INTO `session_registrations` VALUES (684, 74, 41);
INSERT INTO `session_registrations` VALUES (685, 76, 41);
INSERT INTO `session_registrations` VALUES (686, 488, 41);
INSERT INTO `session_registrations` VALUES (687, 561, 41);
INSERT INTO `session_registrations` VALUES (688, 81, 41);
INSERT INTO `session_registrations` VALUES (689, 489, 41);
INSERT INTO `session_registrations` VALUES (690, 487, 41);
INSERT INTO `session_registrations` VALUES (691, 509, 41);
INSERT INTO `session_registrations` VALUES (692, 77, 41);
INSERT INTO `session_registrations` VALUES (693, 483, 41);
INSERT INTO `session_registrations` VALUES (694, 577, 41);
INSERT INTO `session_registrations` VALUES (695, 497, 41);
INSERT INTO `session_registrations` VALUES (696, 522, 41);
INSERT INTO `session_registrations` VALUES (697, 94, 41);
INSERT INTO `session_registrations` VALUES (698, 518, 41);
INSERT INTO `session_registrations` VALUES (699, 85, 41);
INSERT INTO `session_registrations` VALUES (700, 578, 41);
INSERT INTO `session_registrations` VALUES (701, 516, 41);
INSERT INTO `session_registrations` VALUES (702, 95, 41);
INSERT INTO `session_registrations` VALUES (703, 80, 41);
INSERT INTO `session_registrations` VALUES (704, 534, 41);
INSERT INTO `session_registrations` VALUES (705, 552, 41);
INSERT INTO `session_registrations` VALUES (706, 90, 41);
INSERT INTO `session_registrations` VALUES (707, 558, 41);
INSERT INTO `session_registrations` VALUES (708, 559, 41);
INSERT INTO `session_registrations` VALUES (709, 524, 41);
INSERT INTO `session_registrations` VALUES (710, 505, 41);
INSERT INTO `session_registrations` VALUES (711, 71, 41);
INSERT INTO `session_registrations` VALUES (712, 541, 41);
INSERT INTO `session_registrations` VALUES (713, 562, 41);
INSERT INTO `session_registrations` VALUES (714, 99, 41);
INSERT INTO `session_registrations` VALUES (715, 536, 41);
INSERT INTO `session_registrations` VALUES (716, 93, 41);
INSERT INTO `session_registrations` VALUES (717, 572, 41);
INSERT INTO `session_registrations` VALUES (718, 523, 41);
INSERT INTO `session_registrations` VALUES (719, 83, 41);
INSERT INTO `session_registrations` VALUES (720, 101, 41);
INSERT INTO `session_registrations` VALUES (721, 568, 41);
INSERT INTO `session_registrations` VALUES (722, 498, 41);
INSERT INTO `session_registrations` VALUES (723, 482, 41);
INSERT INTO `session_registrations` VALUES (724, 503, 41);
INSERT INTO `session_registrations` VALUES (725, 72, 41);
INSERT INTO `session_registrations` VALUES (726, 539, 41);
INSERT INTO `session_registrations` VALUES (727, 481, 41);
INSERT INTO `session_registrations` VALUES (728, 533, 41);
INSERT INTO `session_registrations` VALUES (729, 691, 31);
INSERT INTO `session_registrations` VALUES (730, 594, 31);
INSERT INTO `session_registrations` VALUES (731, 595, 31);
INSERT INTO `session_registrations` VALUES (732, 684, 31);
INSERT INTO `session_registrations` VALUES (733, 670, 31);
INSERT INTO `session_registrations` VALUES (734, 650, 31);
INSERT INTO `session_registrations` VALUES (735, 672, 31);
INSERT INTO `session_registrations` VALUES (736, 696, 31);
INSERT INTO `session_registrations` VALUES (737, 662, 31);
INSERT INTO `session_registrations` VALUES (738, 633, 31);
INSERT INTO `session_registrations` VALUES (739, 700, 31);
INSERT INTO `session_registrations` VALUES (740, 689, 31);
INSERT INTO `session_registrations` VALUES (741, 614, 31);
INSERT INTO `session_registrations` VALUES (742, 608, 31);
INSERT INTO `session_registrations` VALUES (743, 641, 31);
INSERT INTO `session_registrations` VALUES (744, 587, 31);
INSERT INTO `session_registrations` VALUES (745, 657, 31);
INSERT INTO `session_registrations` VALUES (746, 679, 31);
INSERT INTO `session_registrations` VALUES (747, 602, 31);
INSERT INTO `session_registrations` VALUES (748, 683, 31);
INSERT INTO `session_registrations` VALUES (749, 688, 31);
INSERT INTO `session_registrations` VALUES (750, 619, 31);
INSERT INTO `session_registrations` VALUES (751, 612, 31);
INSERT INTO `session_registrations` VALUES (752, 638, 31);
INSERT INTO `session_registrations` VALUES (753, 613, 31);
INSERT INTO `session_registrations` VALUES (754, 596, 31);
INSERT INTO `session_registrations` VALUES (755, 597, 31);
INSERT INTO `session_registrations` VALUES (756, 598, 31);
INSERT INTO `session_registrations` VALUES (757, 692, 31);
INSERT INTO `session_registrations` VALUES (758, 648, 31);
INSERT INTO `session_registrations` VALUES (759, 593, 31);
INSERT INTO `session_registrations` VALUES (760, 677, 31);
INSERT INTO `session_registrations` VALUES (761, 645, 31);
INSERT INTO `session_registrations` VALUES (762, 580, 31);
INSERT INTO `session_registrations` VALUES (763, 615, 31);
INSERT INTO `session_registrations` VALUES (764, 687, 31);
INSERT INTO `session_registrations` VALUES (765, 702, 31);
INSERT INTO `session_registrations` VALUES (766, 583, 31);
INSERT INTO `session_registrations` VALUES (767, 626, 31);
INSERT INTO `session_registrations` VALUES (768, 632, 31);
INSERT INTO `session_registrations` VALUES (769, 610, 31);
INSERT INTO `session_registrations` VALUES (770, 674, 31);
INSERT INTO `session_registrations` VALUES (771, 586, 31);
INSERT INTO `session_registrations` VALUES (772, 651, 31);
INSERT INTO `session_registrations` VALUES (773, 698, 31);
INSERT INTO `session_registrations` VALUES (774, 695, 31);
INSERT INTO `session_registrations` VALUES (775, 701, 31);
INSERT INTO `session_registrations` VALUES (776, 585, 31);
INSERT INTO `session_registrations` VALUES (777, 673, 31);
INSERT INTO `session_registrations` VALUES (778, 637, 31);
INSERT INTO `session_registrations` VALUES (779, 620, 31);
INSERT INTO `session_registrations` VALUES (780, 680, 31);
INSERT INTO `session_registrations` VALUES (781, 694, 31);
INSERT INTO `session_registrations` VALUES (782, 592, 31);
INSERT INTO `session_registrations` VALUES (783, 649, 31);
INSERT INTO `session_registrations` VALUES (784, 642, 31);
INSERT INTO `session_registrations` VALUES (785, 685, 31);
INSERT INTO `session_registrations` VALUES (786, 622, 31);
INSERT INTO `session_registrations` VALUES (787, 579, 31);
INSERT INTO `session_registrations` VALUES (788, 600, 31);
INSERT INTO `session_registrations` VALUES (789, 629, 31);
INSERT INTO `session_registrations` VALUES (790, 603, 31);
INSERT INTO `session_registrations` VALUES (791, 682, 31);
INSERT INTO `session_registrations` VALUES (792, 647, 31);
INSERT INTO `session_registrations` VALUES (793, 646, 31);
INSERT INTO `session_registrations` VALUES (794, 589, 31);
INSERT INTO `session_registrations` VALUES (795, 666, 31);
INSERT INTO `session_registrations` VALUES (796, 697, 31);
INSERT INTO `session_registrations` VALUES (797, 607, 31);
INSERT INTO `session_registrations` VALUES (798, 616, 31);
INSERT INTO `session_registrations` VALUES (799, 644, 31);
INSERT INTO `session_registrations` VALUES (800, 606, 31);
INSERT INTO `session_registrations` VALUES (801, 699, 31);
INSERT INTO `session_registrations` VALUES (802, 659, 31);
INSERT INTO `session_registrations` VALUES (803, 609, 31);
INSERT INTO `session_registrations` VALUES (804, 635, 31);
INSERT INTO `session_registrations` VALUES (805, 624, 31);
INSERT INTO `session_registrations` VALUES (806, 669, 31);
INSERT INTO `session_registrations` VALUES (807, 628, 31);
INSERT INTO `session_registrations` VALUES (808, 601, 31);
INSERT INTO `session_registrations` VALUES (809, 588, 31);
INSERT INTO `session_registrations` VALUES (810, 663, 32);
INSERT INTO `session_registrations` VALUES (811, 586, 32);
INSERT INTO `session_registrations` VALUES (812, 649, 32);
INSERT INTO `session_registrations` VALUES (813, 666, 32);
INSERT INTO `session_registrations` VALUES (814, 680, 32);
INSERT INTO `session_registrations` VALUES (815, 654, 32);
INSERT INTO `session_registrations` VALUES (816, 623, 32);
INSERT INTO `session_registrations` VALUES (817, 687, 32);
INSERT INTO `session_registrations` VALUES (818, 613, 32);
INSERT INTO `session_registrations` VALUES (819, 640, 32);
INSERT INTO `session_registrations` VALUES (820, 675, 32);
INSERT INTO `session_registrations` VALUES (821, 622, 32);
INSERT INTO `session_registrations` VALUES (822, 597, 32);
INSERT INTO `session_registrations` VALUES (823, 607, 32);
INSERT INTO `session_registrations` VALUES (824, 677, 32);
INSERT INTO `session_registrations` VALUES (825, 659, 32);
INSERT INTO `session_registrations` VALUES (826, 580, 32);
INSERT INTO `session_registrations` VALUES (827, 692, 32);
INSERT INTO `session_registrations` VALUES (828, 661, 32);
INSERT INTO `session_registrations` VALUES (829, 651, 32);
INSERT INTO `session_registrations` VALUES (830, 594, 32);
INSERT INTO `session_registrations` VALUES (831, 658, 32);
INSERT INTO `session_registrations` VALUES (832, 611, 32);
INSERT INTO `session_registrations` VALUES (833, 655, 32);
INSERT INTO `session_registrations` VALUES (834, 612, 32);
INSERT INTO `session_registrations` VALUES (835, 691, 32);
INSERT INTO `session_registrations` VALUES (836, 610, 32);
INSERT INTO `session_registrations` VALUES (837, 685, 32);
INSERT INTO `session_registrations` VALUES (838, 683, 32);
INSERT INTO `session_registrations` VALUES (839, 695, 32);
INSERT INTO `session_registrations` VALUES (840, 657, 32);
INSERT INTO `session_registrations` VALUES (841, 700, 32);
INSERT INTO `session_registrations` VALUES (842, 693, 32);
INSERT INTO `session_registrations` VALUES (843, 636, 32);
INSERT INTO `session_registrations` VALUES (844, 653, 32);
INSERT INTO `session_registrations` VALUES (845, 591, 32);
INSERT INTO `session_registrations` VALUES (846, 581, 32);
INSERT INTO `session_registrations` VALUES (847, 601, 32);
INSERT INTO `session_registrations` VALUES (848, 671, 32);
INSERT INTO `session_registrations` VALUES (849, 629, 32);
INSERT INTO `session_registrations` VALUES (850, 694, 32);
INSERT INTO `session_registrations` VALUES (851, 656, 32);
INSERT INTO `session_registrations` VALUES (852, 604, 32);
INSERT INTO `session_registrations` VALUES (853, 614, 32);
INSERT INTO `session_registrations` VALUES (854, 615, 32);
INSERT INTO `session_registrations` VALUES (855, 616, 32);
INSERT INTO `session_registrations` VALUES (856, 595, 32);
INSERT INTO `session_registrations` VALUES (857, 652, 32);
INSERT INTO `session_registrations` VALUES (858, 662, 32);
INSERT INTO `session_registrations` VALUES (859, 672, 32);
INSERT INTO `session_registrations` VALUES (860, 584, 32);
INSERT INTO `session_registrations` VALUES (861, 624, 32);
INSERT INTO `session_registrations` VALUES (862, 583, 32);
INSERT INTO `session_registrations` VALUES (863, 647, 32);
INSERT INTO `session_registrations` VALUES (864, 643, 32);
INSERT INTO `session_registrations` VALUES (865, 674, 32);
INSERT INTO `session_registrations` VALUES (866, 621, 32);
INSERT INTO `session_registrations` VALUES (867, 590, 32);
INSERT INTO `session_registrations` VALUES (868, 642, 32);
INSERT INTO `session_registrations` VALUES (869, 645, 32);
INSERT INTO `session_registrations` VALUES (870, 690, 32);
INSERT INTO `session_registrations` VALUES (871, 702, 32);
INSERT INTO `session_registrations` VALUES (872, 588, 32);
INSERT INTO `session_registrations` VALUES (873, 589, 32);
INSERT INTO `session_registrations` VALUES (874, 699, 32);

-- ----------------------------
-- Table structure for sessions
-- ----------------------------
DROP TABLE IF EXISTS `sessions`;
CREATE TABLE `sessions`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `room_id` int(11) NOT NULL,
  `title` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `description` mediumtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `speaker` varchar(45) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `start` datetime(0) NOT NULL,
  `end` datetime(0) NOT NULL,
  `type` enum('talk','workshop') CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `cost` decimal(9, 2) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `fk_sessions_rooms1_idx`(`room_id`) USING BTREE,
  CONSTRAINT `fk_sessions_rooms1` FOREIGN KEY (`room_id`) REFERENCES `rooms` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE = InnoDB AUTO_INCREMENT = 43 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sessions
-- ----------------------------
INSERT INTO `sessions` VALUES (1, 1, 'Different place, same skills', 'In an increasingly globalized economy, flows of migrants are contributing to profound demographic changes and shifts in work. The need to create new international standards for skills development and applications in global, human-centered workplaces is more pressing than ever. How can countries harness these trends to build an inclusive global labour market, while recognizing diversity and local specificities?', 'Jens Bjornavold', '2019-09-23 09:00:00', '2019-09-23 09:45:00', 'talk', NULL);
INSERT INTO `sessions` VALUES (2, 1, 'Making work meaningful', 'The increasing shift in the values and cultures of our societies, and growing social and economic inequalities bring new expectations in workplaces. How do we move from discontent to fulfillment? This workshop will examine best practices from respective industries and share lessons learned towards more prosperous individuals and communities.', 'Abigail Fulton', '2019-09-23 10:00:00', '2019-09-23 11:30:00', 'talk', NULL);
INSERT INTO `sessions` VALUES (3, 2, 'Skills and sustainability', 'By 2050, 66 percent of the world’s population is projected to be urban. Moreover, the rapid expansion of human settlements is changing social dynamics and the lifestyle of city-dwellers, especially in megacities. What will be their role as key stakeholders in our globalized world? And how does this affect skilled (and re-skilled) people to achieve sustainable urbanization?', 'Sebastien Turbot', '2019-09-23 10:00:00', '2019-09-23 11:30:00', 'talk', NULL);
INSERT INTO `sessions` VALUES (4, 2, 'Designing skills pathways', 'During this workshop, we summarize trends and challenges within three tracks: economy, society, and environment. Participants will exchange views on what skills, best practices, and solutions they need for their specific regions to meet demands and close their skills gap.', 'Dmitry Zabirov', '2019-09-23 13:00:00', '2019-09-23 15:00:00', 'workshop', 50.00);
INSERT INTO `sessions` VALUES (5, 1, 'Skills change lives', 'Through a series of five short inspiring speeches, changemakers will share solutions through skills to the most pressing social challenges we face, leading the way to an inclusive society.', 'Crispin Thorold', '2019-09-23 13:00:00', '2019-09-23 13:45:00', 'talk', NULL);
INSERT INTO `sessions` VALUES (6, 2, 'Education ecosystem for the future', 'In our fast-changing world, traditional forms of education are becoming outdated. We need to redesign our education systems. What are approaches to learning work in this new world?', 'Pavel Luksha', '2019-09-23 15:00:00', '2019-09-23 17:00:00', 'workshop', 30.00);
INSERT INTO `sessions` VALUES (7, 3, 'The future of work', 'What are young people’s expectations and concerns for their professional future? WorldSkills and the OECD have joined efforts to listen to the youth and provide them with a platform for action.', 'Shayne Maclachlan', '2019-09-23 11:00:00', '2019-09-23 11:45:00', 'talk', NULL);
INSERT INTO `sessions` VALUES (8, 3, 'Digital skills wave', 'Technological progress and the advent of the digital economy have created new business models and a demand for a more skilled workforce. However, uneven digital penetration across socio-economic landscapes results in a digital skills divide, with many people unable to catch up to the fast-paced digital workforce. What are the cutting-edge, promising skills of this field?', 'Jordan Shapiro', '2019-09-23 13:00:00', '2019-09-23 13:45:00', 'talk', NULL);
INSERT INTO `sessions` VALUES (9, 4, 'Training and innovation', 'In this rapidly evolving technological and digital world, adopting and maintaining the “right” skills can often feel like an impossible task. What are the skills missing in your industry? How can we collaborate to find solutions to close the skills gap? This workshop will explore new ways to train workforces to better adapt to our evolving economies.', 'Jayshree Seth', '2019-09-23 14:00:00', '2019-09-23 16:00:00', 'workshop', 45.00);
INSERT INTO `sessions` VALUES (10, 5, 'What are green skills?', 'The sustainability issues faced by our societies require a new kind of growth that guarantees that future generations will enjoy the high standards of living that we expect today. With this deepening collective conscience comes a need to explore and develop “green skills” that not only focus on sustainable production and frugal living, but also help us overcome environmental challenges in a systemic way.', 'Denise Amyot', '2019-09-23 10:00:00', '2019-09-23 10:45:00', 'talk', NULL);
INSERT INTO `sessions` VALUES (11, 5, 'Greening your workforce', 'This hands-on session will explore the green skills necessary for creating self-sustaining work ecosystems. How can sustainability be integrated into your industry\'s skills standards? What type of technologies and innovations will allow different industries to face the ecological transition and tackle climate change?', 'Nader Imani', '2019-09-23 13:00:00', '2019-09-23 14:30:00', 'workshop', 25.00);
INSERT INTO `sessions` VALUES (12, 5, 'Our planet in 2050', 'Five young adults take to the stage to share inspiring stories about leveraging skills to overcome environmental challenges, to more effectively incorporate climate change awareness into education and, more generally, to create solutions for a self-sustaining ecosystem.', 'Kehkashan Basu', '2019-09-23 15:00:00', '2019-09-23 16:00:00', 'talk', NULL);
INSERT INTO `sessions` VALUES (13, 6, 'React today and tomorrow', 'Nulla ut erat id mauris vulputate elementum. Nullam varius. Nulla facilisi. Cras non velit nec nisi vulputate nonummy.', 'Dan Abramov', '2018-06-12 09:00:00', '2018-06-12 09:45:00', 'talk', NULL);
INSERT INTO `sessions` VALUES (14, 6, 'Concurrent rendering', 'Curabitur in libero ut massa volutpat convallis. Morbi odio odio, elementum eu, interdum eu, tincidunt in, leo. Maecenas pulvinar lobortis est. Phasellus sit amet erat. Nulla tempus. Vivamus in felis eu sapien cursus vestibulum. Proin eu mi.', 'Brian Vaughn', '2018-06-12 10:00:00', '2018-06-12 10:45:00', 'talk', NULL);
INSERT INTO `sessions` VALUES (15, 6, 'React suspense', 'Phasellus in felis. Donec semper sapien a libero. Nam dui. Proin leo odio, porttitor id, consequat in, consequat ut, nulla. Sed accumsan felis. Ut at dolor quis odio consequat varius. Integer ac leo. Pellentesque ultrices mattis odio. Donec vitae nisi. Nam ultrices, libero non mattis pulvinar, nulla pede ullamcorper augue, a suscipit nulla elit ac nulla.', 'Jared Palmer', '2018-06-12 11:00:00', '2018-06-12 11:45:00', 'talk', NULL);
INSERT INTO `sessions` VALUES (16, 7, '90% Cleaner React', 'In sagittis dui vel nisl. Duis ac nibh. Fusce lacus purus, aliquet at, feugiat non, pretium quis, lectus. Suspendisse potenti. In eleifend quam a odio. In hac habitasse platea dictumst. Maecenas ut massa quis augue luctus tincidunt. Nulla mollis molestie lorem.', 'Ryan Florence', '2018-06-12 13:00:00', '2018-06-12 13:45:00', 'talk', NULL);
INSERT INTO `sessions` VALUES (17, 7, 'GraphQL in react', 'Etiam vel augue. Vestibulum rutrum rutrum neque. Aenean auctor gravida sem. Praesent id massa id nisl venenatis lacinia. Aenean sit amet justo. Morbi ut odio. Cras mi pede, malesuada in, imperdiet et, commodo vulputate, justo. In blandit ultrices enim. Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Proin interdum mauris non ligula pellentesque ultrices. Phasellus id sapien in sapien iaculis congue. Vivamus metus arcu, adipiscing molestie, hendrerit at, vulputate vitae, nisl. Aenean lectus. Pellentesque eget nunc. Donec quis orci eget orci vehicula condimentum. Curabitur in libero ut massa volutpat convallis. Morbi odio odio, elementum eu, interdum eu, tincidunt in, leo. Maecenas pulvinar lobortis est. Phasellus sit amet erat.', 'Conor Hastings', '2018-06-12 14:00:00', '2018-06-12 14:45:00', 'talk', NULL);
INSERT INTO `sessions` VALUES (18, 7, 'Building a todo app', 'Nulla facilisi. Cras non velit nec nisi vulputate nonummy. Maecenas tincidunt lacus at velit. Vivamus vel nulla eget eros elementum pellentesque. Quisque porta volutpat erat. Quisque erat eros, viverra eget, congue eget, semper rutrum, nulla. Nunc purus. Phasellus in felis. Donec semper sapien a libero. Nam dui. Proin leo odio, porttitor id, consequat in, consequat ut, nulla. Sed accumsan felis.', 'Christina Holland', '2018-06-12 15:00:00', '2018-06-12 16:30:00', 'workshop', 25.00);
INSERT INTO `sessions` VALUES (19, 8, 'React in 2019', 'Nulla tempus. Vivamus in felis eu sapien cursus vestibulum. Proin eu mi. Nulla ac enim. In tempor, turpis nec euismod scelerisque, quam turpis adipiscing lorem, vitae mattis nibh ligula nec sem. Duis aliquam convallis nunc. Proin at turpis a pede posuere nonummy. Integer non velit. Donec diam neque, vestibulum eget, vulputate ut, ultrices vel, augue. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec pharetra, magna vestibulum aliquet ultrices, erat tortor sollicitudin mi, sit amet lobortis sapien sapien non mi. Integer ac neque. Duis bibendum. Morbi non quam nec dui luctus rutrum.', 'Brian Vaughn', '2019-10-24 09:00:00', '2019-10-24 09:45:00', 'talk', NULL);
INSERT INTO `sessions` VALUES (20, 8, 'Hooks', 'Nullam varius.', 'Dan Abramov', '2019-10-24 10:00:00', '2019-10-24 10:45:00', 'talk', NULL);
INSERT INTO `sessions` VALUES (21, 8, 'Beyond react', 'Cras pellentesque volutpat dui. Maecenas tristique, est et tempus semper, est quam pharetra magna, ac consequat metus sapien ut nunc. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Mauris viverra diam vitae quam. Suspendisse potenti.', 'Jared Palmer', '2019-10-24 11:00:00', '2019-10-24 11:45:00', 'talk', NULL);
INSERT INTO `sessions` VALUES (22, 9, 'Next.js: Universal React', 'Etiam faucibus cursus urna. Ut tellus. Nulla ut erat id mauris vulputate elementum. Nullam varius. Nulla facilisi. Cras non velit nec nisi vulputate nonummy. Maecenas tincidunt lacus at velit. Vivamus vel nulla eget eros elementum pellentesque. Quisque porta volutpat erat. Quisque erat eros, viverra eget, congue eget, semper rutrum, nulla. Nunc purus. Phasellus in felis. Donec semper sapien a libero.', 'Guillermo Rauch', '2019-10-24 13:00:00', '2019-10-24 13:45:00', 'talk', NULL);
INSERT INTO `sessions` VALUES (23, 9, 'Realtime Apps', 'Nullam porttitor lacus at turpis. Donec posuere metus vitae ipsum. Aliquam non mauris. Morbi non lectus. Aliquam sit amet diam in magna bibendum imperdiet. Nullam orci pede, venenatis non, sodales sed, tincidunt eu, felis. Fusce posuere felis sed lacus. Morbi sem mauris, laoreet ut, rhoncus aliquet, pulvinar sed, nisl. Nunc rhoncus dui vel sem. Sed sagittis. Nam congue, risus semper porta volutpat, quam pede lobortis ligula, sit amet eleifend pede libero quis orci. Nullam molestie nibh in lectus. Pellentesque at nulla. Suspendisse potenti. Cras in purus eu magna vulputate luctus. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Vivamus vestibulum sagittis sapien.', 'Robert Zhu', '2019-10-24 14:00:00', '2019-10-24 14:45:00', 'talk', NULL);
INSERT INTO `sessions` VALUES (24, 9, 'Developing AR Apps', 'Praesent id massa id nisl venenatis lacinia. Aenean sit amet justo. Morbi ut odio. Cras mi pede, malesuada in, imperdiet et, commodo vulputate, justo. In blandit ultrices enim. Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Proin interdum mauris non ligula pellentesque ultrices. Phasellus id sapien in sapien iaculis congue. Vivamus metus arcu, adipiscing molestie, hendrerit at, vulputate vitae, nisl. Aenean lectus. Pellentesque eget nunc. Donec quis orci eget orci vehicula condimentum. Curabitur in libero ut massa volutpat convallis. Morbi odio odio, elementum eu, interdum eu, tincidunt in, leo. Maecenas pulvinar lobortis est. Phasellus sit amet erat.', 'Pulkit Kakkar', '2019-10-24 15:00:00', '2019-10-24 16:30:00', 'workshop', 25.00);
INSERT INTO `sessions` VALUES (25, 10, 'State of the Vuenion', 'Curabitur in libero ut massa volutpat convallis. Morbi odio odio, elementum eu, interdum eu, tincidunt in, leo. Maecenas pulvinar lobortis est.', 'Evan You', '2020-02-14 09:30:00', '2020-02-14 10:15:00', 'talk', NULL);
INSERT INTO `sessions` VALUES (26, 11, 'Modern Web Apps', 'Vivamus in felis eu sapien cursus vestibulum. Proin eu mi. Nulla ac enim. In tempor, turpis nec euismod scelerisque, quam turpis adipiscing lorem, vitae mattis nibh ligula nec sem. Duis aliquam convallis nunc. Proin at turpis a pede posuere nonummy. Integer non velit. Donec diam neque, vestibulum eget, vulputate ut, ultrices vel, augue. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec pharetra, magna vestibulum aliquet ultrices, erat tortor sollicitudin mi, sit amet lobortis sapien sapien non mi. Integer ac neque. Duis bibendum. Morbi non quam nec dui luctus rutrum. Nulla tellus. In sagittis dui vel nisl. Duis ac nibh. Fusce lacus purus, aliquet at, feugiat non, pretium quis, lectus. Suspendisse potenti.', 'Filip Rakowski', '2020-02-14 10:30:00', '2020-02-14 11:15:00', 'talk', NULL);
INSERT INTO `sessions` VALUES (27, 12, 'Advanced Animations', 'Phasellus id sapien in sapien iaculis congue. Vivamus metus arcu, adipiscing molestie, hendrerit at, vulputate vitae, nisl. Aenean lectus. Pellentesque eget nunc. Donec quis orci eget orci vehicula condimentum. Curabitur in libero ut massa volutpat convallis. Morbi odio odio, elementum eu, interdum eu, tincidunt in, leo.', 'Sarah Drasner', '2020-02-14 10:30:00', '2020-02-14 11:15:00', 'talk', NULL);
INSERT INTO `sessions` VALUES (28, 12, 'NativeScript-Vue + ML', 'Praesent blandit. Nam nulla. Integer pede justo, lacinia eget, tincidunt eget, tempus vel, pede. Morbi porttitor lorem id ligula. Suspendisse ornare consequat lectus. In est risus, auctor sed, tristique in, tempus sit amet, sem. Fusce consequat. Nulla nisl. Nunc nisl. Duis bibendum, felis sed interdum venenatis, turpis enim blandit mi, in porttitor pede justo eu massa.', 'Jen Looper', '2020-02-14 11:30:00', '2020-02-14 12:15:00', 'talk', NULL);
INSERT INTO `sessions` VALUES (29, 10, 'GraphQL + Apollo', 'Curabitur in libero ut massa volutpat convallis.', 'Sara Vieira', '2020-02-14 13:30:00', '2020-02-14 14:15:00', 'talk', NULL);
INSERT INTO `sessions` VALUES (30, 11, 'Media for everyone', 'Aliquam erat volutpat. In congue. Etiam justo.', 'Maya Shavin', '2020-02-14 13:30:00', '2020-02-14 14:15:00', 'talk', NULL);
INSERT INTO `sessions` VALUES (31, 10, 'Designing Components', 'Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Etiam vel augue. Vestibulum rutrum rutrum neque. Aenean auctor gravida sem.', 'Damian Dulisz', '2020-02-14 15:00:00', '2020-02-14 16:30:00', 'workshop', 30.00);
INSERT INTO `sessions` VALUES (32, 12, 'Next Level Jest Testing', 'Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec pharetra, magna vestibulum aliquet ultrices, erat tortor sollicitudin mi, sit amet lobortis sapien sapien non mi. Integer ac neque. Duis bibendum. Morbi non quam nec dui luctus rutrum. Nulla tellus. In sagittis dui vel nisl. Duis ac nibh. Fusce lacus purus, aliquet at, feugiat non, pretium quis, lectus. Suspendisse potenti. In eleifend quam a odio. In hac habitasse platea dictumst. Maecenas ut massa quis augue luctus tincidunt. Nulla mollis molestie lorem.', 'Roman Kuba', '2020-02-14 15:00:00', '2020-02-14 16:30:00', 'workshop', 30.00);
INSERT INTO `sessions` VALUES (33, 13, 'Progressive Angular', 'Integer a nibh. In quis justo. Maecenas rhoncus aliquam lacus. Morbi quis tortor id nulla ultrices aliquet. Maecenas leo odio, condimentum id, luctus nec, molestie sed, justo. Pellentesque viverra pede ac diam. Cras pellentesque volutpat dui. Maecenas tristique, est et tempus semper, est quam pharetra magna, ac consequat metus sapien ut nunc. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Mauris viverra diam vitae quam. Suspendisse potenti. Nullam porttitor lacus at turpis. Donec posuere metus vitae ipsum. Aliquam non mauris. Morbi non lectus. Aliquam sit amet diam in magna bibendum imperdiet. Nullam orci pede, venenatis non, sodales sed, tincidunt eu, felis. Fusce posuere felis sed lacus. Morbi sem mauris, laoreet ut, rhoncus aliquet, pulvinar sed, nisl.', 'Maxim Salnikov', '2019-09-30 09:00:00', '2019-09-30 09:45:00', 'talk', NULL);
INSERT INTO `sessions` VALUES (34, 15, 'Unit Testing Angular', 'Integer pede justo, lacinia eget, tincidunt eget, tempus vel, pede. Morbi porttitor lorem id ligula. Suspendisse ornare consequat lectus. In est risus, auctor sed, tristique in, tempus sit amet, sem. Fusce consequat. Nulla nisl. Nunc nisl.', 'Joe Eames', '2019-09-30 09:00:00', '2019-09-30 09:45:00', 'talk', NULL);
INSERT INTO `sessions` VALUES (35, 14, 'Advanced NGRX', 'Morbi vel lectus in quam fringilla rhoncus. Mauris enim leo, rhoncus sed, vestibulum sit amet, cursus id, turpis. Integer aliquet, massa id lobortis convallis, tortor risus dapibus augue, vel accumsan tellus nisi eu orci. Mauris lacinia sapien quis libero. Nullam sit amet turpis elementum ligula vehicula consequat.', 'Jesse Sanders', '2019-09-30 10:00:00', '2019-09-30 10:45:00', 'talk', NULL);
INSERT INTO `sessions` VALUES (36, 11, 'Testing with Cypress.io', 'In tempor, turpis nec euismod scelerisque, quam turpis adipiscing lorem, vitae mattis nibh ligula nec sem. Duis aliquam convallis nunc. Proin at turpis a pede posuere nonummy. Integer non velit. Donec diam neque, vestibulum eget, vulputate ut, ultrices vel, augue. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec pharetra, magna vestibulum aliquet ultrices, erat tortor sollicitudin mi, sit amet lobortis sapien sapien non mi. Integer ac neque. Duis bibendum. Morbi non quam nec dui luctus rutrum. Nulla tellus. In sagittis dui vel nisl. Duis ac nibh.', 'Joe Eames', '2019-09-30 10:00:00', '2019-09-30 10:45:00', 'talk', NULL);
INSERT INTO `sessions` VALUES (37, 13, 'Web Components', 'Quisque id justo sit amet sapien dignissim vestibulum. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Nulla dapibus dolor vel est. Donec odio justo, sollicitudin ut, suscipit a, feugiat et, eros. Vestibulum ac est lacinia nisi venenatis tristique. Fusce congue, diam id ornare imperdiet, sapien urna pretium nisl, ut volutpat sapien arcu sed augue. Aliquam erat volutpat. In congue. Etiam justo. Etiam pretium iaculis justo. In hac habitasse platea dictumst. Etiam faucibus cursus urna. Ut tellus.', 'Manfred Steyer', '2019-09-30 11:00:00', '2019-09-30 11:45:00', 'talk', NULL);
INSERT INTO `sessions` VALUES (38, 16, 'Not Every App is a SPA', 'Nulla mollis molestie lorem. Quisque ut erat. Curabitur gravida nisi at nibh. In hac habitasse platea dictumst. Aliquam augue quam, sollicitudin vitae, consectetuer eget, rutrum at, lorem.', 'Rob Wormald', '2019-09-30 11:00:00', '2019-09-30 11:45:00', 'talk', NULL);
INSERT INTO `sessions` VALUES (39, 14, 'Using RxJS', 'Morbi non quam nec dui luctus rutrum. Nulla tellus. In sagittis dui vel nisl. Duis ac nibh. Fusce lacus purus, aliquet at, feugiat non, pretium quis, lectus. Suspendisse potenti. In eleifend quam a odio. In hac habitasse platea dictumst. Maecenas ut massa quis augue luctus tincidunt. Nulla mollis molestie lorem. Quisque ut erat.', 'Deborah Kurata', '2019-09-30 13:00:00', '2019-09-30 14:30:00', 'workshop', 75.50);
INSERT INTO `sessions` VALUES (40, 14, 'Angular Playground', 'Proin at turpis a pede posuere nonummy. Integer non velit. Donec diam neque, vestibulum eget, vulputate ut, ultrices vel, augue.', 'Brian Love', '2019-09-30 15:00:00', '2019-09-30 16:00:00', 'workshop', 50.00);
INSERT INTO `sessions` VALUES (41, 15, 'Advanced Cypress.io', 'Integer ac neque. Duis bibendum. Morbi non quam nec dui luctus rutrum. Nulla tellus. In sagittis dui vel nisl. Duis ac nibh. Fusce lacus purus, aliquet at, feugiat non, pretium quis, lectus.', 'Joe Eames', '2019-09-30 15:00:00', '2019-09-30 16:00:00', 'workshop', 50.00);
INSERT INTO `sessions` VALUES (42, 16, 'UX in 2019', 'Donec odio justo, sollicitudin ut, suscipit a, feugiat et, eros. Vestibulum ac est lacinia nisi venenatis tristique. Fusce congue, diam id ornare imperdiet, sapien urna pretium nisl, ut volutpat sapien arcu sed augue. Aliquam erat volutpat. In congue. Etiam justo. Etiam pretium iaculis justo.', 'Rivkah Di Ruggero', '2019-09-30 13:00:00', '2019-09-30 14:30:00', 'workshop', 75.50);

SET FOREIGN_KEY_CHECKS = 1;

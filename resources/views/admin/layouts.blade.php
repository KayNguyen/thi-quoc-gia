<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>{{ $site_title }}</title>

    {!! \HtmlHelper::getInstance()->setCssLink('backend/css/bootstrap.css') !!}
    {!! \HtmlHelper::getInstance()->setCssLink('backend/css/custom.css') !!}
</head>
<body>
    @include('admin.include.header')
    <div class="container-fluid">
        <div class="row">
            @yield('CONTENT')
        </div>
    </div>

    
    {!! \HtmlHelper::getInstance()->setJsLink('backend/js/jquery-3.4.1.min.js') !!}
    {!! \HtmlHelper::getInstance()->setJsLink('backend/js/admin.js') !!}

</body>
</html>